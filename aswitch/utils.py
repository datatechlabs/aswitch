
import time, datetime, socket, traceback, os, struct, re

from wobble import slog, conf


def ip2int(addr):
	return struct.unpack("!I", socket.inet_aton(addr))[0]

def int2ip(addr):
	return socket.inet_ntoa(struct.pack("!I", addr))

def number_from_sipuri(sipuri):
	m = re.search('<sip:(\w+)@.*>', sipuri)
	try:
		nbr = m(1)
	except:
		nbr = ''
	return nbr

def host_from_uri(uri):
	"""Extract hostname part from a SIP URI."""
	tokens = uri.split('@')
	if len(tokens) == 1:
		# could be in form sip/sip:example.org
		subtokens = uri.split(':')
		if len(subtokens) < 2:
			slog.err("cannot extract host, unrecognized uri:", uri)
			return ""
		hname = subtokens[1]
	elif len(tokens) == 2:
		hname = tokens[1]
	else:
		slog.err("cannot extract host, unrecognized uri:", uri)
		return ""
	
	# remove any additional info
	# e.g., in ``sip:hello@example.org:5060;user=phone''
	# we want the ;user=phone part removed
	address = hname.split(';')[0]

	# remove port number and return
	host = address.split(':')[0]

	# remove any parenthesis or something
	host = stringReplace(host,"[]")
	slog.info("extracted host:", host)
	
	return host

def ip_from_uri(uri):
	host = host_from_uri(uri)
	try:
		ip = socket.gethostbyname(host)
	except:
		slog.err("Unable to extract ip from host",host)
		slog.info(traceback.format_exc())
		return False
	return ip

def time_posix_to_cisco(timestamp):
	"""Convert POSIX timestamp (seconds since epoch) to CISCO format."""
	dt = datetime.datetime.fromtimestamp(timestamp)

	# time zone name
	tz = time.strftime("%Z")
	if tz == "":
		tz = "UTC"

	time_tuple = time.localtime(timestamp)	# replace with time.gmtime() for UTC
	cisco_date = time.strftime("%a %b %d %Y", time_tuple)
	cisco_time = time.strftime("%H:%M:%S", time_tuple)

	return cisco_time + " " + tz + " " + cisco_date

def time_posix_to_iso(timestamp):
	"""Convert POSIX timestamp (seconds since epoch) to ISO format."""
	dt = datetime.datetime.fromtimestamp(timestamp)
	return str(dt)

def stringReplace(s,c):
	"""Remove any occurrences of characters in c, from string s
	s - string to be filtered, c - characters to filter"""
	for a in c:
		s = s.replace(a,'')
	return s

def strip_end(text, suffix):
	""" strip suffix from end of text """
	text = str(text)
	if not text.endswith(suffix):
		return text
	return text[:-len(suffix)]

def extract_balance(rad_result):

	""" extract balance from radius response message"""
	balance_attr = rad_result.get("h323-credit-amount", [None])[0]
	if balance_attr == None:
		slog.err("Could not extract h323-credit-amount from "
			"auth response")
		return None

	balance_split = balance_attr.split('=')
	if len(balance_split) != 2:
		slog.err("Invalid balance string:", balance_attr)
		return None

	balance = balance_split[1]
	slog.info("Balance:", balance)
	return balance

def extract_currency(rad_result):
	""" extract currency code from radius response message"""

	currency_attr = rad_result.get("h323-currency", [None])[0]
	if currency_attr == None:
		slog.warn("Could not extract h323-currency from auth "
			 "response.")
		return None

	currency_split = currency_attr.split('=')
	if len(currency_split) != 2:
		slog.warn("Invalid currency string", currency_attr)
		return None

	currency = currency_split[1].upper()
	slog.info("Currency:", currency)
	return currency

def reorder_formats(aleg, bleg):
	"""Perform media list reordering according to preferences of A leg.
	Offer only those codecs which are set by radius (B leg)"""

	alist = aleg.split(',')
	blist = bleg.split(',')

	out = []

	for i in alist:
		if i in blist:
			out.append(i)
			blist.remove(i)

	outlist = out + blist
	return  ','.join(outlist)

def logrotate():
	log_file = conf.get_str("PATHS", "log_file")
	tstamp = time.time()
	now = time.gmtime(tstamp)
	suffix = time.strftime("%Y%m%d%H%M%S", now)
	rot_file = log_file + "." + suffix
	
	slog.rotate_stream(log_file, rot_file)


def logmaintenance():
	# check if need to do log rotate
	if not conf.get_bool("LOGROTATE", "enable", True):
		return False

	filesize = conf.get_str("LOGROTATE", "filesize", "10M")
	days = conf.get_int("LOGROTATE", "days", 14)
	if filesize[-1:].lower() == "k":
		slimit = int(filesize[:-1].strip()) * 1024
	elif filesize[-1:].lower() == "m":
		slimit = int(filesize[:-1].strip()) * 1048576
	elif filesize[-1:].lower() == "g":
		slimit = int(filesize[:-1].strip()) * 1073741824
	else:
		slimit = 1

	log_file = conf.get_str("PATHS", "log_file")
	try:
		statinfo = os.stat(log_file)
	except:
		return False
	if statinfo.st_size >= slimit or statinfo.st_mtime + days * 86400 < time.time():
		logrotate()


def debugmaintenance():
	# check if need to reset debug tags
	when = conf.get_int("RUNTIME", "conf_restore", 0)
	if when > 0 and time.time() > when:
		
		config_file = conf.get_str("PATHS", "config_file")
		conf.read(config_file)
		slog.info("configuration", conf.get_all())
		slog.output_tags(conf.get_list("GENERAL", "log_levels"))

