
import threading, traceback
from Queue import Queue
from wobble import slog
from aswitch import radius

calls = []		# all ongoing calls
call_lock = threading.Lock()

conf_ids = {}	# conference IDs of all channels in the calls

CALLTYPE_NONE = 0
CALLTYPE_STD = 1
CALLTYPE_IVR = 2
CALLTYPE_CONF = 3

class Call:
	"""Call."""
	
	def __init__(self):
		# call module
		self.module = None

		# call ID
		self.id = "NO_ID"
		
		# bill ID
		self.billid = None

		# yate channels
		self.channels = []

		# call type
		self.type = CALLTYPE_NONE

		# whether we can charge for this call
		self.billable = True

		# IVR sound queue
		self.sound_queue = []

		# Message queue is needed for delayed messages which could
		# accumulate if the call is waiting for a blocking operation,
		# such as a RADIUS request.
		self.msg_queue = []

		# a flag that determines whether new messages should be
		# processed or put into message queue (self.msg_queue)
		self.locked = False

		# whether there's an IVR sound playing
		self.playing = False

def lock(call):
	call.locked = True

def unlock(call):
	call.locked = False

def exec_timers(msg):
	"""Execute engine_timer() routine for each call (call module) that has
	it.
	"""
	global calls
	
	for call in calls:
		# locked calls may not be touched, since some other thread
		# could be modifying it at the same time
		if call.locked:
			continue
		
		timer = getattr(call.module, "engine_timer", None)
		if timer != None:
			try:
				timer(call, msg)
			except:
				slog.err(traceback.format_exc())

def queue_msg(call, msg):
	"""Queue message for later processing."""
	
	# find out whether a message handler for this message exists
	func_name = msg.name.replace('.', '_')
	mod_func = getattr(call.module, func_name, None)
	if mod_func == None:
		return False
	
	call.msg_queue.append(msg)
	return True

def handle_queued():
	"""Execute message handlers for each call that has queued messages."""
	
	global calls

	for call in calls:
		# locked calls may not be touched, since some other thread
		# could be modifying it at the same time
		if call.locked:
			continue

		while len(call.msg_queue) != 0:
			msg = call.msg_queue.pop(0)

			# get the function reference and execute it, if it
			# exists
			func_name = msg.name.replace('.', '_')
			mod_func = getattr(call.module, func_name, None)
			if mod_func != None:
				try:
					mod_func(call, msg)
				except:
					slog.err(traceback.format_exc())
				continue

def start(call_module, route_msg):
	"""Start a call with call.route message.

	route_msg -- first message delivered to the call (call.route)
	"""
	global call, call_lock
	
	call = Call()
	call.module = call_module

	# append the first channel
	chan = route_msg.params["id"]
	add_channel(call, chan)
	
	# create an ID
	call.id = route_msg.params.get("callid", "NO_ID")
	call.billid = route_msg.params.get("billid", None)

	call_lock.acquire()
	calls.append(call)
	call_lock.release()

	call_func = getattr(call_module, "call_route")
	call_func(call, route_msg)

def add_channel(call, chan):
	"""Add channel to a call."""

	if (chan == None) or (chan == ""):
		slog.warn("calls.add_channel(" + repr(chan) + ")")
		return
	
	if chan not in call.channels:
		slog.info("Adding channel", chan, "to call with channels:",
		    call.channels)
		call.channels.append(chan)
		conf_ids[chan] = radius.gen_h323_conf_id()
		slog.info("conf id",conf_ids[chan])
#		slog.info("conf_ids",conf_ids)
	
def rm_channel(call, chan, call_rm=True):
	"""Remove channel from a call, and remove the call from global list if
	no channels are left and if so requested.

	call -- call structure
	chan -- channel name
	call_rm -- whether to remove call from the global call list if no
			channels are left
	"""
	slog.info("Removing channel", chan, "from call with channels:",
	    call.channels)
	try:
		call.channels.remove(chan)
		del conf_ids[chan]
	except:
		slog.info("Channel", chan, "already removed from call")

	#
	# If only fork channels remain and @call_rm == True, remove the call
	# from global list.
	#

	if call_rm == False:
		return

	dead = True
	for chan in call.channels:
		if chan.startswith("fork/") == False:
			dead = False
			break

	if dead:
		slog.info("Removing call from global call list.")
		_remove(call)
		
def _remove(call):
	"""Remove call from the global calls list."""

	global calls, call_lock

	# remove confids
	for chan in call.channels:
		if chan in conf_ids:
			slog.info ("Removing conf ID for chan",chan)
			del conf_ids[chan]

	call_lock.acquire()

	# remove call
	for i in xrange(len(calls)):
		if calls[i] == call:
			del calls[i]
			break
	
	call_lock.release()

def shutdown_all():
	"""Stop all calls."""
	
	global calls
	
	slog.info("Shutting down all calls.")
	call_lock.acquire()
	calls = []
	call_lock.release()

def find_by_id(id):
	"""Find call by ID.

	Returns the call struct if it was found, None otherwise.
	"""
	call = None
	call_lock.acquire()
	for c in calls:
		if c.id == id:
			call = c
			break
	call_lock.release()
	return call

def find_by_message(msg):
	"""Find call by message parameters.

	Returns the call struct if it was found, None otherwise.
	"""
	call_lock.acquire()
	call = _find_by_message(msg)
	call_lock.release()
	return call

def find_by_billid(bill_id):
	"""Find call by its billid.
	
	Returns the call struct if it was found, None otherwise.
	"""
	call = None
	call_lock.acquire()
	for call in calls:
		billid = call.billid
		if not billid:
			continue
		if bill_id == billid:
			slog.info("Call lookup by billid", bill_id,
			    "succeeded.")
			call_lock.release()
			return call
	slog.info("Call look up by billid", bill_id, "failed.")
	call_lock.release()
	return None

def _find_by_message(msg):
	"""Find call by message that has been addressed to it (inspect its
	channels).
	"""
	if msg.name == "call.answered":
		chan = msg.params.get("targetid", "")
		id = msg.params.get("id", "")
		slog.info("Call lookup by 'targetid' parameter from "
		    "call.answered message.")
		call = _find_by_channel(chan)
		
		if call != None:
			add_channel(call, id)
			return call
	
	if msg.name == "chan.notify":
		# notify messages may have the channel id in targetid
		# (or they may not -- but it's worth a try)
		chan = msg.params.get("targetid", "")
		slog.info("Call lookup by 'targetid' parameter from "
		    "chan.notify message.")
		return _find_by_channel(chan)

	if msg.name == "call.cdr":
		# cdr messages have channel ID as 'chan' parameter
		chan = msg.params.get("chan", "")
		billid = msg.params.get("billid", "")
		slog.info("Call lookup by 'chan' parameter from call.cdr "
		    "message.")
		call = _find_by_channel(chan)
		if call != None:
			add_channel(call, chan)
			return call
		call = _find_by_billid(billid)
		if call != None:
			add_channel(call, chan)
			return call
		return None

			

	if msg.name == "call.route":
		chan = msg.params["id"]

		# NOTE: we do not add actual fork channels but fork.master instead
		if chan.startswith("fork/"):
			# channel that started forking
			fork_chan_orig = msg.params["fork.origid"]

			# fork channel (fork/X)
			fork_chan = msg.params["fork.master"]

			# extract target channel, if any
			target_chan = msg.params.get("targetid")

			slog.info("Call lookup by 'fork.origid' parameter from "
				"call.route message.")
			call = _find_by_channel(fork_chan_orig)
			if call != None:
				add_channel(call, chan)
				add_channel(call, fork_chan)
				add_channel(call, target_chan)
				return call

	# XXX not so sure about this anymore ?
	#if msg.name == "call.route":
		# for outgoing calls the "target" parameter of the call
		# structure is what we are looking for
	#	if "called" in msg.params:
	#		called_route = msg.params["called"]
	#		for call in calls:
	#			if hasattr(call, "target") and \
	#			    (call.target == called_route):
	#				return call

	if "id" not in msg.params:
		slog.info("Message does not contain channel ID.")
		return None

	chan = msg.params["id"]

	if msg.name != "call.execute":
		slog.info("Call lookup by 'id' parameter from", msg.name,
		    "message")
		return _find_by_channel(chan)

	#
	# handle call.execute messages (including forks!)
	#

	# extract target channel, if any
	target_chan = msg.params.get("targetid")

	# call lookup by "id" parameter
	slog.info("Call lookup by 'id' parameter from call.execute message.")
	call = _find_by_channel(chan)
	if call != None:
		if target_chan != None:
			add_channel(call, target_chan)
		return call

	# call lookup by "targetid" parameter
	slog.info("Call lookup by 'targetid' parameter from call.execute "
	    "message.")
	call = _find_by_channel(target_chan)
	if call != None:
		add_channel(call, chan)
		return call

	# NOTE: we do not add actual fork channels but fork.master instead
	if chan.startswith("fork/"):
		# channel that started forking
		fork_chan_orig = msg.params["fork.origid"]

		# fork channel (fork/X)
		fork_chan = msg.params["fork.master"]

		slog.info("Call lookup by 'fork.origid' parameter from "
		    "call.execute message.")
		call = _find_by_channel(fork_chan_orig)
		if call != None:
			add_channel(call, fork_chan)
			add_channel(call, target_chan)
			return call

	slog.info("Call lookup failed.")
	return None

def _find_by_channel(chan):
	"""Find call by one of its channels."""
	
	for call in calls:
		if chan in call.channels:
			slog.info("Call lookup by channel", chan,
			    "succeeded.")
			return call

	slog.info("Call look up by channel", chan, "failed.")
	return None
	
def _find_by_billid(billid):
	"""Find call by Bill ID.
	Returns the call struct if it was found, None otherwise.
	"""
	call = None
	for c in calls:
		if c.billid == billid:
			call = c
			slog.info("Call lookup by billid", billid,
			    "succeeded.")
			return call
	slog.info("Call look up by billid", billid, "failed.")
	return None
