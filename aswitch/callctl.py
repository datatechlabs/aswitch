"""Some random functions for more convenient call control."""

import math
import time
from wobble import slog, conf
from aswitch import yate, calls, radius, utils, return_codes, sound_map, routes

SOUNDTYPE_WAVE = 1
SOUNDTYPE_TONE = 2
SOUNDTYPE_RECORD = 3

def play(call, chan, sound):
	if isinstance(sound, str):
		call.sound_queue.append((sound, SOUNDTYPE_WAVE, chan))
	elif isinstance(sound, list):
		slist = []
		for s in sound:
			slist.append((s, SOUNDTYPE_WAVE, chan))
		call.sound_queue += slist
	else:
		raise Exception("invalid sound type")

	slog.info("Queueing sound file:", sound)
		
	if not call.playing:
		proc_sound_queue(call)

def play_raw(call, chan, sound):
	if isinstance(sound, str):
		call.sound_queue.append((sound, SOUNDTYPE_WAVE, chan))
	elif isinstance(sound, list):
		slist = []
		for s in sound:
			slist.append((s, SOUNDTYPE_WAVE, chan))
		call.sound_queue += slist
	else:
		raise Exception("invalid sound type")

	slog.info("Queueing sound file:", sound)
		
	if not call.playing:
		proc_sound_queue(call)
		
def play_map(call, chan, soundid, lang="en"):
	"""
	play sounds extracted by sound map schemes
	"""
	prompt_map = conf.get_str("PATHS", "prompt_map", "cisco")

	slog.info("Requesting sound",soundid)
	if isinstance(soundid, str):
		sound = sound_map.scheme[prompt_map][soundid]
	elif isinstance(soundid, list):
		sound = []
		for si in soundid:
			if si in sound_map.scheme[prompt_map]:
				snd = sound_map.scheme[prompt_map][si]
				if snd != None:
					sound.append(snd)
	else:
		raise Exception("invalid soundid type")

	if sound != None:
		if isinstance(sound, str):
			call.sound_queue.append((sound, SOUNDTYPE_WAVE, chan, 0, lang))
		elif isinstance(sound, list):
			slist = []
			for s in sound:
				slist.append((s, SOUNDTYPE_WAVE, chan, 0, lang))
			call.sound_queue += slist
		else:
			raise Exception("invalid sound type")
	else:
		slog.err("sound file for soundid",soundid,"prompt_map",prompt_map, "not found")
		return False

	slog.info("Queueing sound file:", sound)
		
	if not call.playing:
		proc_sound_queue(call)

def play_tone(call, chan, name):
	"""
	From the docs (http://yate.null.ro/pmwiki/index.php?n=Main.Tonegen)
	
	dial - ITU dial tone
	busy - ITU busy tone
	ring - ITU ring tone
	specdial - typically used for secondary dialtones
	congestion - no more channels available, network busy
	outoforder - broken line (ITU, not three tones) 
	milliwatt - the stardard tone for test, 1mW @ 1kHz
	silence - as the name says 
	noise - low level white noise, usable as comfort noise

	dtmf/X - single tone, X must be 0..9, *, # or a..d
	dtmfstr/XXXXX - an entire sequence of DTMF tones
	AAAA - a 1 second sine tone of frequency AAAA (number)
	AAAA+BBBB - a 1 second sum of two sine tones
	AAAA*BB - 1 second tone of AAAA modulated by BB
	"""
	slog.info("Queueing tone:", name)
	call.sound_queue.append((name, SOUNDTYPE_TONE, chan))
	
	if not call.playing:
		proc_sound_queue(call)

def record(call, chan, sound, maxlen = 100000):
	# len is in ms, yate wants it in 1/10000 s
	if isinstance(sound, str):
		call.sound_queue.append((sound, SOUNDTYPE_RECORD, chan, maxlen*10))
	elif isinstance(sound, list):
		slist = []
		for s in sound:
			slist.append((s, SOUNDTYPE_RECORD, chan, maxlen*10))
		call.sound_queue += slist
	else:
		raise Exception("invalid sound type")

	slog.info("Queueing sound file:", sound)
		
	if not call.playing:
		proc_sound_queue(call)

def proc_sound_queue(call, absolute = False):
	"""Process sound queue (play a sound if there's one in the queue).
		Set absolute to True if you will supply absolute path to play
	"""
	
	sound_dir = conf.get_str("PATHS", "sound_dir")
	extension = conf.get_str("PATHS", "extension")

	
	if len(call.sound_queue) == 0:
		call.playing = False
		return True
	
	lang = ""
	# fetch sound name and type from the queue
	if len(call.sound_queue[0]) == 3:
		(sound_name, sound_type, chan) = call.sound_queue[0]
	elif len(call.sound_queue[0]) == 4:
		(sound_name, sound_type, chan, maxlen) = call.sound_queue[0]
	elif len(call.sound_queue[0]) == 5:
		(sound_name, sound_type, chan, maxlen, lang) = call.sound_queue[0]
	del call.sound_queue[0]

	if sound_type == SOUNDTYPE_WAVE:
		if absolute == False:
			source = "wave/play/%s/%s/%s.%s" % (sound_dir, lang, sound_name, extension)
		else:
			source = "wave/play/%s" % (sound_name)
		call.playing = True
	elif sound_type == SOUNDTYPE_TONE:
		source = "tone/%s" % (sound_name)
		
		# tones do not have an end time, so it's as if nothing is being
		# played
		call.playing = False
	elif sound_type == SOUNDTYPE_RECORD:
		consumer = "wave/record/%s/%s.%s" % (sound_dir, sound_name, "au")
		call.playing = True
		# attach wavefile/tonegen module
		msg = yate.Message("chan.masquerade")
		msg.params["id"] = chan
		msg.params["message"] = "chan.attach"
		msg.params["source"] = chan
		msg.params["consumer"] = consumer # file where to record
		msg.params["maxlen"] = maxlen
		msg.params["notify"] = chan
		msg.send()
		return True
	else:
		call.playing = False
		slog.err("Invalid sound type:", sound_type)
		return False

	# attach wavefile/tonegen module
	msg = yate.Message("chan.masquerade")
	msg.params["message"] = "chan.attach"
	msg.params["id"] = chan
	msg.params["source"] = source
	msg.params["notify"] = chan
	msg.send()
	
	return True

def dropchan(chan):
	"""Kill a channel."""

	if chan == None:
		slog.warn("callctl.dropchan(None)")
		return
	
	msg = yate.Message("call.drop")
	msg.params["id"] = chan
	msg.params["reason"] = "busy"
	msg.send()

def smartdrop(call, chan):
	"""Kill a channel if it exists within @call."""

	if chan in call.channels:
		dropchan(chan)

def disconnect_call_by_billid(billid):
	call = calls.find_by_billid(billid)
	slog.info("Found call", call)
	if not call:
		slog.warn("No call found")
		return
	for chan in call.channels:
		slog.info("Dropping channel ", chan)
		callctl.dropchan(chan)

def answer(call):
	answer_msg = yate.Message("call.answered")
	answer_msg.params["id"] = call.dumb_chan
	answer_msg.params["targetid"] = call.in_chan
	answer_msg.send()

def reject(route_msg, reason="Routing failure"):
	route_msg.params["reason"] = reason
	route_msg.retvalue = ""
	route_msg.send()

def accounting_alive(call, cdr_data, route_msg, update=False):


	if update == False:
		slog.info("accounting_start()")
		acct_status_type = "Start"
	else:
		slog.info("accounting_alive()")		
		acct_status_type = "Alive"
		
	setup_time = cdr_data["time"]

	# call IDs
	h323_conf_id = cdr_data["confid"]
	billid = cdr_data["billid"]

	# caller & called numbers
	caller = cdr_data["caller"]
	called = cdr_data["called"]

	# caller address
	caller_addr = cdr_data["caller_addr"]
	called_addr = cdr_data["called_addr"]

	# strip ports IP addresses
	caller_addr = caller_addr.split(':')[0]

	if not conf.get_str("RADIUS", "called_address_port_number", False):
		called_addr = called_addr.split(':')[0]

	# arbitrary username for User-Name attribute
	username = cdr_data["username"]

	slog.info("Caller number:", caller)
	slog.info("Called number:", called)
	slog.info("Caller IP address:", caller_addr)
	slog.info("Called IP address:", called_addr)

	# convert timestamps
	timestamp_format = conf.get_str("RADIUS", "timestamp_format")
	if timestamp_format == "ISO":
		setup_time = utils.time_posix_to_iso(setup_time)
		#connect_time = utils.time_posix_to_iso(connect_time)
	elif timestamp_format == "Cisco":
		setup_time = utils.time_posix_to_cisco(setup_time)
		#connect_time = utils.time_posix_to_cisco(connect_time)
	else:
		raise Exception("Invalid timestamp format: ", timestamp_format)

	# Yate address
	nas_ip_addr = conf.get_str("RADIUS", "nas_ip_address")

	rad_params = {
		"NAS-IP-Address": nas_ip_addr,
		"Framed-IP-Address": caller_addr,
		"h323-remote-address": "h323-remote-address=%s" % called_addr,
		"Calling-Station-Id": caller,
		"Called-Station-Id": called,
		"User-Name" : username,
		"h323-call-origin": "h323-call-origin=proxy",
		"h323-call-type": "h323-call-type=VOIP",
		"h323-conf-id": "h323-conf-id=%s" % h323_conf_id,
		"Acct-Session-Id": billid,
		"h323-setup-time": "h323-setup-time=%s" % setup_time,
		#"h323-connect-time": "h323-connect-time=%s" % connect_time,
		"Acct-Status-Type": acct_status_type
	}

	# append X-headers
	rad_params['Cisco-AVPair'] = []
	# status:
	rad_params["Cisco-AVPair"] += ["h323-ivr-out=status:" + cdr_data["status"]]

	allow_x_hdr_auth = conf.get_bool("CALLS", "allow_x_header_auth", False)
	if allow_x_hdr_auth:
		x_hdr_name = conf.get_str("CALLS", "x_header_name", "x-inboundip")
		auth_x_hdr_name = "sip_" + x_hdr_name
		x_hdr = route_msg.params.get(auth_x_hdr_name, "")
		if x_hdr != "":
			rad_params["Cisco-AVPair"] += ["h323-ivr-out=auth-by-x-header:" + x_hdr_name]
			rad_params["Cisco-AVPair"] += ["h323-ivr-out=x-header-value:" + x_hdr]

	# original called
	orig_called = route_msg.params.get("called_original", None)
	if orig_called:
		rad_params["Cisco-AVPair"] += ["h323-ivr-out=original-called-number:" + orig_called]

	# dispatch RADIUS Accounting-Request packet
	slog.info("RADIUS accounting alive request:", rad_params)
	radius.queue_acct(rad_params)

def accounting_stop(call, cdr_data, route_msg):
	slog.info("accounting_stop()")

	duration = cdr_data["duration"]
	billtime = cdr_data["billtime"]

	if not call.billable:
		billtime = 0.0
	
	setup_time = cdr_data["time"]
	connect_time = setup_time + duration - billtime
	disconnect_time = setup_time + duration

	disconnect_cause = cdr_data["dcause"]

	# call IDs
	h323_conf_id = cdr_data["confid"]
	billid = cdr_data["billid"]

	# caller & called numbers
	caller = cdr_data["caller"]
	called = cdr_data["called"]

	# The actual number called. Might be different than the dialed number
	# after routing. This parameter is, or at least used to be, somewhat
	# quirky in earlier Yate versions. It is also not present in CDR for
	# incoming call leg, so we make sure it contains something.
	# The most preferrable option is to use number stored in call object
	# during authorization phase
	if "calledfull" in cdr_data:
		calledfull = cdr_data["calledfull"]
		outgoing = cdr_data["calledfull"]
		if (calledfull == None):
			calledfull = called
	if "external" in cdr_data:
		if outgoing == "":
			outgoing = cdr_data["external"]
		if outgoing == None:
			outgoing = cdr_data["external"]

	if hasattr(call,"dialed_number"):
		slog.info("call.dialed_number",call.dialed_number)
		calledfull = call.dialed_number


	# Check if we have to send originally dialed or the actually called and
	# possibly rewritten number (during routing) to RADIUS server.
	# if conf.get_bool("RADIUS", "use_rewritten", False):
	called = calledfull

	# caller and caller addresses
	caller_addr = cdr_data["caller_addr"]
	called_addr = cdr_data["called_addr"]

	# strip ports IP addresses
	caller_addr = caller_addr.split(':')[0]

	if not conf.get_bool("RADIUS", "called_address_port_number", False):
		called_addr = called_addr.split(':')[0]

	# arbitrary username for User-Name attribute
	username = cdr_data["username"]

	slog.info("Caller number:", caller)
	slog.info("Called number:", called)
	slog.info("Caller IP address:", caller_addr)
	slog.info("Called IP address:", called_addr)

	# convert timestamps
	timestamp_format = conf.get_str("RADIUS", "timestamp_format")
	if timestamp_format == "ISO":
		setup_time = utils.time_posix_to_iso(setup_time)
		connect_time = utils.time_posix_to_iso(connect_time)
		disconnect_time = utils.time_posix_to_iso(disconnect_time)
	elif timestamp_format == "Cisco":
		setup_time = utils.time_posix_to_cisco(setup_time)
		connect_time = utils.time_posix_to_cisco(connect_time)
		disconnect_time = utils.time_posix_to_cisco(disconnect_time)
	else:
		raise Exception("Invalid timestamp format: ", timestamp_format)

	# Yate address
	nas_ip_addr = conf.get_str("RADIUS", "nas_ip_address")
		
	# round up actual call duration
	session_time = int(math.ceil(billtime))

	# update stats
	timestamp = int(time.time())
	
	hour = timestamp - timestamp % 3600
	
	calls = conf.get_int("STATS", "calls_hour_" + str(hour), 0)
	conf.set_value("STATS", "calls_hour_" + str(hour), calls + 1)

	if session_time > 0:
		goodcalls = conf.get_int("STATS", "goodcalls_hour_" + str(hour), 0)
		conf.set_value("STATS", "goodcalls_hour_" + str(hour), goodcalls + 1)

	secs = conf.get_int("STATS", "dur_hour_" + str(hour), 0)
	conf.set_value("STATS", "dur_hour_" + str(hour), secs + session_time)

	rad_params = {
		"NAS-IP-Address": nas_ip_addr,
		"Framed-IP-Address": caller_addr,
		"h323-remote-address": "h323-remote-address=%s" % called_addr,
		"Calling-Station-Id": caller,
		"Called-Station-Id": called,
		"User-Name" : username,
		"h323-call-origin": "h323-call-origin=proxy",
		"h323-call-type": "h323-call-type=VOIP",
		"h323-conf-id": "h323-conf-id=%s" % h323_conf_id,
		"Acct-Session-Id": billid,
		"Acct-Session-Time": session_time,
		"h323-setup-time": "h323-setup-time=%s" % setup_time,
		"h323-connect-time": "h323-connect-time=%s" % connect_time,
		"h323-disconnect-time": "h323-disconnect-time=%s" % disconnect_time,
		"Acct-Status-Type": "Stop",
		"h323-disconnect-cause": "h323-disconnect-cause=%s" % str(disconnect_cause)
	}

	# set DNIS number
	rad_params['Cisco-AVPair'] = []
	if "dnis_access" in cdr_data:
		rad_params['Cisco-AVPair'] += ["h323-ivr-out=ACCESSCODE:" + cdr_data["dnis_access"]]

	# append any optional params
	if "options" in cdr_data:
		rad_params['Cisco-AVPair'] += ["h323-ivr-out=OPTIONS:" + cdr_data["options"]]

	# append routingtype
	if "routingtype" in cdr_data:
		rad_params['Cisco-AVPair'] += ["h323-ivr-out=ROUTINGTYPE:" + cdr_data["routingtype"]]
		
	# append rewritten number as Cisco VSA
	if outgoing != None:
		rad_params['Cisco-AVPair'] += ["xpgk-dst-number-out=" + outgoing]
		rad_params['Cisco-AVPair'] += ["h323-ivr-out=xpgk-dst-number-out:" + outgoing]

	# original called
	orig_called = route_msg.params.get("called_original", None)
	if orig_called:
		rad_params["Cisco-AVPair"] += ["h323-ivr-out=original-called-number:" + orig_called]

	user = routes.getuser_by_location(cdr_data["called_addr"])
	if user != None:
		outgw = user.name
		rad_params['Cisco-AVPair'] += ["h323-ivr-out=out-gw-id:" + outgw]
		rad_params['Cisco-AVPair'] += ["out-gw-id=" + outgw]

	# append X-headers
	allow_x_hdr_auth = conf.get_bool("CALLS", "allow_x_header_auth", False)
	if allow_x_hdr_auth:
		x_hdr_name = conf.get_str("CALLS", "x_header_name", "x-inboundip")
		auth_x_hdr_name = "sip_" + x_hdr_name
		x_hdr = route_msg.params.get(auth_x_hdr_name, "")
		if x_hdr != "":
			rad_params["Cisco-AVPair"] += ["h323-ivr-out=auth-by-x-header:" + x_hdr_name]
			rad_params["Cisco-AVPair"] += ["h323-ivr-out=x-header-value:" + x_hdr]

	# P-asserted-Identity
	p_asserted_id = route_msg.params.get("sip_p-asserted-identity", "")
	if p_asserted_id != "":
		rad_params["Cisco-AVPair"] += ["h323-ivr-out=p-asserted-id:" + p_asserted_id]

	# determine terminating account of the call
	if cdr_data['chan'].startswith("sip/"):
		uri = 'sip/' + cdr_data['cdr_called'] + '@' + cdr_data['called_addr']

		try:
			for yroute in call.yate_routes:
				if "term_acct_id" not in call.yate_routes[yroute]:
					continue
				if len(yroute.split(":")) < 3 :
					ruri = yroute + ":5060"
				else:
					ruri = yroute
				part_ip = utils.ip_from_uri(ruri)
				part_host = utils.host_from_uri(ruri)
				# if the call URI was hostname, convert it to IP to match results from call.cdr message
				if part_ip != part_host:
					ruri = ruri.replace(part_host, part_ip)

				if ruri == uri:
					rad_params['Cisco-AVPair'] += ["h323-ivr-out=term-acct-id:" + call.yate_routes[yroute]["term_acct_id"]]
		except:
			pass

	# dispatch RADIUS Accounting-Request packet
	slog.info("RADIUS accounting stop request:", rad_params)
	radius.queue_acct(rad_params)
