"""
Security related procedures
"""

import time
from aswitch import stats
from wobble import slog

STATS_SECTION = "security"
IPHOST_KEY = "iphosts"

def log_reject(msg):
    """Logs a rejected auth message"""

    timestamp = time.time()
    hour = timestamp - timestamp % 3600

    ip_host = msg.params.get("ip_host", "")
    value = stats.get_list(STATS_SECTION, IPHOST_KEY, ip_host)

    if isinstance(value, list):
        first_time, last_time, count, min_int = value
        if count < 2:
            min_int = timestamp - last_time
        elif timestamp - last_time < min_int:
            min_int = timestamp - last_time
        new_val = [first_time, timestamp, count+1, min_int]
        stats.set_list(STATS_SECTION, IPHOST_KEY, ip_host, new_val)
    else:
        new_val = [timestamp, timestamp, 1, False]
        stats.set_list(STATS_SECTION, IPHOST_KEY, ip_host, new_val)
