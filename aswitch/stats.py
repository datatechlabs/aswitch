"""
Statistics accumulation module
"""

from wobble import conf, slog

def get_list(section, param, key):

    param = param.replace(".","_")
    key = key.replace(".", "_")
    attr = section + ":" + param + ":" + key

    try:
        res = conf.get_list("STATS", attr, ",", None)
    except:
        res = None
    return res


def get_all_lists(section, param):

    param = param.replace(".", "_")

    try:
        keys = conf.get_list("STATS", section + ":" + param + ":KEYS")
    except:
        slog.info("stats list ", param, " is empty")
        return None
    res = {}
    for k in keys:
        row = conf.get_list("STATS", section + ":" + param + ":" + k, ",", None)
        res[k] = row

    return res

def set_list(section, param, key, value):

    param = param.replace(".", "_")
    key = key.replace(".", "_")
    attr = section + ":" + param + ":" + key

    # check if need to append to keys list
    try:
        keys = conf.get_list("STATS", section + ":" + param + ":KEYS")
    except:
        keys = []

    if key not in keys:
        keys.append(key)
        conf.set_value("STATS", section + ":" + param + ":KEYS", keys)

    conf.set_value("STATS", attr, value)


