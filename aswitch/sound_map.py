"""
IVR Sound file mapping
"""

# src_type_id: call_type: call_origin: ([src_ip], [dest_ip])

scheme = {
	
"ast": {
	"welcome":					"welcome",
	"astcc-please-enter-your":	"astcc-please-enter-your",
	"card-number":				"card-number",
	"astcc-followed-by-pound":	"astcc-followed-by-pound",
	"card-balance-is":			"card-balance-is",
	"vm-enter-num-to-call": 	"vm-enter-num-to-call",
	"vm-then-pound": 			"vm-then-pound",
	"silence/1":				"silence/1",
	"vm-youhave":				"vm-youhave",
	"this-call-will-cost":		"this-call-will-cost",
	"cents-per-minute":			"cents-per-minute",
	"no-route-exists-to-dest":	"no-route-exists-to-dest",
	"check-number-dial-again":	"check-number-dial-again",
	"goodbye":					"goodbye",
	"not-enough-credit":		"not-enough-credit",
	"vm-no":					"vm-no", 
	"number":					"number", 
	"vm-received":				"vm-received",
	"login-fail":				"login-fail"
},


"cisco": {
	"welcome":					None,
	"enter-account":			"enter_account",		#
	"enter-pin":				"enter_pin",			#	
	"card-number":				"card-number",		
	"astcc-followed-by-pound":	None,
	"card-balance-is":			"your_balance_is",   #
	"enter-destination": 		"enter_destination", #
	"invalid-account":			"invalid_pin",		#
	"invalid-pin":				"invalid_pin",		#
	"destin-busy":				"destin_busy",
	"vm-then-pound": 			None,
	"silence/1":				"silence/1",
	"you-have":					"you_have",			#
	"this-call-will-cost":		None,
	"cents-per-minute":			None,
	"no-route-exists-to-dest":	None,
	"still-waiting":			"still_waiting",		#
	"goodbye":					"thanks",			#
	"not-enough-credit":		"not_enough_money",
	"no-money":					"no_money",
	"in-use":					"in_use",			#
	"vm-no":					None,
	"number": 					None,
	"vm-received":				None,
	"invalid-destin":			"invalid_destin", #
	"login-fail":				"thanks",			#
	"account-expired":			"expired_pin",
	"dollar":					"dollar",	#
	"dollars":					"dollars",	#
	"pound":					"pound",
	"pounds":					"pounds",
	"pence":					"pence",
	"euro":						"euro",
	"euros":					"euros",
	"cents":					"cents",
	"digits/0":					"zero",
	"digits/1":					"one",
	"digits/2":					"two",
	"digits/3":					"three",
	"digits/4":					"four",
	"digits/5":					"five",
	"digits/6":					"six",
	"digits/7":					"seven",
	"digits/8":					"eight",
	"digits/9":					"nine",
	"digits/10":				"ten",
	"digits/11":				"eleven",
	"digits/12":				"twelve",
	"digits/13":				"thirteen",
	"digits/14":				"fourteen",
	"digits/15":				"fifteen",
	"digits/16":				"sixteen",
	"digits/17":				"seventeen",
	"digits/18":				"eighteen",
	"digits/19":				"nineteen",
	"digits/20":				"twenty",
	"digits/30":				"thirty",
	"digits/40":				"forty",
	"digits/50":				"fifty",
	"digits/60":				"sixty",
	"digits/70":				"seventy",
	"digits/80":				"eighty",
	"digits/90":				"ninety",
	"digits/100":				"hundred",
	"digits/hundred":			"hundred",
	"digits/1000":				"thousand",
	"digits/thousand":			"thousand",
	"digits/1-and":				"1_and",  # only DE
	"digits/2-and":				"2_and",  # only DE
	"digits/3-and":				"3_and",  # only DE
	"digits/4-and":				"4_and",  # only DE
	"digits/5-and":				"5_and",  # only DE
	"digits/6-and":				"6_and",  # only DE
	"digits/7-and":				"7_and",  # only DE
	"digits/8-and":				"8_and",  # only DE
	"digits/9-and":				"9_and",  # only DE
	"minute":					"minute",
	"minutes":					"minutes",
	"hour":						"hour",
	"hours":					"hours",
	"and":						"and",
	"rec-name":					"pls-rcrd-name-at-tone",
	"beep":						"beep",
	"low-balance":				"low_balance",
	"conf-thereare":			"conf-thereare",      # there are currently
	"conf-peopleinconf":		"conf-peopleinconf",  # people in this conference
	"conf-hasentered":			"conf-hasentered",    # has entered the conference
	"conf-hasjoin":				"conf-hasjoin",       # ... is now in the conference
	"conf-hasleft":				"conf-hasleft",       # ... has left the conf
	"conf-onlypersonleft":		"conf-onlypersonleft",# you are only person left in this conf
	"conf-onlyperson":			"conf-onlyperson",    # you are currently the only person in this conf
	"available-options":		"available-options",
	"press-1":					"press-1",
	"press-2":					"press-2",
	"please-enter-the":			"please-enter-the",   # please enter the
	"number":					"number",
	"number-to-add":			"number-to-add",      # number to add
	"number-to-recharge":		"number-to-recharge", # number to recharge
	"you-entered":				"you-entered",        # you entered
	"1-yes-2-no":				"1-yes-2-no",         # press 1 for yes 2 for no
	"added":					"added",              # added
	"activated":				"activated",          # activated
	"num-was-successfully":		"num-was-successfully", # number was successfully
	"an-error-has-occurred":	"an-error-has-occurred", # an error has occurred
	"enter-amount":				"enter-amount",       # enter amount followed by #
	"or-press-0-for-menu":		"fsm/or-press-0-for-menu",
	"or-press-1-for-menu":		"fsm/or-press-1-for-menu",
	"press-1-to-recharge":		"fsm/press-1-to-recharge",
	"press-3-to-hear-balance":	"fsm/press-3-to-hear-balance",
	"press-star-to-exit":		"fsm/press-star-to-exit",
	"to-add-client-press-1":	"fsm/to-add-client-press-1",
	"to-add-money-press-2":		"fsm/to-add-money-press-2",
	"enter-number-to-add":		"fsm/enter-number-to-add",
	"enter-number-to-recharge":	"fsm/enter-number-to-recharge",
	"you-entered":				"fsm/you-entered",        # you entered	
	"to-save-press-1":			"fsm/to-save-press-1",
	"to-cancel-press-2":		"fsm/to-cancel-press-2",
	"phone-number-was-added":	"fsm/phone-number-was-added",
	"phone-number-was-recharged": "fsm/phone-number-was-recharged"
}

}
