"""
Route management which includes user authentication and registration
"""

from decimal import Decimal
import time, threading, traceback, re, json
from wobble import slog, conf, tools
from aswitch import calls, return_codes, radius, utils, yate, security
import bsdradius.pyrad.packet as packet
from bsdradius.DigestAuth import DigestAuth

class User:
	"""User."""
	
	def __str__(self):
		return str(self.__dict__)

	def __repr__(self):
		return repr(self.__dict__)

registered_users = {}
reguser_lock = threading.Lock()

digest_data = {}

# these are set by the main script
call_modules = None
control_module = None

web_callback_module = None
ani_callback_module = None

def init(cmods, ctlmod):
	global call_modules, control_module, web_callback_module, ani_callback_module
	
	call_modules = cmods
	control_module = ctlmod
	for mod in call_modules:
		if mod.__name__ == "web_callback":
			web_callback_module = mod
		if mod.__name__ == "ani_callback":
			ani_callback_module = mod

def auth(msg):
	"""Handle user.auth message."""
	
	# if RADIUS authentication disabled -> authenticate user without any
	# further questions
	if (not conf.get_bool("RADIUS", "enable")):
		msg.reverse("true")
		msg.send()
		return

	# check method
	sip_update_always_ok = conf.get_bool("CALLS", "sip_update_always_ok", False)

	method = msg.params.get("method", "").lower()
	if method == "update" and sip_update_always_ok:
		slog.info("Respond to SIP UPDATE")
		msg.reverse("true")
		msg.send()
		return

	# get username parameter
	username = msg.params.get("username", "")
	if username == "":
		ip_host = msg.params.get("ip_host", "")
		method = msg.params.get("method", "")
		if method == "REGISTER":
			slog.warn("Cannot register unknown user from", ip_host)
		msg.reverse("false")
		msg.send()
		return
	
	# remove port number from IP address
	address = msg.params.get("address", "").split(':')[0]

	# prepare RADIUS auth request
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"User-Name": username,
		"Framed-IP-Address": address,
		"Calling-Station-Id": username
	}

	# put plain text user password if it exists
	if "password" in msg.params:
		rad_params["User-Password"] = [msg.params["password"]]
	else:
		rad_params["User-Password"] = [username]

	# put digest attibutes in request
	digestAttrs = None
	digestResponse = None
	if ("nonce" in msg.params) and ("response" in msg.params):
		slog.info("Trying DIGEST authentication.")
		
		# create list of digest attributes for radius
		authHandler = DigestAuth(users={})
		if "realm" in msg.params:
			authHandler.realm = msg.params["realm"]
		if "uri" in msg.params:
			authHandler.uri = msg.params["uri"]
		if "method" in msg.params:
			authHandler.method = msg.params["method"]
		authHandler.params["nonce"] = msg.params["nonce"]
		authHandler.params["username"] = username
		
		# parse attributes and add to radius message
		digestAttrs = authHandler.makeDigestAttributes()
		rad_params["Digest-Attributes"] = digestAttrs
		# TODO: what about algorithm? MD5 forever?
			
		# add digest response
		digestResponse = msg.params["response"]
		rad_params["Digest-Response"] = digestResponse

	args = (msg, digestAttrs, digestResponse)
	radius.queue_auth(rad_params, radius_auth_cb, args)

def radius_auth_cb(rad_result, msg, digestAttrs, digestResponse):
	"""Authorization callback."""
	
	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		address = msg.params.get("ip_host", "").split(':')[0]
		if address == "":
			slog.warn("RADIUS authentication unsuccessful.")
		else:
			slog.warn("RADIUS authentication unsuccessful for host", address)
			security.log_reject(msg)
		msg.reverse()
		msg.send()
		return

	username = msg.params.get("username", "")
		
	# see what we got
	slog.info("RADIUS authentication successful.")
	slog.verb("Answer from RADIUS server:", str(rad_result))
	
	if (digestAttrs != None) and (digestResponse != None):
		exp_timeout = 3600
		
		# store digest attributes for later use
		user_digest = {}
		user_digest["digest_response"] = digestResponse
		user_digest["digest_attrs"] = digestAttrs
		user_digest["exp_timeout"] = exp_timeout
		user_digest["exp_timestamp"] = time.time() + exp_timeout
		
		digest_data[username] = user_digest

	# marking the message as handled tells yate we authenticated the caller
	msg.reverse("true")
	msg.send()

def register(msg):
	"""Handle user.register message."""
	
	user = User()

	if "number" in msg.params:
		user.name = msg.params["number"]
	elif "username" in msg.params:
		user.name = msg.params["username"]
	else:
		slog.err("Unrecognized user.register message.")
		msg.reverse()
		msg.send()
		return

	user.location = msg.params["data"]

	user.ip_host = utils.ip_from_uri(user.location)
	if user.ip_host == None:
		slog.warn("Unable to extract IP address."
			" Registration failed.")
		msg.reverse()
		msg.send()
		return

	user.expires = time.time() + int(msg.params["expires"])

	overwrite_ip = conf.get_bool("CALLS", "overwrite_ip_on_register", True)

	# check if another active user with the same, but a different IP
	# address, hasn't already registered
	prev_user = registered_users.get(user.name)
	if prev_user != None:
		now = time.time()
		if (prev_user.expires > now):
			if (prev_user.ip_host != user.ip_host) and (overwrite_ip == False):
				slog.warn("User with the same number, but a different"
					" IP address, is already registered. Registration"
					" failed.")
				msg.reverse()
				msg.send()
				return

	slog.info("Registering user", user)
	
	reguser_lock.acquire()
	registered_users[user.name] = user
	reguser_lock.release()

	msg.reverse("true")
	msg.send()

def unregister(msg):
	"""Handle user.unregister message."""
	
	if "number" in msg.params:
		name = msg.params["number"]
	elif "username" in msg.params:
		name = msg.params["username"]
	else:
		slog.err("Unrecognized user.unregister message.")
		msg.reverse()
		msg.send()
		return

	reguser_lock.acquire()
	user = registered_users.get(name)

	# XXX authenticate before unregistering?

	if user != None:
		slog.info("Unregistering user", user)
		del registered_users[name]
	else:
		slog.warn("User not found, cannot unregister.")

	reguser_lock.release()
	msg.reverse("true")
	msg.send()

def getuser(name, ip_host=None):
	slog.info("Checking if user with number", name, "is registered.")
		
	reguser_lock.acquire()
	user = registered_users.get(name)
	
	if user == None:
		slog.info("User not registered.")
		reguser_lock.release()
		return None

	if user.expires <= time.time():
		slog.info("User has expired.")
		del registered_users[name]
		reguser_lock.release()
		return None

	if ip_host and (user.ip_host != ip_host):
		slog.warn("User's IP address has changed.")
		reguser_lock.release()
		return None

	reguser_lock.release()
	slog.info("User data:", user)
	return user

# get user by ip:port
def getuser_by_location(location):
	slog.info("Checking if user at location", location, "is registered.")
		
	reguser_lock.acquire()
	for username in registered_users:
		user = registered_users[username]
		try:
			ip_port = user.location.split('@')[1]
		except:
			reguser_lock.release()
			return None
		if ip_port == location:
			reguser_lock.release()
			slog.info("User data:", user)
			return user

	reguser_lock.release()
	return None

def route_call(route_msg):
	"""Route the call to the appropriate call module.

	msg -- call.route Yate message
	"""
	slog.info("Routing call.")
	route_msg.reverse("true")
		
	# get caller and called numbers from yate message
	caller = route_msg.params.get("caller", "")
	called = route_msg.params.get("called", "")

	# remove port number
	caller_address = route_msg.params.get("address", "").split(':')[0]

	if not called:
		slog.warn("Called number missing in routing message.")
		route_msg.params["error"] = "Incomplete routing request"
		route_msg.params["reason"] = "Empty called number"
		route_msg.retvalue = ""
		route_msg.send()
		return

	# generate a call identifier for the RADIUS server
	h323_conf_id = radius.gen_h323_conf_id()
	route_msg.params["h323_conf_id"] = h323_conf_id
		
	if not conf.get_bool("CALLS", "allow_unregistered"):
		# must check if user has registered
		if not getuser(caller, caller_address):
			slog.warn("User", caller, "not registered, "
			    "disconnecting.")
			
			route_msg.params["error"] = "permission denied"
			route_msg.params["reason"] = "caller not registered"
			route_msg.retvalue = ""
			route_msg.send()
			return

	# look for a static route in configuration
	for modref in call_modules:
		modname = modref.__name__
		number = conf.get_str("CALLS", "mod_" + modname, "noroute")
		if called in number:
			slog.info("Static route found, call handled by",
			    modname, "module")
			return calls.start(modref, route_msg)

		# regex match
		numlist = number.split(",")
		for number in numlist:
			rnum = re.compile("^" + number)
			if rnum.match(called):
				slog.info("Static route found by regexmatch, call handled by",
				    modname, "module")
				return calls.start(modref, route_msg)
			
	# XXX Routing for WEB callback module
	if web_callback_module != None:
		# find call by ID for callback module (comes as "callername"
		# parameter from call.execute message)
		callername = route_msg.params.get("callername", "")
		call = calls.find_by_id(callername)
		if call != None:
			web_callback_module.call_route(call, route_msg)
			return

		# look for callback initialization messages
		if callername.startswith("web_callback_"):
			chan = route_msg.params["id"]
			return calls.start(web_callback_module, route_msg)

	# XXX Routing for ANI callback module
	if ani_callback_module != None:
		# find call by ID for callback module (comes as "callername"
		# parameter from call.execute message)
		callername = route_msg.params.get("callername", "")
		call = calls.find_by_id(callername)
		if call != None:
			ani_callback_module.call_route(call, route_msg)
			return

		# look for callback initialization messages
		if callername.startswith("ani_callback_"):
			chan = route_msg.params["id"]
			return calls.start(ani_callback_module, route_msg)

	# see if this call was made from Yate itself -- in which case we
	# immediately provide a local route (if there is one, of course)
	called_user = getuser(called)
	if (called_user) and \
	    (caller_address == conf.get_str("SERVER", "ip_address")):
		slog.info("Incoming call from aswitch routed locally to",
		    called)
		route_msg.retvalue = called_user.location
		route_msg.send()
		return

	# pass this call to the main control module
	slog.info("Call passed to main control module",
	    control_module.__name__)
	calls.start(control_module, route_msg)

def route_callback(exec_msg):
	"""Find out routes for a callback request.
	"""
	if (exec_msg.params.get("callto") != "web_callback" and exec_msg.params.get("callto") != "ani_callback"):
		slog.warn("call.execute message was not a callback initiation "
		    "request and could not be handled in any other way.")
		exec_msg.send()
		return

	target = exec_msg.params.get("target", "")
	number_tuple = target.split(':')

	if len(number_tuple) != 3 and len(number_tuple) != 4:
		slog.err("Callback call.execute target parameter should "
		    "contain three or four numbers (caller:callback:called[:password]).")
		exec_msg.send()
		return

	if len(number_tuple) == 3: 
		(caller_number, cb_number, called_number) = number_tuple
	elif len(number_tuple) == 4:
		(caller_number, cb_number, called_number, password) = number_tuple
	# invoke proper module
	module_name = exec_msg.params.get("callto")

	# call the caller on his callback number
	exec_msg = yate.Message("call.execute")
	exec_msg.params["target"] = cb_number
	exec_msg.params["callto"] = "dumb/"
	exec_msg.params["callername"] = module_name + "_" + str(tools.unique_id())
	exec_msg.params["cb_numbers"] = target
	if len(number_tuple) == 4:
		exec_msg.params["caller"] = caller_number + ":" + password
	else:
		exec_msg.params["caller"] = caller_number
	exec_msg.send()

def authorize_call(call, route_msg, auth_cb=None):
	"""Authorize call."""

	if auth_cb == None:
		auth_cb = authorization_cb
	
	# get call attributes
	call.auth_user = route_msg.params.get("caller", "")
	called = route_msg.params.get("called", "")
	caller_address = route_msg.params.get("address", "")
	sip_contact = route_msg.params.get("sip_contact", "")
	sip_number = utils.number_from_sipuri(sip_contact)
	if conf.get_bool("CALLS", "use_contact_as_clip", False):
		caller_id = route_msg.params.get("callername", "")
		# this sometimes fails. use sip_contact then
		if caller_id == "" and sip_number != "":
			caller_id = sip_number
	else:
		caller_id = call.auth_user
	# store for later use in call cdr
	call.dialed_number = called

	chan = route_msg.params.get("id","")
	h323_conf_id = calls.conf_ids[chan]
	# save h323confid for use in first leg accounting request
	slog.info("Saving call.confid value",h323_conf_id)
	call.confid = h323_conf_id

	# remove port number from address
	caller_address = caller_address.split(':')[0]

	# some RADIUS configuration
	radius_enabled = conf.get_bool("RADIUS", "enable")
	routing_enabled = conf.get_bool("RADIUS", "routing")

	# prepare RADIUS auth request
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"User-Name": call.auth_user,
		"Framed-IP-Address": caller_address,
		"Calling-Station-Id": caller_id,
		"Called-Station-Id": called,
		"h323-conf-id": h323_conf_id
	}

	# add password if available
	if "password" in route_msg.params:
		rad_params["User-Password"] = [route_msg.params["password"]]
	else:
		rad_params["User-Password"] = [call.auth_user]

	# add digest stuff, if available for this user
	user_digest = digest_data.get(call.auth_user)
	if user_digest != None:
		rad_params['Digest-Attributes'] = user_digest['digest_attrs']
		rad_params['Digest-Response'] = user_digest['digest_response']
		# refresh timeout timestamp
		user_digest['exp_timestamp'] = time.time() + \
		    user_digest['exp_timeout']

	rad_params["Cisco-AVPair"] = []
	# add X-headers for authorization if allowed
	allow_x_hdr_auth = conf.get_bool("CALLS", "allow_x_header_auth", False)
	if allow_x_hdr_auth:
		x_hdr_name = conf.get_str("CALLS", "x_header_name", "x-inboundip")
		auth_x_hdr_name = "sip_" + x_hdr_name
		x_hdr = route_msg.params.get(auth_x_hdr_name, "")
		if x_hdr != "":
			rad_params["Cisco-AVPair"].append("h323-ivr-out=auth-by-x-header:" + x_hdr_name)
			rad_params["Cisco-AVPair"].append("h323-ivr-out=x-header-value:" + x_hdr)

	# pass P-asserted-ID if present
	p_asserted_id = route_msg.params.get("sip_p-asserted-identity", "")
	if p_asserted_id != "":
		rad_params["Cisco-AVPair"] += ["h323-ivr-out=p-asserted-id:" + p_asserted_id]

	# check if we've got a local route
	called_user = getuser(called)
	if not called_user:
		slog.info("Called number not locally registered.")
		if (not radius_enabled) or (not routing_enabled):
			reject(route_msg, "No routes found", 404)
			return

		# add routing-request attribute
		slog.info("Adding routing-request attribute to RADIUS packet.")

		rad_params["Cisco-AVPair"].append("xpgk-request-type=route")
		rad_params["Cisco-AVPair"].append("xpgk-routing-request=1")

		# this could be route request only for ANI callback
		if hasattr(call,"aleg_route"):
			del call.aleg_route
			rad_params["Cisco-AVPair"].append("h323-ivr-out=DTL-Radius-Command:Request-ANI-CB-Route")

	else:
		if (not radius_enabled):
			slog.info("Using a local route:", called_user.location)
			route_msg.retvalue = called_user.location
			route_msg.send()
			return

	calls.lock(call)
	args = (call, route_msg)
	if not radius.queue_auth(rad_params, auth_cb, args):
		calls.unlock(call)
		# throttling
		reject(route_msg, "Server Timeout", 504)

def authorization_cb(rad_result, call, route_msg):
	"""Authorization callback (RADIUS thread calls this)."""
	
	try:
		proc_authorization(rad_result, call, route_msg)
	except:
		slog.err(traceback.format_exc())

	calls.unlock(call)

def proc_authorization(rad_result, call, route_msg):
	"""Process authorization response."""

	called = route_msg.params.get("called", "")
	# store it, we will need it later
	orig_called = route_msg.params.get("called", "")
	
	if rad_result == None:
		# radius server does not reply
		reject(route_msg, "Server Timeout", 504)
		return

	if rad_result.code != packet.AccessAccept:
		ip_host = route_msg.params.get("ip_host", "")

		# return error message corresponding to h323 return code
		h323RetCode = rad_result.get("h323-return-code",
		    [return_codes.H323_UNKNOWN])[0]
		if isinstance(h323RetCode, str):
			ret_code = int(h323RetCode.split('=')[1])
			slog.warn("RADIUS authorization unsuccessful for host", ip_host, "result code:", ret_code)
		errstr = return_codes.h323CodeToSipResponse.get(h323RetCode, "")
		errcode = return_codes.h323CodeToSipCode.get(h323RetCode, "")
		reject(route_msg, errstr, errcode)
		return

	slog.info("RADIUS authorization successful.")

	# get session timeout
	max_duration = Decimal(conf.get_str("CALLS", "max_duration"))
	session_timeout = rad_result.get("Session-Timeout", [max_duration])[0]
	session_timeout = Decimal(session_timeout)
	if session_timeout > max_duration:
		session_timeout = max_duration
	slog.info("Session timeout:", str(session_timeout))

	# check if requested to rewrite called number (due to speeddial for example)
	call_rate = None
	unauth_flag = False
	if "Cisco-AVPair" in rad_result:
		for avpairs in rad_result["Cisco-AVPair"]:
			parts = avpairs.split("=")
			cld = re.compile("^called-number:(.+)")
			cmd = re.compile("^authenticated:(.+)")
			iv = re.compile("^ivr-options:(.+)")
			mcld = cld.match(parts[1])
			mcmd = cmd.match(parts[1])
			miv = iv.match(parts[1])
			if mcld:
				slog.info("Override called number: ", mcld.group(1))
				route_msg.params["called_original"] = route_msg.params["called"]
				route_msg.params["called"] = mcld.group(1)
				called = mcld.group(1)
				call.dialed_number = mcld.group(1)
			if mcmd:
				if mcmd.group(1) == "false":
					slog.warn("Unauthenticated caller")
					unauth_flag = True
			if miv:
				slog.info("IVR options: ", miv.group(1))
				call.ivr_options = miv.group(1)

	# extract balance
	balance_attr = rad_result.get("h323-credit-amount", [None])[0]
	if balance_attr == None:
		slog.err("Could not extract h323-credit-amount from "
		    "auth response")
		call.balance = 0
	else:
		balance_split = balance_attr.split('=')
		if len(balance_split) != 2:
			slog.err("Invalid balance string:", balance_attr)
			call.balance = 0
		else:
			call.balance = balance_split[1]
			slog.info("Balance:", call.balance)

	# extract currency
	currency_attr = rad_result.get("h323-currency", [None])[0]
	if currency_attr == None:
		slog.warn("Could not extract h323-currency from auth "
		     "response, setting to USD.")
		call.currency = "USD"
	else:
		currency_split = currency_attr.split('=')
		if len(currency_split) != 2:
			slog.warn("Invalid currency string", currency_attr, "setting to USD")
			call.currency = "USD"
		else:
			call.currency = currency_split[1].upper()
			slog.info("Currency:", call.currency)

	# ivr reported remaining seconds
	if "h323-credit-time" in rad_result:
		ivr_sec = rad_result["h323-credit-time"][0]
		ivr_sec = int(ivr_sec.split('=')[1])
	else:
		ivr_sec = max_duration
	slog.info("IVR Remaining seconds:", ivr_sec)
	call.ivr_time = ivr_sec

	# get preferred language
	if "h323-preferred-lang" in rad_result:
		ivr_lang = rad_result["h323-preferred-lang"][0]
		ivr_lang = ivr_lang.split('=')[1]
	else:
		ivr_lang = "en"
	slog.info("Preferred language:", ivr_lang)
	call.ivr_lang = ivr_lang

	# add session timeout in millisecs
	# Yate incorrectly adds setup time to call duration time, therefore
	# we have to maintain call timers locally and disconnect when timers are due
	# here we add some guard time to take in account setup time (120 sec ok?)
	
	session_timeout *= 1000
	call.timeout = float(session_timeout)
	
	yate_timeout = session_timeout + 120000
	route_msg.params["timeout"] = str(yate_timeout)

	# send route to local user if possible
	called_user = getuser(called)
	if called_user:
		slog.info("Routing call to local user:", called_user.location)

		# static caller ID modifications for local route
		caller = route_msg.params.get("caller", "")
		caller_rule = conf.get_str("CALLS", "local_caller_rule", "")
		rules = caller_rule.split(",")
		for rule in rules:
			try:
				(pattern, rewrite) = rule.split("=")
				slog.info("caller: pattern:",pattern," rewrite:",rewrite,"subject:",caller)
				modcaller = re.sub(pattern, rewrite, caller)
				caller = modcaller
				slog.info("rewritten to ",caller)
			except:
				pass
		route_msg.retvalue = called_user.location
		route_msg.params["callername"] = caller
		route_msg.params["caller"] = caller
		if conf.get_bool("CALLS", "use_original_called", False):
			route_msg.params["calledname"] = orig_called
		route_msg.send()
		return

	slog.info("Called number", called, "not locally registered; "
	    "trying to get route.")
	if not rad_result.has_key("Cisco-Command-Code"):
		slog.info("Missing Cisco-Command-Code attribute in RADIUS"
		    " reply.")
		reject(route_msg, "Route Not Found", 404)
		return

	yate_routes = []
	yate_routes_dict = {}
	routing_attrs = rad_result["Cisco-Command-Code"]

	route_no = 1
	callername = route_msg.params.get("callername", "")
	for cmd_code in routing_attrs:
		# protocol:gateway/proxymode/sourcenumber /destnumber-out/ / /dest-ip/formats/term-acct-id/SIPheaders
		# 0               /1        /2            /3             /4/5/6      /7      /8           /9
		tokens = cmd_code.split("/")
		if len(tokens) < 7:
			slog.warn("Invalid Cisco-Command-Code attribute "
				"format.")
			continue
	
		gateway = tokens[0]
		subtokens = gateway.split(":")
		protocol = subtokens[0].strip().lower()

		if protocol == "h.323":
			protocol = "h323"

		if protocol not in ("sip", "h323"):
			slog.warn("Invalid protocol:", protocol)
			continue

		dest_number = tokens[3]
		dest_number_digits = tokens[3]
		dest_ip = tokens[6]

		# add "sip:" prefix to called number for SIP
		if protocol == "sip":
			dest_number = "sip:" + dest_number

		# try to find if target is registered user 
		user = getuser(dest_ip)
		if user != None:
			# extract port number 
			loc = str(user.location).split('@')
			dest_ip = loc[1]
			
		# create a yate route -- module/number@ip
		suffix = conf.get_str("CALLS", "uri_suffix", "")
		yroute = protocol + '/' + dest_number + '@' + dest_ip + suffix
		
		# append line (account) information
		if len(subtokens) > 1:
			account = subtokens[1].strip()
			line = account + '@' + dest_ip
		else:
			line = None
			
		n_tokens = len(tokens)

		r_attrs = {
			'callto': yroute,
			'caller': tokens[2],
			'called': dest_number,
			'callername': tokens[2],
			'domain': tokens[6],
			'line': line,
			'formats': tokens[7]
		}
		if n_tokens > 8:
			r_attrs['term_acct_id'] = tokens[8]

		#append privacy headers
		p_asserted_id = route_msg.params.get("sip_p-asserted-identity", "")
		sip_privacy = route_msg.params.get("sip_privacy", "")

		if p_asserted_id:
			r_attrs['sip_p-asserted-identity'] = p_asserted_id
			route_msg.params['osip_p-asserted-identity'] = p_asserted_id

		if sip_privacy:
			r_attrs['sip_privacy'] = sip_privacy
			route_msg.params['osip_privacy'] = sip_privacy

		# check if we need to apply PAI from contact header
		if conf.get_bool("CALLS", "header_pai_from_contact_enable", False):
			pai_pattern = conf.get_str("CALLS", "header_pai_pattern", "<tel:{caller};cpc=ordinary>")
			pai = pai_pattern.replace('{caller}', route_msg.params.get("caller", ""))
			r_attrs['sip_P-Asserted-Identity'] = pai
			route_msg.params['osip_P-Asserted-Identity'] = pai
			r_attrs['sip_Privacy'] = 'none'
			route_msg.params['osip_Privacy'] = 'none'

		# append Session-Expires and Min-SE headers
		se_header_enable = conf.get_bool("CALLS", "header_session_expires_enable", False)
		se_header_value = conf.get_int("CALLS", "header_session_expires_value", 1800)
		if se_header_enable:
			r_attrs['sip_Session-Expires'] = str(se_header_value) + ';refresher=uac'
			route_msg.params['osip_Session-Expires'] = str(se_header_value) + ';refresher=uac'

		min_se_header_enable = conf.get_bool("CALLS", "header_min_se_enable", False)
		min_se_header_value = conf.get_int("CALLS", "header_min_se_value", 600)
		if min_se_header_enable:
			r_attrs['sip_Min-SE'] = min_se_header_value
			route_msg.params['osip_Min-SE'] = min_se_header_value

		# append Expires header
		expires_enable = conf.get_bool("CALLS", "header_expires_enable", False)
		expires_value = conf.get_int("CALLS", "header_expires_value", 1800)
		if expires_enable:
			r_attrs['sip_Expires'] = str(expires_value)
			route_msg.params['osip_Expires'] = str(expires_value)

		# append P-Early-Media header
		p_early_media_header = conf.get_bool("CALLS", "header_p_early_media", False)
		if p_early_media_header:
			r_attrs['sip_P-Early-Media'] = 'supported'
			route_msg.params['osip_P-Early-Media'] = 'supported'

		# modify From
		if conf.get_bool("CALLS", "header_from_modify", False):
			from_pattern = conf.get_str("CALLS", "header_from_pattern", "<{sip_from};user=phone>")
			sip_from = route_msg.params.get("sip_from", "")
			# we probably need to overwrite FROM header with the value returned by RADIUS in any case?
			host = utils.host_from_uri(sip_from)
			sip_from = r_attrs['caller'] + '@' + host

			from_value = from_pattern.replace('{sip_from}', re.sub('[<>]', '', sip_from))
			route_msg.params['osip_From'] = from_value
			r_attrs['sip_From'] = from_value

		# modify To
		if conf.get_bool("CALLS", "header_to_modify", False):
			to_pattern = conf.get_str("CALLS", "header_to_pattern", "\"{dest_number_digits[6:]}\"<sip:{dest_number_digits[6:]}@{dest_ip};user=phone>")
			sip_to = route_msg.params.get("sip_to", "")
			to_value = to_pattern.replace('{sip_to}', re.sub('[<>]', '', sip_to))
			slog.info("trying to find pattern in", to_pattern)
			dest_num_patt = re.search("\{(dest_number_digits)\[?(\-?\d*)\:?(\-?\d*)\]?\}", to_pattern)
			if dest_num_patt and dest_num_patt.group():
				slog.info("matched ", dest_num_patt.group(0), dest_num_patt.group(1), dest_num_patt.group(2))
				if dest_num_patt.group(2):
					start = int(dest_num_patt.group(2))
					dest_number_digits = dest_number_digits[start:]

				to_value = to_value.replace(dest_num_patt.group(0), dest_number_digits)
			to_value = to_value.replace('{dest_ip}', dest_ip)
			route_msg.params['osip_To'] = to_value
			r_attrs['sip_To'] = to_value

		# Append headers requested by RADIUS
		if n_tokens > 9:
			slog.info(tokens[9])
			radius_headers = json.loads(tokens[9])
			for hd in radius_headers:
				slog.info("HEADERS", hd, "IN", radius_headers)
				r_attrs['sip_' + hd] = radius_headers[hd]
				route_msg.params['osip_' + hd] = radius_headers[hd]

		yate_routes_dict[yroute] = r_attrs

		yate_routes.append(yroute)
		route_no = route_no + 1

	slog.info("Yate routes:", yate_routes)

	if len(yate_routes) == 0:
		slog.warn("No valid routes from RADIUS.")
		reject(route_msg, "Route Not Found", 404)
		return

	if unauth_flag == False:
		call.authenticated = True
	else:
		call.authenticated = False

	call.yate_routes = yate_routes_dict

	if len(yate_routes) == 1:
		# single route
		route_msg.retvalue = yate_routes[0]
		route_msg.params["caller"] = tokens[2]
		route_msg.params["called"] = dest_number
		if len(subtokens) > 1:
			account = subtokens[1].strip()
			dest_ip = tokens[6]
			route_msg.params["line"] = account + '@' + dest_ip
		route_msg.send()
		return

	# for description of yate's callfork module see
	# http://yate.null.ro/pmwiki/index.php?n=Main.Callfork
	forked_route = "fork " + " | ".join(yate_routes)

	slog.info("Forked route:", forked_route)
	slog.info("yate_routes", yate_routes)
	route_msg.retvalue = forked_route
	fork_stoperror = conf.get_str("CALLS", "fork_stoperror", "busy")
	route_msg.params["stoperror"] = fork_stoperror
	route_msg.send()

def reject(route_msg, reason="Server Error", error=500):
	route_msg.params["reason"] = reason
	route_msg.params["error"] = error
	route_msg.retvalue = ""
	route_msg.send()

def acct_register():

	# Yate address
	nas_ip_addr = conf.get_str("RADIUS", "nas_ip_address")

	rad_params = {
		"NAS-IP-Address": nas_ip_addr,
		"Cisco-AVPair": "h323-ivr-out=DTL-Radius-Command:Register-Carrier-Accounts"
	}
	
	# dispatch RADIUS Access-Request packet
	slog.info("RADIUS Register Carrier accounts message:", rad_params)
	radius.queue_auth(rad_params, acct_register_cb)

def acct_register_cb(rad_result, call):
	if rad_result == None:
		slog.err("No reply from RADIUS server")
		return 
	if not "Cisco-AVPair" in rad_result:
		slog.warn("Radius server does not support carrier registration request")
		return

	slog.info("Cisco-AVPair", rad_result["Cisco-AVPair"])
	for avpairs in rad_result["Cisco-AVPair"]:
		dpart = avpairs.split("=")[1]
		(i, host, username, passw) = dpart.split(":")
		host = host.replace("*",":")
		login_msg = yate.Message("user.login")
		login_msg.params["account"] = username + "@" + host
		login_msg.params["username"] = username
		login_msg.params["password"] = passw
		login_msg.params["domain"] = host
		login_msg.params["enabled"] = "yes"
		login_msg.params["protocol"] = "sip"
		login_msg.params["authname"] = username
		login_msg.params["number"] = username
		login_msg.params["registrar"] = host
		login_msg.params["outbound"] = host
		login_msg.send()

def get_radius_conf():

	# Yate address
	nas_ip_addr = conf.get_str("RADIUS", "nas_ip_address")

	rad_params = {
		"NAS-IP-Address": nas_ip_addr,
		"Cisco-AVPair": "h323-ivr-out=DTL-Radius-Command:Get-Switch-Config"
	}
	
	# dispatch RADIUS Access-Request packet
	slog.info("RADIUS Get switch config message:", rad_params)
	radius.queue_auth(rad_params, get_radius_conf_cb)

def get_radius_conf_cb(rad_result, call):
	if rad_result == None:
		slog.err("No reply from RADIUS server")
		return 
	if not "Cisco-AVPair" in rad_result:
		slog.warn("Radius server does not support config update message")
		return

	slog.info("Cisco-AVPair", rad_result["Cisco-AVPair"])
	for avpairs in rad_result["Cisco-AVPair"]:
		dpart = avpairs.split("=")[1]
		values = dpart.split(":")[1]
		try:
			(section, param, value) = values.split("|")
		except:
			slog.err("cannot parse radius config update message response")
			continue
		section = section.upper()
		param = param.lower()
		slog.info("section %s param %s value %s" % (section, param, value))
		# update module access numbers
		if section == 'CALLS' and param.startswith("mod_"):
			try:
				values = conf.get_str("CALLS", param)
				if value not in values:
					values+= ","
					values+= value
					conf.set_value(section, param, values)
			except:
				conf.set_value(section, param, value)
			# append to callthrough numbers
			cthrough = conf.get_str("CALLS", "call_through_numbers")
			if value not in cthrough:
				cthrough+= ","
				cthrough+= value
				conf.set_value("CALLS", "call_through_numbers", cthrough)

def accounting_update_status(upd_msg):
	# send accounting update message about carrier registration status
	nas_ip_addr = conf.get_str("RADIUS", "nas_ip_address")
	
	account = upd_msg.params.get("account","")
	account = account.replace(":","*")
	reg_status = upd_msg.params.get("registered","")
	reason = upd_msg.params.get("reason","unknown")

	rad_params = {
		"NAS-IP-Address": nas_ip_addr,
		"Acct-Status-Type": "Interim-Update",
		"Cisco-AVPair": "h323-ivr-out=UPDATETYPE:carrier-registration",
		"Cisco-AVPair": "h323-ivr-out=ACCOUNT:" + account,
		"Cisco-AVPair": "h323-ivr-out=STATUS:" + reg_status
	}
	
	rad_params['Cisco-AVPair'] = []
	rad_params['Cisco-AVPair'] += ["h323-ivr-out=UPDATETYPE:carrier-registration"]
	rad_params['Cisco-AVPair'] += ["h323-ivr-out=ACCOUNT:" + account]
	rad_params['Cisco-AVPair'] += ["h323-ivr-out=STATUS:" + reg_status]

	if reg_status == "false":
		rad_params['Cisco-AVPair'] += ["h323-ivr-out=REASON:" + reason]
	
	# dispatch RADIUS Accounting-Request packet
	slog.info("RADIUS accounting interim-update request:", rad_params)
	radius.queue_acct(rad_params)


def send_sip_update(call, msg):
	route_msg = call.route_msg
	update_msg = yate.Message("xsip.generate")
	update_msg.params["method"] = "UPDATE"
	# get rid of leading sip/ in request string
	req_string = route_msg.retvalue.replace("sip/sip:", "sip:")
	update_msg.params["uri"] = req_string
	update_msg.params["id"] = call.out_chan
	update_msg.send()

def send_sip_options():
	sip_options_urls = conf.get_list("CALLS", "sip_options_urls", ",", [])

	for url in sip_options_urls:
		options_msg = yate.Message("xsip.generate")
		options_msg.params["method"] = "OPTIONS"
		options_msg.params["uri"] = url
		options_msg.params["antiloop"] = "1"
		options_msg.send()
