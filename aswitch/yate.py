"""
Communication with Yate.
"""

import sys, socket, threading, select, errno, time, traceback, Queue
from wobble import slog, tools

engine = {'runid': 0}		# engine values (runid only supported now)

_sock = None		# communication socket with Yate
_conn_data = None	# connection info to the Yate server (IP, port, role)
_poll_thread = None	# the polling thread object
_cmd_queue = Queue.Queue() # command queue (the polling thread puts received
			   # lines here)
_keep_polling = True	# we set this to False to signal the polling thread to
			# stop operation
_send_lock = threading.Lock()	# prevent threads from sending data
				# simultaneously
_handlers = []		# store yate message handler registering strings

def connect(conn_type, ip_addr, port, path, role, rec_timeout):
	"""Connect to Yate."""

	global _conn_data

	# save connection data (we may need to reconnect later)
	_conn_data = (conn_type, ip_addr, port, path, role)

	# try connecting to Yate
	connected = _connect()

	if connected:
		return True
	
	if (not connected) and (rec_timeout == 0):
		return False
	
	reconnect(rec_timeout)
	return True

def _connect():
	"""Connect to Yate."""
	
	global _sock, _poll_thread, _conn_data

	(conn_type, ip_addr, port, path, role) = _conn_data

	slog.info("Connecting to yate..")
	slog.info("IP address", ip_addr, "port", port)

	# connect to Yate and send the initial "connect" command
	if conn_type == "tcp":
		try:
			_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			_sock.connect((ip_addr, port))
		except:
			slog.err("Connection to yate failed.")
			slog.err(traceback.format_exc())
			return False
	elif conn_type == "unix":
		try:
			_sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
			_sock.connect(path)
		except:
			slog.err("Connection to yate failed.")
			slog.err(traceback.format_exc())
			return False
	else:
		slog.err("Invalid connection type %s" % conn_type)
		return False
		
	if not send("%%>connect:" + role):
		slog.warn("Could not send initial connect message.")
		return False

	# start message receiver thread
	_poll_thread = threading.Thread(target=_receive_thread)
	_poll_thread.start()
	return True

def disconnect():
	"""Disconnect from Yate."""
	
	global _keep_polling
	
	slog.info("Disconnecting from yate..")
	_sock.close()

	if _poll_thread:
		# wait until our polling thread stops
		_keep_polling = False
		_poll_thread.join()	

def reconnect(timeout):
	"""Keep trying to reconnect every timeout seconds."""

	global _keep_polling
	_keep_polling = True

	while True:
		slog.info("Sleeping", timeout, "seconds before trying to"
		    " reconnect.")
		time.sleep(timeout)
		if _connect():
			# reinstall all message handlers
			slog.info("Reinstalling message handlers.")
			for handler in _handlers:
				send(handler)
			break
		
		slog.info("Connection failed.")

def send(line):
	"""Send data to Yate."""

	_send_lock.acquire()
	slog.verb("Sending line:", line)
	
	try:
		_sock.send(line + "\n")
	except:
		slog.warn("Sending data to yate failed.")
		slog.warn(traceback.format_exc())
		_send_lock.release()
		return False

	_send_lock.release()
	return True

def receive():
	"""Fetch queued data received from Yate, construct a message and
	return it. Returns only messages, ignores all other commands."""
	
	while True:
		try:
			line = _cmd_queue.get(timeout=10)
		except Queue.Empty:
			slog.warn("No data has been received from Yate within "
			    "10 seconds. Assuming we've been disconnected and "
			    "trying to reconnect.")
			#calls.shutdown_all()
			reconnect(5)
			continue

		# None means end of messages
		if line == None:
			return None
	
		if line[3:10] == "message":
			# create and return message
			return Message(raw=line)

		if line[3:11] == "setlocal":
			# create and return local setting
			return Setlocal(raw=line)

		# ignore the line
		continue

def _receive_thread():
	"""Receive data from Yate."""
	
	global _keep_polling

	goodflags = select.POLLIN | select.POLLPRI
	
	pollobj = select.poll()
	pollobj.register(_sock, goodflags)
	
	saved_str = ""
	while True:
		while True:
			try:
				# check if there's any data or if the thread
				# should stop
				fd_stat = pollobj.poll(300)
				if not _keep_polling:
					_sock.close()
					return
				if fd_stat:
					break
			except select.error, err:
				if err[0] == errno.EINTR:
					# signal end of lines
					slog.info("Interrupted poll syscall.")
					_cmd_queue.put(None)
					return
				else:
					slog.warn("Socket polling error.")
					slog.warn(traceback.format_exc())
					_sock.close()
					return
		
		# check socket status
		event = fd_stat[0][1]
		if not (event & goodflags):
			slog.warn("Socket error (invalid event).")
			_sock.close()
			return

		# read data
		try:
			data = _sock.recv(1024)
		except:
			slog.warn("Reading data from socket failed.")
			slog.warn(traceback.format_exc())
			_sock.close()
			return
			
		if not data:
			slog.warn("End of data from Yate.")
			_sock.close()
			return
		
		# process received data (queue the lines)
		pos1 = 0
		while True:
			# split up the data by newlines
			pos2 = data.find("\n", pos1)
			if pos2 == -1:
				# save received data (no newline yet!)
				saved_str += data[pos1:]
				break

			# extract line
			line = data[pos1:pos2]

			# append saved data if there is any
			if saved_str != "":
				line = saved_str + line
				saved_str = ""

			slog.verb("Received line:", line)

			# put data in queue
			_cmd_queue.put(line)
			
			pos1 = pos2 + 1

def install(name, priority=100):
	"""Install a message handler.
	
	%%>install:[<priority>]:<name>[:<filter-name>[:<filter-value>]]
	"""
	line = "%%>install:" + ("%i" % priority) + (":%s" % name)
	_handlers.append(line)
	send(line)

def watch(name):
	"""Install message watcher.

	%%>watch:<name>
	"""
	line = "%%>watch:" + name
	_handlers.append(line)
	send(line)

def setlocal(name):
	"""Request Local Parameter.

	%%>setlocal:<name>
	"""
	line = "%%>setlocal:" + name
	_handlers.append(line)
	send(line)

def encode(encode_me):
	"""Encode a string for transfer to Yate engine."""
	
	encode_me = str(encode_me)
	new_str = ""
	for c in encode_me:
		if (ord(c) < 32) or (c == ':'):
			new_str += '%' + chr(64 + ord(c))
		elif c == '%':
			new_str += '%%'
		else:
			new_str += c

	return new_str

def decode(decode_me):
	"""Decode a string received from Yate."""
	
	new_str = ""
	i = 0
	n = len(decode_me)
	while i < n:
		c = decode_me[i]
		if c == '%':
			i += 1
			c = decode_me[i]
			if c != '%':
				c = chr(ord(c) - 64)

		new_str += c
		i += 1

	return new_str

M_REQUEST = 0	# > message
M_ANSWER = 1	# < message
SL_REQUEST = 2	# > setlocal
SL_ANSWER = 3	# < setlocal

class Message:
	"""Yate message."""

	def __init__(self, name="unnamed", msg_type=M_REQUEST, raw=None):
		if raw == None:
			# create a new 'blank' message
			self.type = msg_type
			self.raw = ""
			if msg_type == M_REQUEST:
				self.keyword = "%%>message"
			else:
				self.keyword = "%%<message"
			self.id = tools.unique_id()
			self.time = int(time.time())
			self.name = name
			self.retvalue = ""
			self.params = {}
			
			return

		#
		# construct a message from its raw network
		# external module representation
		#
		
		self.raw = raw

		# split message line
		values = self.raw.split(':')

		# extract keyword
		self.keyword = values[0]

		if self.keyword == "%%>message":
			# request message from yate
			self.type = M_REQUEST
			self.id = decode(values[1])
			self.time = int(decode(values[2]))
			self.name = decode(values[3])
			self.retvalue = decode(values[4])
			self.params = {}

			for attr in values[5:]:
				key, value = attr.split('=', 1)
				key = decode(key)
				value = decode(value)
				self.params[key] = value
				
			return

		if self.keyword == "%%<message":
			# answer message from yate
			self.type = M_ANSWER
			self.id = decode(values[1])
			self.processed = decode(values[2])
			self.name = decode(values[3])
			self.retvalue = decode(values[4])
			self.params = {}

			for attr in values[5:]:
				key, value = attr.split('=', 1)
				key = decode(key)
				value = decode(value)
				self.params[key] = value

			return

		raise Exception("invalid keyword")

	def reverse(self, processed="false"):
		"""Reverse message direction by changing its type and
		keyword.
		"""
		if self.type == M_REQUEST:
			self.type = M_ANSWER
			self.keyword = "%%<message"
			self.processed = processed
			return
		
		if self.type == M_ANSWER:
			self.type = M_REQUEST
			self.keyword = "%%>message"
			return

		raise Exception("cannot reverse direction, unknown keyword")

	def send(self):
		"""Create a network representation of the message and send
		it to Yate.
		"""			
		if self.type == M_ANSWER:
			# we're sending an answer message
			# %%<message:<id>:<processed>:[<name>]:<retvalue>
			# [:<key>=<value>...]
			self.raw = "%%<message"
			self.raw += ':' + encode(self.id)
			self.raw += ':' + encode(self.processed)
			self.raw += ':' + encode(self.name)
			self.raw += ':' + encode(self.retvalue)
			
			for key, value in self.params.iteritems():
				self.raw += ':' + encode(key) + '=' + \
				    encode(value)
		elif self.type == M_REQUEST:
			# send message to engine for processing
			# %%>message:<id>:<time>:<name>:<retvalue>
			# [:<key>=<value>...]
			# fix if time is missing in incoming yate message
			if not hasattr(self, "time"):
				self.time = int(time.time())
			self.raw = "%%>message"
			self.raw += ':' + encode(self.id)
			self.raw += ':' + encode(self.time)
			self.raw += ':' + encode(self.name)
			self.raw += ':' + encode(self.retvalue)
			
			for key, value in self.params.iteritems():
				self.raw += ':' + encode(key) + '=' + \
				    encode(value)
		else:
			slog.err("Unknown message keyword.")
			raise Exception("unknown keyword")

		_log_message(self)
			
		send(self.raw)

	def __str__(self):
		output = "--- " + self.name + " ---\n"
		output += "retvalue: %s\n" % self.retvalue
		for k, v in self.params.iteritems():
			output += "%s: %s\n" % (k, v)

		return output

	def __repr__(self):
		return self.__str__()

def _log_message(msg):
	if (msg.name == "engine.timer") or (msg.name == "call.cdr"):
		slog.verb("Sending", msg.name, "message")
		slog.verb(msg)
		return
		
	slog.info("Sending", msg.name, "message")
	slog.info(msg)

class Setlocal:
	"""Yate setlocal command."""

	def __init__(self, name="unnamed", msg_type=SL_REQUEST, raw=None):
		if raw == None:
			# create a new 'blank' message
			self.type = msg_type
			self.raw = ""
			if msg_type == SL_REQUEST:
				self.keyword = "%%>setlocal"
			else:
				self.keyword = "%%<setlocal"
			self.id = tools.unique_id()
			self.time = int(time.time())
			self.name = name
			self.retvalue = ""
			self.params = {}
			
			return

		#
		# construct a message from its raw network
		# external module representation
		#
		
		self.raw = raw

		# split message line
		values = self.raw.split(':')

		# extract keyword
		self.keyword = values[0]

		if self.keyword == "%%<setlocal":
			# answer message from yate
			self.type = SL_ANSWER
			self.name = decode(values[1])  # engine.runid
			self.value = decode(values[2])
			self.status = decode(values[3])
			self.params = {}

			return

		raise Exception("invalid keyword")

def new_runid(newid):
	""" Set new runid value if it is different from old and remove all old 
		calls."""
	global engine	
	if engine['runid'] == 0:
		slog.info("Setting runid to ",newid)
		engine['runid'] = newid
		return False 

	if engine['runid'] == newid:
		slog.info("Runid not changed ",newid)
		return False

	slog.info("New runid ",newid," old: ",engine['runid'])
	engine['runid'] = newid
	return True

	
