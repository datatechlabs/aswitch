import socket
import select
import threading
import string
import time
import os
import pkg_resources
from wobble import slog, conf
from aswitch import routes, calls, utils, radius, stats

def init():
	bind_addr = conf.get_str("SERVER", "ip_address", "localhost")
	bind_port = conf.get_int("SERVER", "status_port", 7001)
	myServer=TelnetServer(bind_addr, bind_port)
	threading.Thread(target=myServer.run).start()
	

def shutdown():
	slog.info("Shutting down telnet server")
	conf.set_value("RUNTIME", "telnet_halt", True)

class TelnetServer:
	def __init__(self, host ,port):
		self.port = port;

		self.srvsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.srvsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		try:
			self.srvsock.bind((host, port))
		except:
			self.srvsock.bind(("0.0.0.0", port))
		self.srvsock.listen(5)
		self.descriptors = [self.srvsock]
		self.interactive = True
		slog.info("Telnet Server started on ", host,":", port)

	def run(self):
		while True:
			# check for kill signal
			flag = conf.get_bool("RUNTIME", "telnet_halt", False)
			if flag == True:
				return
			#Await an event on a readable socket descriptor 
			(sread, swrite, sexc) = select.select(self.descriptors,[],[], 1)
			#Iterate through the tagged readdescriptors 	
			for sock in sread:
			#Received a connect to the server (listening) socket		
				if sock == self.srvsock:
					self.accept_new_connection()
				else:
					if self.interactive == False:
						time.sleep(1)
						if self.run_monitor(sock) == False:
							self.interactive = True
					else:

						try:
							str = sock.recv(100)
						except:
							slog.warn("Telnet server socket read failed")
							continue
						#Check to see if the peer socket closed
						if str == '':
							self.interactive = True
							self.disconnect_client(sock)
						else:
							host,port=sock.getpeername()
							slog.verb("Telnet Server Received:", str, "from",host,":",port)
							self.parse_request(str, sock)

	def run_monitor(self, sock):
		thr = conf.get_int("STATS", "rad_throttle", 0)
		msg  = "calls | reg | radque | throttle\r\n"
		msg += "   %d |  %d |     %d |       %d\r\n" \
 % (len(calls.calls), len(routes.registered_users.keys()), radius.req_queue.qsize(), thr)
		if self.send_string(msg, sock) == False:
			sock.close
			self.descriptors.remove(sock)
			return False
		return True

	def send_string(self, msg, sock):
		try:
			sock.send(msg)
			slog.verb("Telnet Server Sending:", msg)
			return True
		except:
#			self.interactive = True
#			self.disconnect_client(sock)
			slog.err("Telnet Server closed socket")
			return False

	def broadcast_string(self, str, omit_sock):
		for sock in self.descriptors:
			if sock != self.srvsock and sock != omit_sock:
				sock.send(str)
			print str,

	def accept_new_connection(self):
		version = pkg_resources.require("aswitch")[0].version

		newsock,(remhost,remport) = self.srvsock.accept()
		self.descriptors.append(newsock)
		newsock.send("Connected to aswitch status port \r\n")
		newsock.send("aswitch version: %s \r\n" % (version))
		newsock.send("Press ? for command help \r\n")

		str = 'Client joined %s:%s\r\n' % (remhost, remport)
		slog.info("Telnet server new connection from ",remhost,":",remport)

	def disconnect_client(self, sock):
		slog.info("Telnet server client closed")
		try:
			sock.shutdown(1)
		except:
			slog.warn("telnet server socket shutdown exception")
		sock.close
		self.descriptors.remove(sock)
		
	def parse_request(self, msg, sock):
		msg =  str(msg)
		msg = msg.rstrip("\n")
		msg = msg.rstrip("\r")
		if msg == "":
			return

		if msg == "?" or msg == "h":
			msg = "Available commands:\n"
			msg += "  h          - command help\n"
			msg += "  r          - registered users\n"
			msg += "  c          - calls\n"
			msg += "  reload     - reload configuration\n"
			msg += "  rotatelog  - rotate log file\n"
			msg += "  mod[ules]  - list modules with DID numbers\n"
			msg += "  mon[itor]  - monitor mode\n"
			msg += "  rad[ius]   - radius statistics\n"
			msg += "  clearstats - clear all statistics\n"
			msg += "  stat[s]    - show call statistics\n"
			msg += "  sec        - show security statistics\n"
			msg += "  debug +|-<tag> [<minutes>] - add or remove logging tag for given time\n" 
			msg += "  q[uit]     - quit\n"

		elif msg.startswith("debug"):
			msg = self._cmd_debug(msg)
			
		elif msg == "rotatelog":
			
			conf.set_value("RUNTIME", "log_rotate", True)
			msg = "OK\n"

		elif msg == "reload":
			config_file = conf.get_str("PATHS", "config_file")
			conf.read(config_file)
			slog.info("configuration", conf.get_all())
			slog.output_tags(conf.get_list("GENERAL", "log_levels"))
			msg = "config reloaded\n"

		elif msg == "r":
			msg = "Registered Users:\n"
			count = 0
			for name in routes.registered_users.keys():
				user = routes.registered_users.get(name)
				# remove any expired users from list
				if user.expires <= time.time():
					del routes.registered_users[user.name]
					continue
				expires = utils.time_posix_to_iso(user.expires)
				msg += str(user.name) + "|"
				msg += str(user.ip_host) + "|"
				msg += str(user.location) + "|"
				msg += str(expires) +"\n"
				count += 1
			msg += "\nTotal: "+str(count)

		elif msg == "c" or msg == "calls":
			msg = "Calls:\n"
			count = 0
			for call in calls.calls:
				if hasattr(call,"answer_time"):
					answered = utils.time_posix_to_iso(call.answer_time)
				else:
					answered = "False"
				if hasattr(call,"outgoing_number"):
					outnumber = call.outgoing_number
				else:
					outnumber = "False"
				msg += str(answered) + "|"
				msg += str(call.billid) + "|"
				msg += str(call.id) + "|"
				msg += str(outnumber) + "|"
				msg += str(call.type) + "|"
				msg += str(call.channels)
				msg += "\n"
				count += 1
			msg += "\nTotal: "+str(count) 

		elif msg == "mod" or msg == "modules":
			call_modules = conf.get_list("CALLS", "modules", sep=",")
			msg = "Modules:\n"
			msg += "%-20s  %s \n" % ("Name", "DID number(s)")
			msg += "%-20s  %s \n" % ("====", "=============")
			for modname in call_modules:
				try:
					number = conf.get_list("CALLS", "mod_" + modname, sep=",")
					msg += "%-20s: %s \n" % (modname, ', '.join(number))
				except:
					msg += "%-20s: Undefined \n" % modname

		elif msg == "radius" or msg == "rad":
			msg = "Radius statistics:\n"
			msg += "queue: %s \n" % radius.req_queue.qsize()
			auth_sent = conf.get_int("STATS", "radauth_sent", 0)
			auth_rec = conf.get_int("STATS", "radauth_rec", 0)
			acct_sent = conf.get_int("STATS", "radacct_sent", 0)
			acct_rec = conf.get_int("STATS", "radacct_rec", 0)
			auth_thr = conf.get_int("STATS", "radauth_thr", 0)
			nrad = conf.get_int("RADIUS", "number_of_workers", 16)
			rthreads = []
			for x in range(nrad):
				thr = {
					'ping' : conf.get_int("STATS", "radius_thread_ping_" + str(x), 0),
					'busy' : conf.get_int("STATS", "radius_thread_busy_" + str(x), 0),
					'idle' : conf.get_int("STATS", "radius_thread_idle_" + str(x), 0)
				}
				rthreads.append(thr)
			if auth_sent == 0:
				auth_loss = 0
			else:
				auth_loss = float(auth_rec) * 100 / float(auth_sent)

			if auth_rec == 0:
				auth_time_avg = 0
			else:
				auth_time_avg = conf.get_float("STATS", "radauth_time", 0.0) * 1000 / auth_rec

			if acct_sent == 0:
				acct_loss = 0
			else:
				acct_loss = float(acct_rec) * 100 / float(acct_sent)

			if acct_rec == 0:
				acct_time_avg = 0
			else:
				acct_time_avg = conf.get_float("STATS", "radacct_time", 0.0) * 1000 / acct_rec
				
			msg += "auth requests sent: %s received: %s lost: %s (%3.2f %%) dropped: %s average %6.3f ms\n" \
			  % (auth_sent, auth_rec, auth_sent - auth_rec, auth_loss, auth_thr, auth_time_avg)
			msg += "acct requests sent: %s received: %s lost: %s (%3.2f %%) average %6.3f ms\n" \
			  % (acct_sent, acct_rec, acct_sent - acct_rec, acct_loss, acct_time_avg)

			msg += "Radius worker threads:\n"
			fmt = "%12s | %10s | %4s"
			hdr1 = fmt % ('Last Active', 'Work Time', 'Load')
			msg += hdr1 + "\n"
			now = time.time()
			for rthr in rthreads:
				if rthr['idle'] + rthr['busy'] == 0:
					percent = 0.00
				else:
					percent = rthr['idle'] / float(rthr['idle'] + rthr['busy'])
				last = now - rthr['ping']
				msg += " %3.3f sec ago | %3.3f sec | %3.2f %%\n" % (last, rthr['busy'], percent)

		elif msg.startswith("stat"):
			msg = self._cmd_stats(msg)	

		elif msg == "clearstats":
			conf.set_value("STATS", "radauth_time", 0.0)
			conf.set_value("STATS", "radacct_time", 0.0)
			conf.set_value("STATS", "radauth_rec", 0)
			conf.set_value("STATS", "radacct_rec", 0)
			conf.set_value("STATS", "radauth_sent", 0)
			conf.set_value("STATS", "radacct_sent", 0)
			conf.set_value("STATS", "radauth_thr", 0)
			nrad = conf.get_int("RADIUS", "number_of_workers", 16)
			for x in range(nrad):
				conf.set_value("STATS", "radius_thread_ping_" + str(x), time.time())
				conf.set_value("STATS", "radius_thread_busy_" + str(x), 0)
				conf.set_value("STATS", "radius_thread_idle_" + str(x), 0)

			msg = "Stats cleared"

		elif msg.startswith("sec"):
			msg = self._cmd_security(msg)

		elif msg == "monitor" or msg == "mon":
			msg = "Monitor:\n"
			msg += "hit <enter> to start monitor, ctrl+] and then q to quit\n"
			self.interactive = False

		elif msg == "q" or msg == "quit":
			msg = "bye!\n"
			self.send_string(msg, sock)
			self.disconnect_client(sock)
			return
		else:
			msg = "Unknown command: "+msg
		
		msg += "\n"
		self.send_string(msg, sock)

	def _cmd_stats(self, msg):
		timestamp = int(time.time())
		fmt = "%16s | %8s | %10s | %8s | %8s"
		hdr1 = fmt % ('Hour', 'Calls', 'Duration', 'ASR', 'ACD')
		hdr2 = fmt % ('YYYY-MM-DD HH   ', 'num', 'mm.ss', '%', 'mm.ss')
		msg = hdr1 + "\n" + hdr2 + "\n"
		hour = timestamp - timestamp % 3600
		for x in range(0,24):
			tst = hour - x * 3600
			hourstr = time.strftime("%Y-%m-%d %H hr", time.localtime(tst))
			calls = conf.get_int("STATS", "calls_hour_" + str(tst), 0)
			goodcalls = conf.get_int("STATS", "goodcalls_hour_" + str(tst), 0)
			dur = conf.get_int("STATS", "dur_hour_" + str(tst), 0)
			if calls == 0:
				continue
			asr = float(goodcalls)/float(calls)*100
			if goodcalls == 0:
				acd = 0
			else:
				acd = float(dur)/float(goodcalls)/60
			msg += '{0:16s} | '.format(hourstr)
			msg += '{0:8.0f} | '.format(calls)
			msg += '{0:10.2f} | '.format(float(dur)/60)
			msg += '{0:8.2f} | '.format(asr)
			msg += '{0:8.2f}'.format(acd)
			msg += "\n"
			
		return msg

	def _cmd_security(self, msg):
		iphosts = stats.get_all_lists("security", "iphosts")
		msg = ''
		if not iphosts:
			msg += "IP hosts table is empty"
		else:
			for iphost in iphosts:
				msg += '%s' % (iphosts[iphost]) + "\n"

		return msg

	def _cmd_debug(self, msg):
		msg_usage = "Usage: debug +|-<tag> [<minutes>]"
		parts = msg.split()
		numparts = len(parts)
		if numparts < 2:
			return msg_usage
		
		tag = parts[1]
		
		if not tag.startswith('+') and not tag.startswith('-'):
			return msg_usage
		
		levels = conf.get_list("GENERAL", "log_levels")
		if numparts >= 3:
			try:
				timeout = int(parts[2])
			except:
				return msg_usage
		else:
			timeout = 30
		
		if tag[0] == '+':
			# append
			levels.append(tag[1:])
		else:
			if not tag[1:] in levels:
				return "Debug tag " + tag[1:] + " not found"
			levels.remove(tag[1:])
		slog.output_tags(levels)
		
		restore_at = time.time() + timeout * 60
		conf.set_value("RUNTIME", "conf_restore", restore_at)
		
		return "debug " + tag + " expires in " + str(timeout) + " minutes"		

		
	
