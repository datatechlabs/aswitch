
import traceback, threading, Queue, time, random
from wobble import slog, conf
import bsdradius.pyrad.packet as packet
from bsdradius.RadClient import RadClient, RadClientError

### TODO remove in production
import string

rad = None			# RADIUS client object
req_queue = Queue.Queue()	# RADIUS request queue
pool = [] # RADIUS connection thread pool

REQ_AUTH = 1
REQ_ACCT = 2

def init():
	"""Initialize RADIUS client."""
	
	global req_queue
	threading.Thread(target=_AAA_thread).start()

def shutdown():
	"""Deinitialize RADIUS client."""

	global req_queue
	slog.info("Shutting down RADIUS thread")
	req_queue.put(None)
	for thread in pool:
		thread.join()


def _AAA_thread():
	"""RADIUS client thread which takes each request one by one, sends it
	and waits for a response.
	"""

	nrad = conf.get_int("RADIUS", "number_of_workers", 16)

	for x in range(nrad):
		rad = RadClient(
				dictFile = conf.get_str('RADIUS', 'dict_file'),
				host = conf.get_str('RADIUS', 'auth_host'),
				authport = conf.get_int('RADIUS', 'auth_port'),
				acctport = conf.get_int('RADIUS', 'acct_port'),
				secret = conf.get_str('RADIUS', 'auth_secret'),
				retries = conf.get_int('RADIUS', 'auth_request_retries'),
				timeout = conf.get_int('RADIUS', 'auth_request_timeout')
			)
		client_addr = conf.get_str("SERVER", "ip_address"), 0
		try:
			rad.bind(client_addr)
		except:
			rad.bind(("0.0.0.0",0))

		rClient = SendRequestThread(x, rad)
		rClient.start()

	pool.append(rClient)
	slog.info("Initialized RADIUS connection pool with ", nrad, " workers")

#	while True:
		# nothing much to do actually for this thread anymore
#		time.sleep(10)

		

class SendRequestThread(threading.Thread):

	def __init__(self, x, rad):
		super(SendRequestThread, self).__init__()
		self.x = x
		self.rad = rad
		self.stoprequest = threading.Event()
		# stats collection
		self.idletime = 0
		self.busytime = 0

	def run(self):
		idlenow = time.time()

		while not self.stoprequest.isSet():

			# call watchdog from time to time
			self.watchdog()

			# fetch next queued request
			try:
				req_data = req_queue.get(True, 0.05)
			except Queue.Empty:
				continue

			if req_data == None:
				return

			busynow = time.time()
			self.idletime = self.idletime + busynow - idlenow
			req_type, packet_dict, callback, args = req_data

			if req_type == REQ_AUTH:
				rad_result = _send_auth(self.rad, packet_dict)
			elif req_type == REQ_ACCT:
				rad_result = _send_acct(self.rad, packet_dict)
			else:
				slog.err("Wrong packet type")

			# execute callback
			if callback != None:
				if args != None:
					callback(rad_result, *args)
				else:
					callback(rad_result)
			idlenow = time.time()
			self.busytime = self.busytime + idlenow - busynow

	def join(self, timeout=None):
		self.stoprequest.set()
		super(SendRequestThread, self).join(timeout)

	def watchdog(self):
		conf.set_value("STATS", "radius_thread_ping_" + str(self.x), time.time())
		conf.set_value("STATS", "radius_thread_busy_" + str(self.x), self.busytime)
		conf.set_value("STATS", "radius_thread_idle_" + str(self.x), self.idletime)


def throttle():
	rqsize = req_queue.qsize()
	slim = conf.get_int("RADIUS", "throttle_soft_limit", 10)
	hlim = conf.get_int("RADIUS", "throttle_hard_limit", 20)
	if rqsize > hlim:
		thr = 0
		conf.set_value("STATS", "rad_throttle", thr)
		slog.warn("Radius queue size is over hardlimit. Throttle value is %s, discarding request" % thr)
		return False
	if rqsize < slim:
		thr = 100
		conf.set_value("STATS", "rad_throttle", thr)
		return True
	thr = float( hlim - rqsize ) / float(hlim - slim ) * 100
	conf.set_value("STATS", "rad_throttle", thr)
	l = random.randrange(100)
	if l < thr:
		return True
	slog.warn("Radius throttle value is %s, discarding request" % thr)
	return False


def queue_auth(packet_dict, callback=None, args=None):
	"""Queue authorization request. @callback will be called when a
	response is received or an error occurs. @callback receives the
	response as its single argument, or None in case of failure. @callback
	may be None.
	"""
	
	# throttle control
	if conf.get_bool("RADIUS", "throttle_enable", False) == True:
		if throttle() == False:
			rec = conf.get_int("STATS", "radauth_thr", 0)
			conf.set_value("STATS", "radauth_thr", rec+1)
			return False
	
	# callback arguments should be converted to a tuple
	if (not isinstance(args, list)) and (not isinstance(args, tuple)):
		args = (args,)

	req_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
	slog.info("Queueing radius auth request ID ", req_id)

	req_queue.put((REQ_AUTH, packet_dict, callback, args))
	return True

def queue_acct(packet_dict, callback=None, args=None):
	"""Queue accounting request. @callback will be called when a response
	is received or an error occurs. @callback receives the response as its
	single argument, or None in case of failure. @callback may be None.
	"""
	
	if conf.get_bool("RADIUS", "acct_enable", True) == False:
		return
	
	# callback arguments should be converted to a tuple
	if (not isinstance(args, list)) and (not isinstance(args, tuple)):
		args = (args,)

	req_queue.put((REQ_ACCT, packet_dict, callback, args))

def _send_auth(rad, packet_dict):
	"""Send authorization packet to RADIUS server."""

	sent = conf.get_int("STATS", "radauth_sent", 0)
	conf.set_value("STATS", "radauth_sent", sent+1)
	start = time.time()
	
	slog.info("Sending authorization request to RADIUS server")
	
	req = rad.getAuthPacket(attributes=packet_dict)

	slog.verb("Request details:", str(req))
	try:
		rad_result = rad.sendPacket(req)
	except:
		slog.warn(traceback.format_exc())
		return None

	tdiff = time.time() - start
	reqtime = conf.get_float("STATS", "radauth_time", 0.0)
	conf.set_value("STATS", "radauth_time", reqtime + tdiff)
	
	rec = conf.get_int("STATS", "radauth_rec", 0)
	conf.set_value("STATS", "radauth_rec", rec+1)
		
	slog.verb("Response data:", str(rad_result))
	return rad_result

def _send_acct(rad, packet_dict):
	"""Send accounting packet to RADIUS server."""

	sent = conf.get_int("STATS", "radacct_sent", 0)
	conf.set_value("STATS", "radacct_sent", sent+1)
	start = time.time()
	
	slog.info("Sending accounting request to RADIUS server")

	req = rad.getAcctPacket(attributes=packet_dict)

	slog.verb("Request details:", str(req))
	try:
		# NOTE:
		# keeps sending requests until a response is received
		rad_result = rad.sendPacket(req)
	except:
		slog.warn(traceback.format_exc())
		return None

	slog.verb("Response data:", str(rad_result))

	tdiff = time.time() - start
	reqtime = conf.get_float("STATS", "radacct_time", 0.0)
	conf.set_value("STATS", "radacct_time", reqtime + tdiff)
	
	rec = conf.get_int("STATS", "radacct_rec", 0)
	conf.set_value("STATS", "radacct_rec", rec+1)
	
	if rad_result.code == packet.AccountingResponse:
		slog.info("Accounting packet accepted.")
		return True

	slog.info("Server did not accept accounting packet.")
	return False

def gen_h323_conf_id():
	"""Generate a h323 conference ID."""

	hexSymbols = '0123456789abcdef'

	sesid = ''
	for i in range(4):
		for j in range(8):
			sesid += random.choice(hexSymbols)
		sesid += ' '
	sesid = sesid.rstrip(' ')
	return sesid


def accounting_on():

	# Yate address
	nas_ip_addr = conf.get_str("RADIUS", "nas_ip_address")

	rad_params = {
		"NAS-IP-Address": nas_ip_addr,
		"Acct-Status-Type": "Accounting-On"
	}
	# dispatch RADIUS Accounting-Request packet
	slog.info("RADIUS accounting on request:", rad_params)
	queue_acct(rad_params)

def extract_balance(rad_result):
	# extract balance from radius result
	balance_attr = rad_result.get("h323-credit-amount", [None])[0]
	if balance_attr == None:
		balance = 0
	else:
		balance_split = balance_attr.split('=')
		if len(balance_split) != 2:
			balance = 0
		else:
			balance = balance_split[1]
	return balance


def extract_currency(rad_result):
	# extract currency from radius result
	currency_attr = rad_result.get("h323-currency", [None])[0]
	if currency_attr == None:
		slog.warn("Could not extract h323-currency from auth "
		     "response.")
		return None
		
	currency_split = currency_attr.split('=')
	if len(currency_split) != 2:
		slog.warn("Invalid currency string", currency_attr)
		return None

	currency = currency_split[1].upper()
	slog.info("Currency:", currency)
	return currency