"""
Sound name assembly
"""

from wobble import conf

number_dir = "digits"
silence_dir = "silence"

time_play_format = conf.get_str("IVR", "time_play_format", "hhmmss")

def fullpath(soundname):
	return soundname

def silence(length=1):
	"""play silence for length (1-10) seconds"""
	return fullpath(silence_dir + '/' + str(length))

def money(amount, currency="USD", lang="en"):
	"""Assemble sounds that pronounce a monetary value."""
	
	# available currency sounds
	if lang == "de":
		csounds = {
			"USD": ["dollar", "dollars", "cents"],
			"EUR": ["euro", "euro", "cents"],
			"GBP": ["pound", "pounds", "pence"],
			"GYD": ["dollar", "dollars", "cents"],
			"CAD": ["dollar", "dollars", "cents"],
			"AUD": ["dollar", "dollars", "cents"]
		}
	else:
		csounds = {
			"USD": ["dollar", "dollars", "cents"],
			"EUR": ["euro", "euros", "cents"],
			"GBP": ["pound", "pounds", "pence"],
			"GYD": ["dollar", "dollars", "cents"],
			"CAD": ["dollar", "dollars", "cents"],
			"AUD": ["dollar", "dollars", "cents"]
		}

	snd_queue = []

	# check if we have currency sounds
	if currency not in csounds:
		snd_queue += floatstr(amount)
		return snd_queue

	dollars = int(float(amount))

	# check if we're dealing with a floating point number
	cents = 0
	if "." in amount:
		cents = int(amount[amount.find(".") + 1:])
		if (dollars == 0) and ("-" in amount):
			cents = -cents

	# check if we only have to report "dollars"
	if (dollars == 0 and cents == 0) or (cents == 0):
		snd_queue += currency_amount(dollars, csounds[currency], lang)
		return snd_queue

	# say "dollars"
	if dollars != 0:
		snd_queue += currency_amount(dollars, csounds[currency], lang)
		snd_queue += [fullpath("and")]
		
	# and "cents"
	snd_queue += integer(cents, lang)
	snd_queue += [fullpath(csounds[currency][2])]
	return snd_queue

def time(seconds):
	snd_queue = []

	# divide in hours, minutes, seconds according to config
	if time_play_format == "mmss":
		hours = 0
		minutes = seconds / 60
		seconds = seconds % 60
	elif time_play_format == "mm":
		hours = 0
		minutes = seconds / 60
		if minutes > 0:
			seconds = 0
		else:
			seconds = seconds % 60
	else:
		hours = seconds / (60*60)
		extra_seconds = seconds % (60*60)
		minutes = extra_seconds / 60
		seconds = extra_seconds % 60

	if hours > 0:
		snd_queue += integer(hours)
		if hours == 1:
			snd_queue += ["hour"]
		else:
			snd_queue += ["hours"]

	if minutes > 0:
		snd_queue += integer(minutes)
		if minutes == 1:
			snd_queue += ["minute"]
		else:
			snd_queue += ["minutes"]

	if seconds > 0:
		snd_queue += integer(seconds)
		if seconds == 1:
			snd_queue += ["second"]
		else:
			snd_queue += ["seconds"]

	return snd_queue

def currency_amount(amount, csoundlist, lang="en"):
	snd_queue = []
	
	snd_queue += integer(amount, lang)
	if amount == 1 or amount == -1:
		snd_queue += [fullpath(csoundlist[0])]
		return snd_queue
	
	snd_queue += [fullpath(csoundlist[1])]
	return snd_queue

def sequence(number, lang="en"):
	""" Assemble sequence of number sounds """
	snd_queue = list(number)
	for i in xrange(len(snd_queue)):
		snd_queue[i] = fullpath(number_dir + '/' + snd_queue[i])
		
	return snd_queue

def integer(number, lang="en"):
	"""Assemble sounds for an integer."""
	snd_queue = number_sounds(number, lang)

	# create complete path names
	for i in xrange(len(snd_queue)):
		snd_queue[i] = fullpath(number_dir + '/' + snd_queue[i])
		
	return snd_queue

def floatstr(amount):
	"""Assemble sounds for a floating point number.

	NOTE: this function expects a padded number with two decimal places,
		e.g., 45.40 (NOT 45.4). This behavior may later be changed to
		allow for any kind of representation.
	""" 
	snd_queue = []
	
	dollars = int(float(amount))
        cents = 0
        if "." in amount:
                cents = int(amount[amount.find(".") + 1:])

	# negative, even though zero?
	if "-" in amount and dollars == 0:
		snd_queue += fullpath(number_dir + "/minus")
	snd_queue += integer(dollars)
	if cents == 0:
		return snd_queue

	# say "point"
	snd_queue += [fullpath("point")]
	
	if cents < 10 and cents != 0:
		snd_queue += [fullpath(number_dir + "/0")];
	snd_queue += integer(cents)

	return snd_queue

def number_sounds(num, lang="en"):
	numstr = str(num)

	if numstr[0] == '-':
		sign = ["minus"]
		numstr = numstr[1:]
	else:
		sign = []
	
	if len(numstr) > 6:
		return sign + _say_7(numstr)
	if len(numstr) > 3:
		return sign + _say_4(numstr)
	if len(numstr) > 2:
		return sign + _say_3(numstr, lang)
	if len(numstr) > 1:
		return sign + _say_2(numstr, lang)
	return sign + _say_1(numstr)

def _say_1(numstr):
	# length = 1
	return [numstr]

def _say_2(numstr, lang="en"):
	# length = 2
	if numstr[0] == '0':
		return _say_1(numstr[1:])
	if numstr[0] == '1':
		return [numstr]
	if numstr[1] == '0':
		return [numstr[0] + '0']
	if lang == "de":
		return [numstr[1] + '-and', numstr[0] + '0']
	return [numstr[0] + '0', numstr[1]]

def _say_3(numstr, lang="en"):
	# length = 3
	if numstr[0] == '0':
		return _say_2(numstr[1:], lang)
	if numstr[1:] == '00':
		return [numstr[0], "hundred"]
	return [numstr[0], "hundred"] + _say_2(numstr[1:], lang)

def _say_4(numstr):
	# 3 < length < 7
	numth = int(numstr[:-3])
	numth = number_sounds(numth)
	remain = int(numstr[-3:])
	if remain == 0:
		return numth + ["thousand"]
	return numth + ["thousand"] + number_sounds(remain)
	
def _say_7(numstr):
	# 6 < length < ?, 1000000
	nummil = int(numstr[:-6])
	nummil = number_sounds(nummil)
	remain = int(numstr[-6:])
	if remain == 0:
		return nummil + ["million"]
	return nummil + ["million"] + number_sounds(remain)
