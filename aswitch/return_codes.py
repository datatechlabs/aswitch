"""Sip response codes extracted from
	yate/contrib/ysip/engine.cpp
"""

# sip responses mapped with sip return code
sipResponseToSipCode = {
	"trying" : 100,
	"ringing" : 180,
	"call is being forwarded" : 181,
	"queued" : 182,
	"session progress" : 183,
	"ok" : 200,
	"accepted" : 202,
	"multiple choices" : 300,
	"moved permanently" : 301,
	"moved temporarily" : 302,
	"use proxy" : 305,
	"alternative service" : 380,
	"bad request" : 400,
	"unauthorized" : 401,
	"payment required" : 402,
	"forbidden" : 403,
	"not found" : 404,
	"method not allowed" : 405,
	"not acceptable"  : 406,
	"proxy authentication required" : 407,
	"request timeout" : 408,
	"gone" : 410,
	"request entity too large" : 413,
	"request-uri too long" : 414,
	"unsupported media type" : 415,
	"unsupported uri scheme" : 416,
	"bad extension" : 420,
	"extension required" : 421,
	"session timer too small" : 422,
	"interval too brief" : 423,
	"temporarily unavailable" : 480,
	"call/transaction does not exist"  : 481,
	"loop detected" : 482,
	"too many hops" : 483,
	"address incomplete" : 484,
	"ambiguous" : 485,
	"busy here" : 486,
	"request terminated" : 487,
	"not acceptable here" : 488,
	"bad event" : 489,
	"request pending" : 491,
	"undecipherable" : 493,
	"security agreement required" : 494,
	"server internal error" : 500,
	"not implemented" : 501,
	"bad gateway" : 502,
	"service unavailable" : 503,
	"server time-out" : 504,
	"version not supported" : 505,
	"message too large" : 513,
	"response cannot be sent safely" : 514,
	"response requires congestion management" : 515,
	"proxying of request would induce fragmentation" : 516,
	"precondition failure" : 580,
	"busy everywhere" : 600,
	"decline" : 603,
	"does not exist anywhere" : 604,
	"not acceptable" : 606,
	"unspecified error" : 0
}

# H323 return codes
H323_OK = 0
H323_INVALID_ACCOUNT = 1
H323_INVALID_PASSWORD = 2
H323_ACCT_IN_USE = 3
H323_ZERO_BALANCE = 4
H323_EXPIRED = 5
H323_CREDIT_LIMIT_EXCEEDED = 6
H323_USER_DENY = 7
H323_SERVICE_NOT_AVAILABLE = 8
H323_DEST_NUMBER_BLOCKED = 9
H323_RETRIES_EXCEEDED = 10
H323_INVALID_RADIUS_ARGUMENT = 11
H323_INSUFFICIENT_BALANCE = 12
H323_TOLL_FREE_CALL = 13
H323_INVALID_CARD_NUMBER = 14
H323_INVALID_DEST_NUMBER = 21
H323_UNKNOWN = 51

h323CodeToSipResponse = {
	H323_OK : "OK",
	H323_INVALID_ACCOUNT : "Forbidden",
	H323_INVALID_PASSWORD : "Unauthorized",
	H323_ACCT_IN_USE : "Forbidden",
	H323_ZERO_BALANCE : "Payment Required",
	H323_EXPIRED : "Forbidden",
	H323_CREDIT_LIMIT_EXCEEDED : "Payment Required",
	H323_USER_DENY : "Forbidden",
	H323_SERVICE_NOT_AVAILABLE : "Service Unavailable",
	H323_DEST_NUMBER_BLOCKED : "Temporarily Unavailable",
	H323_RETRIES_EXCEEDED : "Server Time-out",
	H323_INVALID_RADIUS_ARGUMENT : "Not Acceptable",
	H323_INSUFFICIENT_BALANCE : "Payment Required",
	H323_TOLL_FREE_CALL : "OK",
	H323_INVALID_CARD_NUMBER : "Not Found",
	H323_INVALID_DEST_NUMBER : "Not Found",
	H323_UNKNOWN : "Forbidden",
}

h323CodeToSipCode = {
	H323_OK : 200,
	H323_INVALID_ACCOUNT : 403,
	H323_INVALID_PASSWORD : 401,
	H323_ACCT_IN_USE : 403,
	H323_ZERO_BALANCE : 402,
	H323_EXPIRED : 403,
	H323_CREDIT_LIMIT_EXCEEDED : 402,
	H323_USER_DENY : 403,
	H323_SERVICE_NOT_AVAILABLE : 503,
	H323_DEST_NUMBER_BLOCKED : 480,
	H323_RETRIES_EXCEEDED : 504,
	H323_INVALID_RADIUS_ARGUMENT : 606,
	H323_INSUFFICIENT_BALANCE : 402,
	H323_TOLL_FREE_CALL : 200,
	H323_INVALID_CARD_NUMBER : 404,
	H323_INVALID_DEST_NUMBER : 404,
	H323_UNKNOWN : 403,
}
