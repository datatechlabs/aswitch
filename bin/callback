#!/usr/bin/env python
"""
This script is useful if one wishes for Arrow Switch to initiate a callback. A
connection is opened to the Yate rmanager port, usually 127.0.0.1:5038, and a
single command is sent:
	'call web_callback caller_number:callback_number:called_number'

This makes rmanager dispatch a call.execute message:
	call.execute
		callto: web_callback
		target: caller_number:callback_number:called_number

The message is interpreted by Arrow Switch, which then performs the callback.
"""

import sys, getopt, socket, time

def usage():
	"""Print usage info to stdout."""
	
	print "Arrow Switch callback script"
	print
	print "usage:", sys.argv[0], "[options] caller_number " \
	    "callback_number called_number"
	print
	print "Options:"
	print "  -h\t\tthis help message"
	print "  -y address\trmanager port address (e.g., '127.0.0.1:5038')"
	print

try:
	(opts, args) = getopt.getopt(sys.argv[1:], "hy:")
except getopt.GetoptError:
	usage()
	sys.exit(1)

#
# parse command line options
#

if len(args) != 3:
	usage()
	sys.exit(1)

caller = args[0]
callback = args[1]
called = args[2]

ip_address = None
port = None
for (opt, val) in opts:
	if opt == "-h":
		usage()
		sys.exit(1)
	if opt == "-y":
		pair = val.split(':')
		if len(pair) != 2:
			usage()
			sys.exit(1)
		ip_address, port = pair
		try:
			port = int(port)
		except ValueError:
			print "ERROR: Port number must be an integer."
			sys.exit(1)
			
#
# set defaults if necessary
#

if ip_address == None:
	ip_address = "127.0.0.1"
	port = 5038

#
# Feed data to rmanager
#

sock = socket.socket()
sock.connect((ip_address, port))

# read initial "Yate ready" line
ready_line = sock.recv(1024)

#
# pass callback data to Arrow Switch through rmanager
#

line = "call web_callback %s:%s:%s\r\n" % (caller, callback, called)
data_len = len(line)
while True:
	num_sent = sock.send(line)
	if num_sent == data_len:
		break

	if num_sent < data_len:
		data_len -= num_sent
		line = line[num_sent:]

time.sleep(1)
sock.close()
sys.exit(0)

