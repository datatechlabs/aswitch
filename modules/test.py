
from wobble import slog
from aswitch import yate, calls, sounds, callctl

#
# Message handlers
#

def engine_timer(call, msg):
	pass

def call_route(call, route_msg):
	# route the call to a 'dumb' channel
	route_msg.retvalue = "dumb/"
	route_msg.send()

	call.type = calls.CALLTYPE_IVR	# set call type
	call.route_msg = route_msg	# save routing message

	# extract incoming channel
	call.in_chan = route_msg.params["id"]

def call_execute(call, exec_msg):
	# extract channels
	call.dumb_chan = exec_msg.params["targetid"]

	# pick up the phone
	callctl.answer(call)

	chan = call.dumb_chan
	callctl.play(call, chan, "silence/2")
	callctl.play_map(call, chan, "card-balance-is", "en")
	callctl.play_map(call, chan, sounds.money("1000"), "en")
#	callctl.play_map(call, chan, "dollars", "en")
	callctl.play_map(call, chan, "goodbye", "en")

def call_conference(call, conf_msg):
	pass

def call_answered(call, answer_msg):
	pass

def call_cdr(call, cdr_msg):
	chan = cdr_msg.params.get("chan", "")
	op = cdr_msg.params.get("operation", "")

	if op == "finalize":
		calls.rm_channel(call, chan)

def chan_notify(call, note_msg):
	call.playing = False
	
#	if len(call.sound_queue) == 0:
#		# drop the call
#		callctl.dropchan(call.in_chan)
#		return

	# play queued sounds
	callctl.proc_sound_queue(call)

def chan_dtmf(call, dtmf_msg):
	
	chan = call.dumb_chan

	text = dtmf_msg.params.get("text")
	callctl.play_map(call, chan, "you-entered", "en")
	callctl.play_map(call, chan, sounds.integer(text), "en")

