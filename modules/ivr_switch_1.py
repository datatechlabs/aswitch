# 
# this is IVR module compatible with YATE2.
#
# This is now official IVR module and should be used in any cases

from decimal import Decimal
import time, traceback, re
from wobble import slog, conf
from aswitch import yate, calls, callctl, sounds, radius, routes, return_codes, sound_map
import bsdradius.pyrad.packet as packet

callto_breakup = re.compile("([0-9]+)@([^\s]+)")

CALLCMD_NONE = 0
CALLCMD_BALPLAY = 1
CALLCMD_REQDROP = 2

acct_update_enable = conf.get_bool("RADIUS", "acct_update_enable", False)
acct_update_interval = conf.get_int("RADIUS", "acct_update_interval", 60)

low_balance_warning = conf.get_bool("IVR", "low_balance_warning", False)
low_balance_level = conf.get_float("IVR", "low_balance_level", 1.0)

sound_dir = conf.get_str("PATHS", "sound_dir")
extension = conf.get_str("PATHS", "extension")
prompt_map = conf.get_str("PATHS", "prompt_map", "cisco")

play_spec_tone = conf.get_bool("IVR", "play_tone_on_connect", False)
spec_tone = conf.get_str("IVR", "tone_on_connect", "dtmf/c")

#
# Message handlers
#

def engine_timer(call, msg):
	# check if DTMF timeout has been reached
	time_diff = time.time() - call.last_dtmf_time
	#slog.info("call.last_dtmf_time",call.last_dtmf_time,"time_diff",time_diff)
	# check if call drop time is reached (incl safety margin)
	if hasattr(call,"timeout") and hasattr(call,"answer_time"): 
		if call.answer_time -2 + call.timeout/1000  < time.time():
			if call.in_chan in call.channels:
				slog.info("disconnecting the call")
				callctl.dropchan(call.in_chan)
				calls.rm_channel(call, call.in_chan)

		# play low balance warning at low_balance_time
		if low_balance_warning != False and call.answer_time + call.low_balance_time < time.time():
			if call.low_balance_msg_done == False:
				#	play message into in chan
			#	callctl.play_map(call, call.in_chan, "low-balance")
				play_message(call.in_chan, "low-balance", notify=False)
				call.low_balance_msg_done = True
				return 

	if (call.last_dtmf_time != 0) and (time_diff > call.dtmf_timeout) and \
		(call.type == calls.CALLTYPE_IVR) and (call.callout == False):
		slog.info("DTMF timeout occured")
		handle_dtmf_timeout(call)	

	if (call.last_dtmf_time != 0) and (time_diff > call.dtmf_timeout) and \
		(call.type == calls.CALLTYPE_STD):
		handle_dtmf_timeout_during_call(call)
	# check for accounting update timers 
	if acct_update_enable and hasattr(call,"answer_time"):
		call.update_timer = call.update_timer + 1
		if 	call.update_timer >= acct_update_interval:
			acct_update(call)
			call.update_timer = 0

def call_route(call, route_msg):

	# catch any lateroute messages here - use special called: lateroute_XXX param
	called = route_msg.params.get("called", "")
	if called.startswith("lateroute"):
		callstr = called.split(";")[0]
		lateroute_id = int(callstr.split("_")[1])
		slog.info("lateroute ID", lateroute_id)
		# replace attributes from call object
		if hasattr(call,"yate_routes"):
			rstr = call.yate_routes[lateroute_id-1].rsplit(';')
			slog.info("routestring",rstr)
			for rdata in rstr:
				(key,val) = rdata.rsplit('=')
				route_msg.params[key] = val
				if key == "callto":
					route_msg.retvalue = val
			route_msg.reverse("true")
			route_msg.send()
			return
		else:
			# sometimes we get here- not sure why
			slog.warn("dropping rogue lateroute message without yate_routes")
			return


	chan = route_msg.params["id"]
	callto = route_msg.params.get("callto", "")
	if chan.startswith("fork/") and callto != "":
		route_msg.send()
		return

	call.type = calls.CALLTYPE_IVR	# set call type
	call.billable = False		# the IVR part is not billable
	call.route_msg = route_msg	# save routing message

	# extract incoming channel & set outgoing to empty string
	call.in_chan = route_msg.params["id"]
	call.out_chan = ""
	call.target_chan = ""   # target chan is chan which is expected to connect 
	call.tried_routes = [] # pairs of (number, address)

	call.dropme = False		# whether to drop the call
	call.callout = False		# whether to place an outgoing call
	call.authenticated = False	# whether we have authenticated user
	if "device" in route_msg.params:
		device = route_msg.params["device"]
	else:
		device = ""
	if device.startswith("Nokia"):
		call.formats = "g729,g723"
		route_msg.params["formats"] = call.formats
	elif device.startswith("E71"):
		call.formats = "g729,g723"
		route_msg.params["formats"] = call.formats
	elif "formats" in route_msg.params:
		call.formats = route_msg.params["formats"]	# media formats (codecs)
	else:
		call.formats = ""
		slog.warn("call.route message without formats.")

	# set outgoing number and initial password (which may not be required)
	call.outgoing_number = route_msg.params.get("called", "")
	call.password = ""

	# set DTMF command string
	call.dtmf_cmd = ""
	call.command = CALLCMD_NONE		# special call handling commands

	# set DNIS number - IVR access number
	call.dnis = ""

	# get call-through (DID) number list
	call.call_through_numbers = conf.get_list("CALLS",
	    "call_through_numbers")
	# set call through flag to indicate call to access number (calling card app)
	call.call_through = False 
	
	# don't wait for messages forever
	call.timeout_tick = conf.get_int("CALLS", "timeout_tick")
	call.dtmf_timeout = conf.get_int("CALLS", "dtmf_timeout")
	call.last_dtmf_time = 0	# when was the last DTMF received
	call.update_timer = 0 # accounting update timer

	# maximum number of digits in outgoing number
	call.dest_number_max_digits = conf.get_int("CALLS",
	    "dest_number_max_digits")

	# maximum retries (password and destination number)
	call.max_pw_attempts = conf.get_int("CALLS", "max_attempts")
	call.max_dn_attempts = conf.get_int("CALLS", "max_attempts")
	call.num_pw_attempts = 0
	call.num_dn_attempts = 0
	
	call.low_balance_msg_done = False
	
	# initial IVR language
	call.ivr_lang = conf.get_str("IVR", "default_ivr_lang", "en")
	lang_map = conf.get_str("IVR", "lang_map", "1111=en,2222=de,3333=es")
	langrules = lang_map.split(",")
	for rule in langrules:
		(num, l) = rule.split("=")
		if num == called:
			slog.info("dialed number",called,"language",l)
			call.ivr_lang = l
			break

	call.status = None # FSM status for menu operations
	call.access_level = None
	slog.info("call.route call.type",call.type)
	
	if call.outgoing_number in call.call_through_numbers:
		# the call is towards access number, check ANI
		# route to dumb channel to gt it answered later
		call.call_through = True
		call.dumb_chan = call.route_msg.params.get("target","")
		route_msg.retvalue = "dumb/"
		# enforce codecs
		codec_map = conf.get_str("IVR", "codec_map", "")
                crules = codec_map.split(",")
                for crule in crules:
                        try:
                                (num, c) = crule.split("=")
                        except:
                                c = ""
                        if num == called:
                                cr = c.replace(";", ",")
                                route_msg.params["formats"] = cr
		route_msg.send()
		return
	
	call.dumb_chan = route_msg.params.get("target","")
	routes.authorize_call(call, route_msg, authorization_cb)

def call_execute(call, exec_msg):
	# save outgoing channel ID
	chan = exec_msg.params.get("id")
	target_chan = exec_msg.params.get("targetid", "")
	callto = exec_msg.params.get("callto")
	if target_chan == "" and callto.startswith("dumb/"):
		slog.warn("call.execute message without target channel ID.")
		exec_msg.reverse()
		exec_msg.send()
		return

	call.target_chan = target_chan
	# gather tried routes if we're in callout mode
	
	slog.info("call.execute call.type",call.type)
	
	if call.type == calls.CALLTYPE_STD:
		callto = exec_msg.params.get("callto")
		if callto == None:
			exec_msg.reverse()
			exec_msg.send()
			return

		# since call.execute may arrive before call.cdr, use targetid to set out_chan
		call.out_chan = target_chan

		if (chan == call.in_chan) and target_chan.startswith("fork/"):
			# not interested in incoming_chan->fork_chan
			exec_msg.reverse()
			exec_msg.send()
			return

		# this could be return after outgoing leg disconnect
		if (chan == call.in_chan) and target_chan.startswith("dumb/"):
			call.dumb_chan = target_chan

			# for a non-calling card application just end the call
			if call.call_through == False:
				call.dropme = True
				callctl.dropchan(call.in_chan)
				exec_msg.reverse()
				exec_msg.send()
				return

			# pick up the phone
			callctl.answer(call)
			# prompt for destination number
			if call.command == CALLCMD_REQDROP:
				# we dropped it, prompt for destination
				callctl.play_map(call, call.dumb_chan, "enter-destination", call.ivr_lang)
			else:
				# it probably gone
				callctl.play_map(call, call.dumb_chan, ["destin-busy", "enter-destination"], call.ivr_lang)
			# reset call
			call.outgoing_number = ""
			call.dtmf_cmd = ""
			call.last_dtmf_time = 0
			call.callout = False
			call.dropme = False
			call.tried_routes = []
			call.type = calls.CALLTYPE_IVR
			exec_msg.reverse()
			exec_msg.send()
			return

		matches = callto_breakup.findall(callto)
		if len(matches) != 1:
			slog.warn("Unrecognized 'callto' parameter in "
			    "call.execute.")
			exec_msg.reverse()
			exec_msg.send()
			return

		call.tried_routes.append(matches[0])
		exec_msg.reverse()
		exec_msg.send()
		return

	#call.dumb_chan = target_chan

	# we should decide here if we need to pickup the phone and play any message
	# or try to route the call

	# answer call in one of the cases:
	#	1) we have called number and we have any of IVR play flags set
	#	2) we dont have called number and we need to prompt for it
	
	answernow = False
	playbalance = False
	playtime = False
	ivr_set_mode = conf.get_str("IVR", "ivr_settings_mode")
	balance_announce = conf.get_bool("IVR", "balance_announce")
	time_announce = conf.get_bool("IVR", "time_announce")
	
	havedestnum = True
	if call.outgoing_number in call.call_through_numbers:
		havedestnum = False
		slog.info("call to accessnumber")
		auth_user(call,"number")
	
	# IVR options could be set by radius
	if (hasattr(call,"ivr_options") and (ivr_set_mode == "rad_dtl") and havedestnum):
		if "time" in call.ivr_options:
			answernow = True
			playtime = True
		if "balance" in call.ivr_options:
			answernow = True
			playbalance = True
	
	if ivr_set_mode == "conf":
		if balance_announce:
			answernow = True
			playbalance = True
		if time_announce:
			answernow = True
			playtime = True
	
	if havedestnum == False:
		answernow = True

	if answernow:
		# pick up the phone
		call.dumb_chan = target_chan
		callctl.answer(call)
		callctl.play(call, call.dumb_chan, "silence/1")
		if playbalance and hasattr(call,"balance"):
			slog.info("play balance is yes")
			callctl.play_map(call, call.dumb_chan, "card-balance-is", call.ivr_lang)
			balance_sounds = sounds.money(call.balance, call.currency)
			callctl.play_map(call, call.dumb_chan, balance_sounds, call.ivr_lang)
		if playtime and hasattr(call,"ivr_time"):
			slog.info("play time is yes")
			callctl.play_map(call, call.dumb_chan, "you-have", call.ivr_lang)
			callctl.play_map(call, call.dumb_chan, sounds.time(call.ivr_time), call.ivr_lang)
		exec_msg.reverse()
		exec_msg.send()
		return
	exec_msg.reverse()
	exec_msg.send()

def call_answered(call, answer_msg):
	chan = answer_msg.params.get("id")
	target_chan = answer_msg.params.get("targetid")

	# set outgoing channel when it answers
	if (target_chan == call.in_chan) and (chan != call.dumb_chan):
		call.out_chan = chan
		slog.info("Setting outgoing channel to:", chan)
		call.answer_time = time.time()
		slog.info("Setting answer time to", time.time())
		# play special tone when call is answered (somebody asked for this)
		if play_spec_tone:
			play_tone(call.in_chan, spec_tone, notify=False)
			play_tone(call.out_chan, spec_tone, notify=False)

def call_cdr(call, cdr_msg):
	chan = cdr_msg.params.get("chan", "")
	op = cdr_msg.params.get("operation", "")
	status = cdr_msg.params.get("status", "")
	direction = cdr_msg.params.get("direction", "")

	slog.info ("call_cdr message for direction ",direction)

	# the first leg which got active or disconnected will use confid stored in call structure
	# that is to match conf ids in auth and acct messages
	if ((op == "finalize") and (direction == "outgoing")) or \
	    ((op == "update") and (chan == call.out_chan) and \
	    (status == "answered")):
		if hasattr(call,"confid"):
			confid = call.confid
			calls.conf_ids[chan] = confid
			del call.confid
			slog.info("replacing confid for chan",chan,"with",confid)
		else:
			slog.info("call has no confid attribute, using confid from chan",chan)
			confid = calls.conf_ids[chan]

	# remove outgoing leg, keep the call on
	if (op == "finalize") and (direction == "outgoing"):
		slog.info("remove outgoing leg")
		calls.rm_channel(call, chan, True)

	# remove incoming leg
	if (op == "finalize") and (direction == "incoming"):
		slog.info("remove incoming leg")
		calls.rm_channel(call, chan, True)	

	# accounting stops for all routes
	if (op == "finalize") and (direction == "outgoing") and \
	    (not chan.startswith("dumb/")):
		route_msg = call.route_msg
		
		cdr_data = {}
		cdr_data["duration"] = float(cdr_msg.params["duration"])
		cdr_data["billtime"] = float(cdr_msg.params["billtime"])
		cdr_data["time"] = float(cdr_msg.params["time"])
		cdr_data["billid"] = cdr_msg.params["billid"]
		cdr_data["caller"] = route_msg.params["caller"]
		cdr_data["called"] = call.outgoing_number
		cdr_data["calledfull"] = cdr_msg.params.get("calledfull")
		cdr_data["caller_addr"] = route_msg.params["address"]
		cdr_data["called_addr"] = cdr_msg.params["address"]

		if call.password != "":
			cdr_data["username"] = call.password
		else:
			cdr_data["username"] = route_msg.params["caller"]
		if cdr_data["billtime"] != 0.0:
			cdr_data["dcause"] = 200
		else:
			reason = cdr_msg.params.get("reason", "Unspecified error")
			cdr_data["dcause"] = return_codes.sipResponseToSipCode.get(reason, 0)
		# set DNIS number
		cdr_data["dnis_access"] = call.dnis
		cdr_data["confid"] = confid

		callctl.accounting_stop(call, cdr_data, route_msg)

	# accounting alive
	if (op == "update") and (chan == call.out_chan) and \
	    (status == "answered"):
		route_msg = call.route_msg

		cdr_data = {}
		cdr_data["time"] = float(cdr_msg.params["time"])
		cdr_data["billid"] = cdr_msg.params["billid"]
		cdr_data["caller"] = route_msg.params["caller"]
		cdr_data["called"] = call.outgoing_number
		cdr_data["caller_addr"] = route_msg.params["address"]
		cdr_data["called_addr"] = cdr_msg.params["address"]
		if call.password != "":
			cdr_data["username"] = call.password
		else:
			cdr_data["username"] = route_msg.params["caller"]
		cdr_data["confid"] = confid

		# save cdr_data for use in updates
		call.cdr_msg = cdr_msg

		callctl.accounting_alive(call, cdr_data, route_msg)


def chan_dtmf(call, dtmf_msg):
	# extract the DTMF char
	text = dtmf_msg.params["text"].strip()

	# set last DTMF time
	call.last_dtmf_time = time.time()

	# reseller level access
	if call.access_level == "Reseller":
		reseller_menu(call, text)
		return

	if call.type != calls.CALLTYPE_IVR:
		slog.info("Received DTMF during call")
		# collect ** or ##
		if (text in "#*5"):
			call.dtmf_cmd += text
		if (call.dtmf_cmd == "##") or (call.dtmf_cmd == "**") or (call.dtmf_cmd == "*5"):
			peerid = dtmf_msg.params.get("peerid","")
			slog.info("Recieved disconnect command during call ",call.dtmf_cmd)
			call.command = CALLCMD_REQDROP
			
			# prompt for destination number again 
			exec_msg = yate.Message("chan.masquerade")
			exec_msg.params["message"] = "call.execute"
			exec_msg.params["id"] = call.in_chan
			exec_msg.params["callto"] = "dumb/"
			exec_msg.send()
		return

	# stop playing any sounds since user pressed a button
	if call.playing:
		call.sound_queue = []

	if call.dropme:
		return

	if call.locked:
		# don't do anything while RADIUS client is doing its job
		return

	# check if we can try to authenticate user
	if (not call.authenticated) and (call.password != "") and \
	    (text in "#*"):
		# start the DTMF timeout thing over again
		call.last_dtmf_time = 0
				
		# try password authentication
		# (+ get user balance from RADIUS)
		auth_user(call, "password")
		return

	# check if we can try to place the outgoing call (received #/*
	# character or maximum number of digits dialled)
	if (call.authenticated) and (call.outgoing_number != "") and \
	    (call.callout == False) and \
	    ((text in "#") or (len(call.outgoing_number) >= call.dest_number_max_digits)):
		# start the DTMF timeout thing over again
		call.last_dtmf_time = 0

		# try to authorize the call
		auth_call(call, call.route_msg, call.outgoing_number)
		return

	# try destination based max digits
	if (call.authenticated) and (call.outgoing_number != "") and \
		(call.callout == False):
		dest_max_digits = conf.get_list("IVR", "dest_max_digits", "," ,["0=20"])
		slog.info("dest_max_digits",dest_max_digits)
		for dmaxdigits in dest_max_digits:
			slog.info("dmaxdigits",dmaxdigits)
			(mprefix, mdigits) = dmaxdigits.split("=")
			if call.outgoing_number.startswith(mprefix) and \
				len(call.outgoing_number) >= mdigits:
				# start the DTMF timeout thing over again
				call.last_dtmf_time = 0

				# try to authorize the call
				auth_call(call, call.route_msg, call.outgoing_number)
				return

	if (call.authenticated) and (call.callout == False):
		# append digits/letters to outgoing number
		if text.isalnum() or text == "*":
			call.outgoing_number += text

		if call.outgoing_number.endswith("**") or call.outgoing_number.endswith("##"):
			slog.info("resetting destination number")
			call.outgoing_number = ""
			callctl.play_map(call, call.dumb_chan, "enter-destination", call.ivr_lang)
			return

		# see if any special command was received
		if call.outgoing_number == conf.get_str("IVR", "balance_dtmf", "*"):
			slog.info("User requested balance check")
			call.outgoing_number = call.dnis # reset the called number back to DNIS
			call.command = CALLCMD_BALPLAY
			call.callout = False
			call.type = calls.CALLTYPE_IVR
			call.num_dn_attempts = 0
			call.last_dtmf_time = 0
			auth_user(call, "default")

	elif (call.callout == False):
		# append digits/letters to user password
		if text.isalnum():
			call.password += text


def chan_disconnected(call, disconnect_msg):
	chan = disconnect_msg.params.get("chan", "")
	id = disconnect_msg.params.get("id", "")
	op = disconnect_msg.params.get("operation", "")
	status = disconnect_msg.params.get("status", "")
	direction = disconnect_msg.params.get("direction", "")
	slog.verb("disconnect_msg call call.in_chan ",call.in_chan)

	if id == call.in_chan:
		# inbound channel disconected, route it to dumb chan
		slog.info ("disconnect_msg message for inbound chan")
		max_dn_attempts = conf.get_int("IVR", "max_dn_attempts", 3)
		slog.info ("dest number attempt", call.num_dn_attempts)
		if call.num_dn_attempts >= max_dn_attempts:
			disconnect_msg.reverse("true")
			disconnect_msg.send()
			return
		
		#	route to dumb channel to play IVR again
		exec_msg = yate.Message("chan.masquerade")
		exec_msg.params["message"] = "call.execute"
		exec_msg.params["id"] = call.in_chan
		exec_msg.params["callto"] = "dumb/"
		exec_msg.send()

	# we should reply to disconnect message
	disconnect_msg.reverse("true")
	disconnect_msg.send()
	return


def chan_notify(call, note_msg):
	"""Notify messages are received whenever a sound has finished playing.

	Since all IVR operations have some sort of informational text played
	before them, this is the logical place to do the most important stuff
	-- like place an outgoing call (the whole point of this module).
	"""
	call.playing = False

	if len(call.sound_queue) == 0:
		if call.dropme:
			callctl.dropchan(call.in_chan)
					
		if call.callout:
			# make the call
			exec_msg = yate.Message("chan.masquerade")
			exec_msg.params["message"] = "call.execute"
			exec_msg.params["id"] = call.in_chan
			exec_msg.params["caller"] = call.route_msg.params["caller"]
			exec_msg.params["called"] = call.outgoing_number
			exec_msg.params["callto"] = call.callout_route
			exec_msg.params["formats"] = call.formats
#			exec_msg.params["timeout"] = call.timeout
			exec_msg.send()
			
			# change call type and set the flag which indicates
			# that we may charge for this call
			call.type = calls.CALLTYPE_STD
			call.billable = True

			return

		if call.last_dtmf_time == 0:
			# start the DTMF timeout thing
			call.last_dtmf_time = time.time()

	# play queued sounds
	callctl.proc_sound_queue(call)

#
# Helper routines
#

def handle_dtmf_timeout(call):
	if call.locked:
		# don't do anything while RADIUS client is doing its job
		return
	
	# start the DTMF timeout thing over again
	call.last_dtmf_time = 0

	# if we are in resellers mode, go to resellers menu
	if call.access_level == "Reseller":
		reseller_menu(call)
		return

	# check if we can try to authenticate user
	if (not call.authenticated) and (call.password == ""):
		auth_failure(call)
		return

	if (not call.authenticated):
		auth_user(call, "password")		
		return

	#
	# we are authenticated if we get past this point
	#

	# check if we can try to place the outgoing call
	if call.outgoing_number == "":
		callout_failure(call, "timeout")
		return
	
	# try to authorize the call
	auth_call(call, call.route_msg, call.outgoing_number)

def handle_dtmf_timeout_during_call(call):
	if call.locked:
		# don't do anything while RADIUS client is doing its job
		return

	# start the DTMF timeout thing over again
	call.last_dtmf_time = 0
	# reset DTMF command
	call.dtmf_cmd = ""


def call_through(call):
	"""Check if called number is in the DID number list and if so play the
	instructions to enter the desired destination number
	"""
	if call.outgoing_number in call.call_through_numbers:
		# set DNIS access number 
		call.dnis = call.outgoing_number
		# no need for the original called number anymore
		call.outgoing_number = ""
				
		# instructions
		callctl.play_map(call, call.dumb_chan, "enter-destination", call.ivr_lang)
		
		return True

	return False

def auth_call(call, route_msg, outgoing_number):
	"""Authorize call.

	Returns (route, remaining time in seconds)
		or None if something went wrong.
	"""
	# get call attributes
	caller = route_msg.params.get("caller", "")
	called = outgoing_number
	route_msg.params["called"] = called
	caller_address = route_msg.params.get("address", "")
	billid = route_msg.params.get("billid", "")

	chan = route_msg.params.get("id","")
	if not chan in calls.conf_ids:
		slog.warn("Channel",chan,"not in conf_ids, probably gone")
		return False

	h323_conf_id = calls.conf_ids[chan]
	# save h323confid for use in first leg accounting request
	slog.info("Saving call.confid value",h323_conf_id)
	call.confid = h323_conf_id

	# remove port number from address
	caller_address = caller_address.split(':')[0]

	# some RADIUS configuration
	radius_enabled = conf.get_bool("RADIUS", "enable")
	routing_enabled = conf.get_bool("RADIUS", "routing")

	slog.info("Access-Request for call to", called)

	# prepare RADIUS auth request
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"User-Name": caller,
		"Framed-IP-Address": caller_address,
		"Calling-Station-Id": caller,
		"Called-Station-Id": called,
		"h323-conf-id": h323_conf_id,
		"Acct-Session-Id": billid
	}
	## enable below line to allow passwordless auth
	# rad_params['Cisco-AVPair'] = "h323-ivr-out=DTL-Ignore-Password:YES"
	# alter call attributes if user entered her password
	if call.password:
		rad_params["User-Name"] = call.password
		rad_params["User-Password"] = [call.password]

	# add digest attributes if available
	user_digest = routes.digest_data.get(caller)
	if user_digest != None:
		rad_params['Digest-Attributes'] = user_digest['digest_attrs']
		rad_params['Digest-Response'] = user_digest['digest_response']
		# refresh timeout timestamp
		user_digest['exp_timestamp'] = time.time() + user_digest['exp_timeout']

	# set DNIS number
	rad_params['Cisco-AVPair'] = "h323-ivr-out=ACCESSCODE:" + call.dnis

	# send auth packet
	calls.lock(call)
	args = (call, route_msg)
	radius.queue_auth(rad_params, authorization_cb, args)

def authorization_cb(rad_result, call, route_msg):
	"""Authorization callback (RADIUS thread calls this)."""
	
	try:
		proc_authorization(rad_result, call, route_msg)
	except:
		slog.err(traceback.format_exc())

	calls.unlock(call)

def proc_authorization(rad_result, call, route_msg):
	"""Process authorization response."""
	
	# get session timeout
	max_duration = Decimal(conf.get_str("CALLS", "max_duration"))

	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		ip_host = route_msg.params.get("ip_host", "")
		slog.warn("RADIUS authorization unsuccessful for host", ip_host, "result code:", rad_result.code)
		failure_cause = "noroutes"
		if rad_result != None:
			# extract failure reason
			h323_return_code = rad_result["h323-return-code"][0]
			h323_return_code = int(h323_return_code.split('=')[1])
			slog.info("RADIUS return code ",h323_return_code)
			if h323_return_code == 12:
				failure_cause = "nocredit"
			if h323_return_code == 3:
				failure_cause = "inuse"

		callout_failure(call, failure_cause)
		return

	# get called number and local user
	called = route_msg.params.get("called", "")
	called_user = routes.getuser(called)

	# get remaining seconds
	if "h323-credit-time" in rad_result:
		rem_sec = rad_result["h323-credit-time"][0]
		rem_sec = int(rem_sec.split('=')[1])
	else:
		rem_sec = max_duration
	slog.info("Remaining seconds:", rem_sec)
	# ivr reported remaining seconds
	ivr_rem_sec = rem_sec
	call.ivr_time = ivr_rem_sec

	# get call rate in cents (or any other currency 1/100 units per minute)
	slog.info("Cisco-AVPair", rad_result["Cisco-AVPair"])
	call_rate = None
	for avpairs in rad_result["Cisco-AVPair"]:
		parts = avpairs.split("=")
		rt = re.compile("^CALLRATE:(.+)")
		du = re.compile("^DURATION:(.+)")
		cn = re.compile("^called-number:(.+)")
		iv = re.compile("^ivr-options:(.+)")
		mrt = rt.match(parts[1])
		mdu = du.match(parts[1])
		mcn = cn.match(parts[1])
		miv = iv.match(parts[1])
		if mrt:
			slog.info("Callrate is: ", mrt.group(1))
			call_rate = int(float(mrt.group(1)) * 100)
		if mdu:
			slog.info("Override remaining seconds: ", mdu.group(1))
			rem_sec = int(float(mdu.group(1)))
		if mcn:
			slog.info("Override Called number: ", mcn.group(1))
			call.outgoing_number = mcn.group(1)
		if miv:
			slog.info("IVR options: ", miv.group(1))
			call.ivr_options = miv.group(1)

	# extract balance
	balance_attr = rad_result.get("h323-credit-amount", [None])[0]
	if balance_attr == None:
		slog.err("Could not extract h323-credit-amount from "
		    "auth response")
		call.balance = 0
	else:
		balance_split = balance_attr.split('=')
		if len(balance_split) != 2:
			slog.err("Invalid balance string:", balance_attr)
			call.balance = 0
		else:
			call.balance = balance_split[1]
			slog.info("Balance:", call.balance)

	# billing might request to recharge now or if we are at zero, do it now
	if (hasattr(call,"ivr_options") and ("recharge" in call.ivr_options)) or call.balance == 0:
		auth_user(call, "password")

	# extract currency
	currency_attr = rad_result.get("h323-currency", [None])[0]
	if currency_attr == None:
		slog.warn("Could not extract h323-currency from auth "
		     "response, setting to USD.")
		call.currency = "USD"
	else:
		currency_split = currency_attr.split('=')
		if len(currency_split) != 2:
			slog.warn("Invalid currency string", currency_attr, "setting to USD")
			call.currency = "USD"
		else:
			call.currency = currency_split[1].upper()
			slog.info("Currency:", call.currency)

	# get preferred language
	if "h323-preferred-lang" in rad_result:
		ivr_lang = rad_result["h323-preferred-lang"][0]
		ivr_lang = ivr_lang.split('=')[1]
	elif hasattr(call, "ivr_lang"):
		ivr_lang = call.ivr_lang
	else:
		ivr_lang = "en"
	slog.info("Preferred language:", ivr_lang)
	call.ivr_lang = ivr_lang

	call.dialed_number = call.outgoing_number
	# add session timeout in millisecs
	route_msg.params["timeout"] = str(rem_sec * 1000)
	slog.info("Attaching remaining time to route message, ms ",route_msg.params["timeout"])

	# send route to local user if possible
	if called_user:
		slog.info("Routing call to local user:", called_user.location)
		callout_success(call, called_user.location, rem_sec, ivr_rem_sec, call_rate)
		return

	# check if we should use RADIUS based routing
	routing_enabled = conf.get_bool("RADIUS", "routing")

	# static routing
	if not routing_enabled:
		static_route = conf.get_str("RADIUS", "static_route")
		if static_route:
			p = re.compile('\{called\}')
			static_route = p.sub(called, static_route)
			slog.info("Static route", static_route)
			callout_success(call, static_route, rem_sec, ivr_rem_sec, call_rate)
			return
		else:
			slog.err("Static route is invalid.")
			callout_failure(call, "noroutes")
			return

	# RADIUS based routing
	if routing_enabled and not rad_result.has_key("Cisco-Command-Code"):
		slog.err("No route from RADIUS.")
		callout_failure(call, "noroutes")
		return

	yate_routes = []
	yate_routes_attrs = []
	routing_attrs = rad_result["Cisco-Command-Code"]
	lateroute_id = 1
	for cmd_code in routing_attrs:
		tokens = cmd_code.split("/")
		if len(tokens) < 7:
			slog.warn("Invalid Cisco-Command-Code attribute"
			    " format")
			continue
	
		gateway = tokens[0]
		subtokens = gateway.split(":")
		protocol = subtokens[0].strip().lower()

		if protocol == "h.323":
			protocol = "h323"

		if protocol not in ("sip", "h323"):
			slog.warn("Invalid protocol:", protocol)
			continue

		dest_number = tokens[3]
		dest_ip = tokens[6]

		# add "sip:" prefix to called number for SIP
		if protocol == "sip":
			dest_number = "sip:" + dest_number

		# try to find if target is registered user 
		user = routes.getuser(dest_ip)
		if user != None:
			# extract port number 
			loc = str(user.location).split('@')
			dest_ip = loc[1]
			
		# create a yate route -- module/number@ip
		yroute = protocol + '/' +  dest_number + '@' +  dest_ip

		# build full routestring
		yroute_attrs = 'callto=' + yroute + ';caller=' + tokens[2] + ';called=' + dest_number + ';domain=' + tokens[6]
		# append line (account) information
		if len(subtokens) > 1:
			account = subtokens[1].strip()
			yroute_attrs = yroute_attrs + ';line=' + account + '@' + dest_ip
		# append codec information
		if len(tokens) > 7 and len(tokens[7]) > 0:
			yroute_attrs = yroute_attrs + ';formats=' + tokens[7]

		yate_routes.append('lateroute/lateroute_' + str(lateroute_id))
		lateroute_id = lateroute_id + 1
		yate_routes_attrs.append(yroute_attrs)

	slog.info("yate routes:", yate_routes)

	if len(yate_routes) == 0:
		slog.err("No valid routes received from RADIUS server.")
		callout_failure(call, "noroutes")
		return

	call.authenticated = True
	call.yate_routes = yate_routes_attrs

	# one-choice routes do not need any special handling here anymore

	# for description of yate's callfork module see
	# http://yate.null.ro/pmwiki/index.php?n=Main.Callfork
	forked_route = "fork " + " | ".join(yate_routes) + ";stoperror=busy"
	slog.info("Forked route:", forked_route)
	call.route_msg.retvalue = forked_route

	callout_success(call, forked_route, rem_sec, ivr_rem_sec, call_rate)

def callout_success(call, yate_route, rem_sec, ivr_rem_sec, call_rate):
	# play remaining time for this call
	call.callout = True
	call.callout_route = yate_route
	call.timeout = rem_sec * 1000
	call.billable = True
	call.num_dn_attempts += 1

	# IVR play mode 
	
	ivr_set_mode = conf.get_str("IVR", "ivr_settings_mode")
	play_time_conf = conf.get_bool("IVR", "time_announce")
	play_rate_conf = conf.get_bool("IVR", "rate_announce")

	# determine the time when low balance should be announced
	if call_rate == 0:
		call_rate = 1
	call.low_balance_time = rem_sec - low_balance_level * 6000 / call_rate
	slog.info("low_balance_time is", call.low_balance_time, "seconds from call start")

	# we should decide here if we need to pickup the phone and play any message
	# or try to route the call

	# answer call in one of the cases:
	#	1) we have called number and we have any of IVR play flags set
	#	2) we dont have called number and we need to prompt for it
	answernow = False

	play_time = False
	if (play_time_conf == 1) and (ivr_set_mode == "conf"):
		play_time = True
		answernow = True

	play_rate = False
	if (play_rate_conf == 1) and (ivr_set_mode == "conf"):
		play_rate = True
		answernow = True

	# the calls to access number should have been answered already
	if call.call_through == True:
		slog.info("callthrough")
		call.callout = True
		# radius mode
		if (hasattr(call,"ivr_options") and (ivr_set_mode == "rad_dtl")):
			if "time" in call.ivr_options:
				callctl.play_map(call, call.dumb_chan, "you-have", call.ivr_lang)
				callctl.play_map(call, call.dumb_chan, sounds.time(call.ivr_time), call.ivr_lang)
			else:
				callctl.play(call, call.dumb_chan, "silence/1")
		# config file mode
		elif play_time == True:
			callctl.play_map(call, call.dumb_chan, "you-have", call.ivr_lang)
			callctl.play_map(call, call.dumb_chan, sounds.time(call.ivr_time), call.ivr_lang)
		# the silence operations here are only to trigger chan.notify
		else:
			callctl.play(call, call.dumb_chan, "silence/1")
		return

	# IVR options could be set by radius
	if (hasattr(call,"ivr_options") and (ivr_set_mode == "rad_dtl")):
		if "time" in call.ivr_options:
			play_time = True
			answernow = True
		if "balance" in call.ivr_options:
			play_balance = True
			answernow = True

	if answernow:
		# route to dumb channel to get it answered later
		call.dumb_chan = call.route_msg.params.get("target","")
		route_msg = call.route_msg
		route_msg.retvalue = "dumb/"
		route_msg.send()
		return
	
	# no IVR mode
	call.type = calls.CALLTYPE_STD
	route_msg = call.route_msg
	route_msg.send()


def callout_failure(call, reason):
	call.num_dn_attempts += 1
	noroutes_sounds = [
		"invalid-destin",
		"enter-destination",
	]
	noroutes_sounds_drop = ["no-route-exists-to-dest", "goodbye"]
	timeout_sounds = "still-waiting"
	timeout_sounds_drop = "goodbye"
	nocredit_sounds = [
		"not-enough-credit",
		"check-number-dial-again",
	]
	nocredit_sounds_drop = ["not-enough-credit", "goodbye"]
	inuse_sounds = ["in-use","goodbye"]
	if reason == "noroutes":
		sounds = noroutes_sounds
		sounds_drop = noroutes_sounds_drop
	elif reason == "nocredit":
		sounds = nocredit_sounds
		sounds_drop = nocredit_sounds_drop
	elif reason == "timeout":
		sounds = timeout_sounds
		sounds_drop = timeout_sounds_drop
	elif reason == "inuse":
		sounds_drop = inuse_sounds
		call.dropme = True
	else:
		slog.err("Unknown reason passed to callout_failure()")

	# FIXME: here we would probably need to answer the call before playing IVR

	max_dn_attempts = conf.get_int("IVR", "max_dn_attempts", 3)
	if call.num_dn_attempts >= max_dn_attempts:
		callctl.dropchan(call.in_chan)
		return
	
	if call.num_dn_attempts < call.max_dn_attempts and call.dropme == False:
		call.outgoing_number = ""
		callctl.play_map(call, call.dumb_chan, sounds, call.ivr_lang)
	else:
		call.dropme = True
		callctl.play_map(call, call.dumb_chan, sounds_drop, call.ivr_lang)

def auth_user(call, auth_type):
	"""Authenticate caller with RADIUS server.

	call -- structure that contains all data about this call
	auth_type -- 'number', 'password' or 'default'

	auth_type = 'number' means we set User-Name attribute to the calling
			number and omit the User-Password attribute.

	auth_type = 'password' means we set User-Name and User-Password to the
			thepassword that user has input (collected from
			DTMFs), accessible here as call.password

	auth_type = 'default' means we will take User-Name and User-Password
			from existing call data. This method should only be used for 
			request of special operation, like balance etc where caller is already
			authenticated and their data is stored in call object

	Returns user balance.
	"""
	# get caller number and IP address from call.route message
	caller = call.route_msg.params.get("caller", "")
	caller_address = call.route_msg.params.get("address", "")

	# remove port number from address
	caller_address = caller_address.split(':')[0]

	# authenticate caller (and get her balance)
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"Framed-IP-Address": caller_address,
		"Calling-Station-Id": caller
	}

	# do we need to register this ANI?
	register_ani = conf.get_bool("IVR", "register_ani", False)
	if register_ani != False and caller.isdigit():
		rad_params['Cisco-AVPair'] = "h323-ivr-out=DTL-Radius-Command:Register-CB-ANI"

	# add digest stuff, if available for this user
	user_digest = routes.digest_data.get(caller)
	if user_digest != None:
		rad_params['Digest-Attributes'] = user_digest['digest_attrs']
		rad_params['Digest-Response'] = user_digest['digest_response']
		# refresh timeout timestamp
		user_digest['exp_timestamp'] = time.time() + user_digest['exp_timeout']

	if auth_type == "number":
		rad_params["User-Name"] = caller
		call.auth_type = "number"
	elif auth_type == "password":
		rad_params["User-Name"] = call.password
		rad_params["User-Password"] = [call.password]
		call.auth_type = "password"
	elif auth_type == "default":
		if call.auth_type == "password":
			rad_params["User-Name"] = call.password
		else:
			rad_params["User-Name"] = caller
		rad_params["User-Password"] = [call.password]
	else:
		raise Exception("invalid authorization type")

	# send auth packet
	calls.lock(call)
	radius.queue_auth(rad_params, authentication_cb, call)

def authentication_cb(rad_result, call):
	"""Authentication callback (RADIUS thread calls this)."""
	
	try:
		proc_authentication(rad_result, call)
	except:
		slog.err(traceback.format_exc())

	calls.unlock(call)

def proc_authentication(rad_result, call):
	"""Process authentication response."""
	
	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		slog.info("RADIUS auth failed")
		if "h323-return-code" in rad_result:
			ret = rad_result["h323-return-code"][0]
			reject_code = ret.split("=")[1]
			auth_failure(call, reject_code)
		else:
			auth_failure(call)
		return

	# check if any special command was received
	if "Cisco-AVPair" in rad_result:
		for avpairs in rad_result["Cisco-AVPair"]:
			parts = avpairs.split("=")
			al = re.compile("^access-level:(.+)")
			ri = re.compile("^reseller-id:(.+)")
			iv = re.compile("^ivr-options:(.+)")
			mal = al.match(parts[1])
			mri = ri.match(parts[1])
			miv = iv.match(parts[1])
			if mal:
				slog.info("Access Level is: ", mal.group(1))
				call.access_level = "Reseller"
			if mri:
				slog.info("Reseller ID: ", mri.group(1))
				call.reseller_id = mri.group(1)
			if miv:
				slog.info("IVR options: ", miv.group(1))
				call.ivr_options = miv.group(1)
		if call.access_level != None and call.reseller_id != None and call.status != "exit":
			reseller_menu(call)
			return
	# force lowering access level because we might get here after exit from menu
	call.access_level = None
	#
	# extract balance
	#
	
	balance_attr = rad_result.get("h323-credit-amount", [None])[0]
	if balance_attr == None:
		slog.err("Could not extract h323-credit-amount from "
		    "auth response")
		auth_success(call, None, None)
		return
	
	balance_split = balance_attr.split('=')
	if len(balance_split) != 2:
		slog.err("Invalid balance string:", balance_attr)
		auth_success(call, None, None)
		return
	
	balance = balance_split[1]
	slog.info("Balance:", balance)

	#
	# extract currency
	#
	
	currency_attr = rad_result.get("h323-currency", [None])[0]
	if currency_attr == None:
		slog.warn("Could not extract h323-currency from auth "
		     "response.")
		auth_success(call, balance, None)
		return
		
	currency_split = currency_attr.split('=')
	if len(currency_split) != 2:
		slog.warn("Invalid currency string", currency_attr)
		auth_success(call, balance, None)
		return

	currency = currency_split[1].upper()
	slog.info("Currency:", currency)

	# get preferred language
	if "h323-preferred-lang" in rad_result:
		ivr_lang = rad_result["h323-preferred-lang"][0]
		ivr_lang = ivr_lang.split('=')[1]
	elif hasattr(call, "ivr_lang"):
		ivr_lang = call.ivr_lang
	else:
		ivr_lang = "en"
	slog.info("Preferred language:", ivr_lang)
	call.ivr_lang = ivr_lang

	auth_success(call, balance, currency)

def auth_success(call, balance, currency):
	# we now know who's calling
	call.authenticated = True
	
	# IVR play mode 
	
	ivr_set_mode = conf.get_str("IVR", "ivr_settings_mode")
 	play_bal_conf = conf.get_bool("IVR", "balance_announce")

	play_balance = False
	if (play_bal_conf == 1) and (ivr_set_mode == "conf"):
		play_balance = True

	# IVR options could be set by radius
	if (hasattr(call,"ivr_options") and (ivr_set_mode == "rad_dtl")):
		if "balance" in call.ivr_options:
			play_balance = True

	# override play balance by call command attribute
	if call.command == CALLCMD_BALPLAY:
		play_balance = True

	slog.info("Play balance is ",play_balance)
	# play account balance if it is available
	if balance != None and play_balance:
		callctl.play_map(call, call.dumb_chan, "welcome", call.ivr_lang)
		# callctl.play(call, call.dumb_chan,
		#    ["your", "account-balance-is"])
		callctl.play_map(call, call.dumb_chan, "card-balance-is", call.ivr_lang)
		balance_sounds = sounds.money(balance, currency)
		callctl.play_map(call, call.dumb_chan, balance_sounds, call.ivr_lang)

	# try the dialled number on first attempt
	if (call.num_dn_attempts == 0) and (not call_through(call)):
		auth_call(call, call.route_msg, call.outgoing_number)
		call.num_dn_attempts = 1
		return

	call.num_pw_attempts = 1

def auth_failure(call, reject_code=1):
	call.num_pw_attempts += 1
	call.password = ""
	reject_code = int(reject_code)
	slog.info("Attempt ",call.num_pw_attempts,"reject code",reject_code)
	# expired accounts reject immediately
	if reject_code == 5:
		call.dropme = True
		callctl.play_map(call, call.dumb_chan, "account-expired", call.ivr_lang)
		return
	# no money- reject and ask for new PIN
	if reject_code == 12:
		callctl.play_map(call, call.dumb_chan, "no-money", call.ivr_lang)
		callctl.play_map(call, call.dumb_chan, "enter-account", call.ivr_lang)
		return
	
	if call.num_pw_attempts == 1:
		callctl.play_map(call, call.dumb_chan, "enter-account", call.ivr_lang)
		return
	
	if call.num_pw_attempts < call.max_pw_attempts + 1:
		callctl.play_map(call, call.dumb_chan, "invalid-account", call.ivr_lang)
		callctl.play_map(call, call.dumb_chan, "enter-account", call.ivr_lang)

	else:
		call.dropme = True
		callctl.play_map(call, call.dumb_chan, "login-fail", call.ivr_lang)

def acct_update(call):
	route_msg = call.route_msg
	if not hasattr(call,"cdr_msg"):
		return
	cdr_msg = call.cdr_msg
	if cdr_msg == False:
		return
	chan = call.out_chan

	cdr_data = {}
	if not chan in calls.conf_ids:
		slog.warn("cannot find chan",chan,"call leg possibly gone")
		return

	cdr_data["time"] = float(cdr_msg.params["time"])
	cdr_data["billid"] = cdr_msg.params["billid"]
	cdr_data["caller"] = route_msg.params["caller"]
	cdr_data["called"] = call.outgoing_number
	cdr_data["caller_addr"] = route_msg.params["address"]
	cdr_data["called_addr"] = cdr_msg.params["address"]
	if call.password != "":
		cdr_data["username"] = call.password
	else:
		cdr_data["username"] = route_msg.params["caller"]
	cdr_data["confid"] = calls.conf_ids[chan]
	callctl.accounting_alive(call, cdr_data, route_msg, True)

def play_message(chan, soundid, notify=False):
	""" Local sound play function. It is used to play various sounds within
		call. We cannot use the one provided by callctl module as it will
		always put sound files in queue instead of playing them in all
		channels simultaneously
	"""
	
	sound = sound_map.scheme[prompt_map][soundid]

	slog.info("Playing message",sound,"into channel",chan)

	msg = yate.Message("chan.masquerade")
	msg.params["message"] = "chan.attach"
	msg.params["id"] = chan
	msg.params["override"] = "wave/play/%s/%s.%s" % (sound_dir, sound, extension)
	if notify != False:
		msg.params["notify"] = chan
	msg.send()

def play_tone(chan, tone, notify=False):
	""" Local tonegeneration function. It is used to generate tones like dial,
		busy within call.
	"""

	slog.info("Playing tone",tone,"into channel",chan)

	msg = yate.Message("chan.masquerade")
	msg.params["message"] = "chan.attach"
	msg.params["id"] = chan
	msg.params["override"] = "tone/%s" % (tone)
	if notify != False:
		msg.params["notify"] = chan
	msg.send()

# routines related to reseller function (account creating and recharge)
def reseller_menu(call, command = False):

	# get caller number and IP address from call.route message
	caller = call.route_msg.params.get("caller", "")
	caller_address = call.route_msg.params.get("address", "")
	# remove port number from address
	caller_address = caller_address.split(':')[0]
	nas_ip_address = conf.get_str("RADIUS", "nas_ip_address")
	res_id = str(call.reseller_id)
	slog.info("call.status", call.status, "command", command)

	if call.status == "menu" or  call.status == None:
		if command == False:
			call.authenticated = True
			callctl.play_map(call, call.dumb_chan, 
				["to-add-client-press-1", "to-add-money-press-2"], call.ivr_lang)
			call.status = "menu"
			return
	# inside the menu
	if call.status == "menu":
		if command == "1":
			callctl.play_map(call, call.dumb_chan,
				["enter-number-to-add"], call.ivr_lang)
			call.status = "menu_1"
			call.last_dtmf_time = 0
			return
		if command == "2":
			callctl.play_map(call, call.dumb_chan,
				["enter-number-to-recharge"], call.ivr_lang)
			call.status = "menu_2"
			call.last_dtmf_time = 0
			return
		if command == "*":
			callctl.play_map(call, call.dumb_chan, "goodbye")
			call.last_dtmf_time = 0
			call.dtmf_cmd = ""
			call.access_level = None
			call.status = "exit"
			auth_user(call, "number")
	# inside menu item 1 (add account)
	if call.status == "menu_1":
		if command != False:
			if command != "#":
				call.dtmf_cmd += command
			else:
				# expire DTMF
				call.last_dtmf_time = 1
			slog.info("call.dtmf_cmd",call.dtmf_cmd)
			return
			
		# got complete number to add
		number = call.dtmf_cmd
		if number == "" or number == "#":
			# no number was received, start over
			slog.info("no number was received, retrying")
			call.dtmf_cmd = ""
			call.last_dtmf_time = 1 # forces DTMF to timeout immediately
			call.status = "menu"
			return
		slog.info("got number to verify", number)
		number_snd = sounds.sequence(number)
		callctl.play_map(call, call.dumb_chan, "you-entered", call.ivr_lang)
		callctl.play_map(call, call.dumb_chan, number_snd, call.ivr_lang)
		callctl.play_map(call, call.dumb_chan, ["to-save-press-1", "to-cancel-press-2"], call.ivr_lang)
		call.status = "menu_1_1"
		return
	# menu item 1 (waiting to confirm or reject)
	if call.status == "menu_1_1":
		if command == "1":
			slog.info("Reseller requesting to add number",call.dtmf_cmd)
			call.status = "menu_1_1_1"
			# send auth packet
			calls.lock(call)
			
			# authenticate caller (and get her balance)
			rad_params = {
				"NAS-IP-Address": nas_ip_address,
				"Framed-IP-Address": caller_address,
				"Calling-Station-Id": caller,
				"Cisco-AVPair": "h323-ivr-out=DTL-Radius-Command:Custom/1/" + res_id + "/" + call.dtmf_cmd
			}
			call.dtmf_cmd = ""
			radius.queue_auth(rad_params, res_cmd_cb, call)
			return
		slog.info("cancelling and returning to menu")
		call.dtmf_cmd = ""
		call.status = "menu"
		call.last_dtmf_time = 1 # forces DTMF to timeout immediately
		return
	# inside menu item 2 (recharge account)
	if call.status == "menu_2":
		if command != False:
			if command != "#":
				call.dtmf_cmd += command
			else:
				# expire DTMF
				call.last_dtmf_time = 1
			slog.info("call.dtmf_cmd",call.dtmf_cmd)
			return
			
		# got complete number to add
		number = call.dtmf_cmd
		if number == "" or number == "#":
			# no number was received, start over
			slog.info("no number was received, retrying")
			call.dtmf_cmd = ""
			call.last_dtmf_time = 1 # forces DTMF to timeout immediately
			call.status = "menu"
			return
		slog.info("got number to add ", number)
		number_snd = sounds.sequence(number)
		call.dtmf_cmd += ";"
		callctl.play_map(call, call.dumb_chan, "enter-amount", call.ivr_lang)
		call.status = "menu_2_1"
		return
	if call.status == "menu_2_1":
		# collecting the amount of money
		if command != False:
			if command != "#":
				call.dtmf_cmd += command
			else:
				# expire DTMF
				call.last_dtmf_time = 1
			slog.info("call.dtmf_cmd",call.dtmf_cmd)
			return
			
		# got complete number to add
		number = call.dtmf_cmd
		if number == "" or number == "#":
			# no number was received, start over
			slog.info("no number was received, retrying")
			call.dtmf_cmd = ""
			call.last_dtmf_time = 1 # forces DTMF to timeout immediately
			call.status = "menu"
			return
		slog.info("got number to verify", number)
		num = number.split(";")
		number_snd = sounds.sequence(num[0])
		amount_snd = sounds.integer(num[1])
		callctl.play_map(call, call.dumb_chan, "you-entered", call.ivr_lang)
		callctl.play_map(call, call.dumb_chan, number_snd, call.ivr_lang)
		callctl.play_map(call, call.dumb_chan, "and", call.ivr_lang)
		callctl.play_map(call, call.dumb_chan, amount_snd, call.ivr_lang)
		callctl.play_map(call, call.dumb_chan, ["dollars", "to-save-press-1", "to-cancel-press-2"], call.ivr_lang)
		call.status = "menu_2_1_1"
		return
	# menu item 2 (waiting to confirm or reject)
	if call.status == "menu_2_1_1":
		if command == "1":
			slog.info("Reseller requesting to recharge account",call.dtmf_cmd)
			call.status = "menu_2_1_1_1"
			# send auth packet
			calls.lock(call)
			
			# authenticate caller (and get her balance)
			rad_params = {
				"NAS-IP-Address": nas_ip_address,
				"Framed-IP-Address": caller_address,
				"Calling-Station-Id": caller,
				"Cisco-AVPair": "h323-ivr-out=DTL-Radius-Command:Custom/2/" + res_id + "/" + call.dtmf_cmd
			}
			call.dtmf_cmd = ""
			radius.queue_auth(rad_params, res_cmd_cb, call)
			return
		slog.info("cancelling and returning to menu")
		call.dtmf_cmd = ""
		call.status = "menu"
		call.last_dtmf_time = 1 # forces DTMF to timeout immediately
		return

def res_cmd_cb(rad_result, call):
	"""Reseller command callback (RADIUS thread calls this)."""
	
	try:
		proc_res_cmd(rad_result, call)
	except:
		slog.err(traceback.format_exc())

	calls.unlock(call)

def proc_res_cmd(rad_result, call):
	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		slog.info("RADIUS response failed.")
		slog.info(rad_result)
		callctl.play_map(call, call.dumb_chan, "an-error-has-occurred", call.ivr_lang)
		call.status = "menu"
		call.last_dtmf_time = 0
		reseller_menu(call)
		return
	# process accept
	if call.status == "menu_2_1_1_1":
		callctl.play_map(call, call.dumb_chan, "phone-number-was-recharged", call.ivr_lang)
	else:
		callctl.play_map(call, call.dumb_chan, "phone-number-was-added", call.ivr_lang)
	call.status = "menu"
	call.last_dtmf_time = 0
	reseller_menu(call)
	return