"""
Callback module.
"""

from wobble import slog, conf
from aswitch import yate, calls, callctl, routes, return_codes

#
# Message handlers
#

def call_route(call, route_msg):
	if not hasattr(call, "leg1_route_msg"):
		# call.route for 1st leg

		call.id = route_msg.params["callername"]
		call.type = calls.CALLTYPE_NONE
		
		call.leg1_dumb_chan = route_msg.params["id"]
		call.leg2_dumb_chan = None
		
		call.leg1_answered = False
		call.leg2_answered = False

		call.leg1_chan = None
		call.leg2_chan = None

		call.leg1_route_msg = route_msg
		call.leg2_route_msg = None

		call.leg1_called = route_msg.params.get("called", "") # save leg 1 called number

		# determine if callback request contains password
		caller = route_msg.params["caller"]
		number_tuple = caller.split(':')
		if len(number_tuple) == 2:
			route_msg.params["caller"] = number_tuple[0]
			route_msg.params["password"] = number_tuple[1]

		# route call for 1st leg
		routes.authorize_call(call, route_msg)
		return



	# catch any lateroute messages here - use special called: lateroute_XXX param
	called = route_msg.params.get("called", "")
	if called.startswith("lateroute"):
		callstr = called.split(";")[0]
		lateroute_id = int(callstr.split("_")[1])
		slog.info("lateroute ID", lateroute_id)
		# replace attributes from call object
		if hasattr(call,"yate_routes"):
			rstr = call.yate_routes[lateroute_id-1].rsplit(';')
			slog.info("routestring",rstr)
			for rdata in rstr:
				(key,val) = rdata.rsplit('=')
				route_msg.params[key] = val
				if key == "callto":
					route_msg.retvalue = val
			route_msg.reverse("true")
			route_msg.send()
			return
		else:
			# sometimes we get here- not sure why
			route_msg.reverse("true")
			route_msg.send()
			return
	
	
		slog.err("Extra call.route message for callback module.")
		return
	
	call.leg2_dumb_chan = route_msg.params["id"]
	call.leg2_route_msg = route_msg

	calls.add_channel(call, call.leg2_dumb_chan)

	# check if 1st leg has gone away (before we perform routing for 2nd
	# leg)
	if call.leg1_chan == None:
		slog.warn("1st leg has gone away, not doing routing for 2nd.")
		callctl.reject(route_msg, "First leg gone")
		return
		
	# route call for 2nd leg

	# determine if callback request contains password
	cb_numbers = call.cb_numbers
	number_tuple = cb_numbers.split(':')
	if len(number_tuple) == 4:
		route_msg.params["caller"] = number_tuple[0]
		route_msg.params["password"] = number_tuple[3]

	routes.authorize_call(call, route_msg)

def call_conference(call, conf_msg):
	call.type = calls.CALLTYPE_CONF
	
	call.conf_room = conf_msg.params["room"]
	call.conf_chan = conf_msg.params["peerid"]

	connect_leg2(call)

def call_execute(call, exec_msg):
	if call.type == calls.CALLTYPE_STD:
		# no need for tone generating channel anymore
		callctl.dropchan(call.leg1_dumb_chan)
	
	if "cb_numbers" in exec_msg.params:
		# store all callback numbers
		call.cb_numbers = exec_msg.params["cb_numbers"]

	chan = exec_msg.params.get("id")
	target_chan = exec_msg.params.get("targetid")

	# check if call.execute concerns 2nd leg, and if 1st leg has gone away,
	# drop the 2nd leg
	if (chan == call.leg2_dumb_chan) and (call.leg1_chan == None):
		slog.warn("1st leg has gone away, dropping 2nd.")
		callctl.smartdrop(call, call.leg2_dumb_chan)
		call.leg2_dumb_chan = None

def call_answered(call, answer_msg):
	chan = answer_msg.params.get("id")

	if call.leg1_answered == False:
		call.leg1_chan = chan
		call.leg1_answered = True
		
		# play ringing tone
		callctl.play_tone(call, call.leg1_dumb_chan, "ring")

		conf_msg = yate.Message("chan.masquerade")
		conf_msg.params["message"] = "call.conference"
		conf_msg.params["id"] = call.leg1_chan
		conf_msg.send()
		return

	if call.leg2_answered == False:
		call.leg2_chan = chan
		call.leg2_answered = True
		
		# stop playing sounds
		callctl.play_tone(call, call.leg1_dumb_chan, "silence")

		# connect 2nd leg to conference
		exec_msg = yate.Message("chan.masquerade")
		exec_msg.params["message"] = "call.execute"
		exec_msg.params["id"] = call.leg2_chan
		exec_msg.params["callto"] = call.conf_room
		exec_msg.send()

		call.type = calls.CALLTYPE_STD
		return

	slog.err("Too many answers for callback call.")

def call_cdr(call, cdr_msg):
	chan = cdr_msg.params.get("chan", "")
	op = cdr_msg.params.get("operation", "")
	status = cdr_msg.params.get("status", "")
	direction = cdr_msg.params.get("direction", "")
	caller_ip = conf.get_str("RADIUS", "nas_ip_address") # NAS is caller for both legs
	called = None

	# the first leg which got active or disconnected will use confid stored in call structure
	# that is to match conf ids in auth and acct messages
	if not chan.startswith("dumb/"):
		if hasattr(call,"confid"):
			confid = call.confid
			calls.conf_ids[chan] = confid
			del call.confid
			slog.info("replacing confid for chan",chan,"with",confid)
		else:
			slog.info("call has no confid attribute, using confid from chan",chan)
			confid = calls.conf_ids[chan]

	# check if 1st leg hung up (stop the call if so)
	if (op == "finalize") and (chan == call.leg1_chan):
		slog.info("1st leg hung up.")
		call.leg1_chan = None
		callctl.smartdrop(call, call.leg2_dumb_chan)
		req_tuple = call.cb_numbers.split(':')
		called = req_tuple[1]
		if hasattr(call, "dialed_number"):
			del(call.dialed_number)
		route_msg = call.leg1_route_msg
	# check if routing for 2nd call leg failed or it just hung up
	# (stop the call if so)
	elif (op == "finalize") and (chan == call.leg2_dumb_chan) and \
	    (not call.leg2_answered):
		slog.warn("2nd call leg failed to route or hung up.")
		req_tuple = call.cb_numbers.split(':')
		called = req_tuple[2]
		call.dialed_number = called
		callctl.smartdrop(call, call.leg2_dumb_chan)
		callctl.dropchan(call.leg1_chan)
	# leg2 stop message
	elif (op == "finalize") and (chan == call.leg2_chan):
		slog.info("2nd leg hung up.")
		req_tuple = call.cb_numbers.split(':')
		called = req_tuple[2]
		call.dialed_number = called
		route_msg = call.leg2_route_msg
	# first leg failed to route
	else: 
		slog.warn("1st leg failed to route")
		route_msg = call.leg1_route_msg

	if op == "finalize":
		calls.rm_channel(call, chan)
		req_tuple = call.cb_numbers.split(':')
		#called = req_tuple[1]

	# accounting stops for all routes
	if (op == "finalize") and (direction == "outgoing") and \
	    (not chan.startswith("dumb/")):
		req_tuple = call.cb_numbers.split(':')
		cdr_data = {}
		cdr_data["duration"] = float(cdr_msg.params["duration"])
		cdr_data["billtime"] = float(cdr_msg.params["billtime"])
		cdr_data["time"] = float(cdr_msg.params["time"])
		# XXX this is real hack (but sometimes there is no billid for failed calls)
		billid = cdr_msg.params.get("billid", "")
		if billid != "":
			cdr_data["billid"] = cdr_msg.params["billid"]
		else:
			cdr_data["billid"] = cdr_msg.params["runid"]
		cdr_data["caller"] = req_tuple[0]
		cdr_data["caller_addr"] = caller_ip
		cdr_data["called_addr"] = cdr_msg.params["address"]
		cdr_data["username"] = req_tuple[0]
		if cdr_data["billtime"] != 0.0:
			cdr_data["dcause"] = 200
		else:
			reason = cdr_msg.params.get("reason", "Unspecified error")
			cdr_data["dcause"] = return_codes.sipResponseToSipCode.get(reason, 0)
		# calledfull and called numbers
		calledfull = cdr_msg.params.get("calledfull", "")
		if calledfull != "":
			cdr_data["calledfull"] = calledfull
		else:
			slog.warn("Using called as calledfull", called)
			cdr_data["calledfull"] = called
		call.dialed_number = called
		cdr_data["called"] = called
		cdr_data["confid"] = confid
		slog.info("cdr_data", cdr_data)
		callctl.accounting_stop(call, cdr_data, route_msg)

#
# Helper routines
#

def connect_leg2(call):	
	cb_numbers = call.cb_numbers
	number_tuple = cb_numbers.split(':')
	if len(number_tuple) == 3:
		caller_number, cb_number, called_number = cb_numbers.split(':')
	elif len(number_tuple) == 4:
		caller_number, cb_number, called_number, password = cb_numbers.split(':')
	else:
		slog.err("Incorrect number of cb_numbers")
		return

	exec_msg = yate.Message("call.execute")
	exec_msg.params["target"] = called_number
	exec_msg.params["callto"] = "dumb/"
	exec_msg.params["caller"] = caller_number
	exec_msg.params["callername"] = call.id
	exec_msg.send()
