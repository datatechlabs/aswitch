
import time, traceback, re
from wobble import slog, conf
from aswitch import yate, calls, sounds, callctl, radius, sound_map, routes
import bsdradius.pyrad.packet as packet

lang_map = conf.get_str("MOD_BALANCE", "lang_map", "999111=en,999112=de")

#
# Message handlers
#

def engine_timer(call, msg):
	pass

def call_route(call, route_msg):
	# route the call to a 'dumb' channel
	route_msg.retvalue = "dumb/"
	route_msg.send()

	call.type = calls.CALLTYPE_IVR	# set call type
	call.route_msg = route_msg	# save routing message
	call.num_pw_attempts = 0
	call.max_pw_attempts = conf.get_int("CALLS", "max_attempts")
	call.access_level = None
	call.status = None
	call.dropme = False
	call.authenticated = False
	call.password = ""

	# extract incoming channel
	call.in_chan = route_msg.params["id"]

def call_execute(call, exec_msg):
	# extract channels
	call.dumb_chan = exec_msg.params["targetid"]

	#
	# detect language based on called number
	#

	called = exec_msg.params["called"]
	call.lang = "en"
	langrules = lang_map.split(",")
	for rule in langrules:
		(num, l) = rule.split("=")
		if num == called:
			slog.info("dialed number",called,"language",l)
			call.lang = l
			break

	# pick up the phone
	callctl.answer(call)

	callctl.play(call, call.dumb_chan, "silence/1")

	# try initial authentication 
	auth_user(call, "number")

def call_conference(call, conf_msg):
	pass

def call_answered(call, answer_msg):
	pass

def call_cdr(call, cdr_msg):
	chan = cdr_msg.params.get("chan", "")
	op = cdr_msg.params.get("operation", "")

	if op == "finalize":
		calls.rm_channel(call, chan)

def chan_notify(call, note_msg):
	call.playing = False
	
	if len(call.sound_queue) == 0:
		if call.dropme:
			callctl.dropchan(call.in_chan)
			return

	# play queued sounds
	callctl.proc_sound_queue(call)

def chan_dtmf(call, dtmf_msg):
	# extract the DTMF char
	text = dtmf_msg.params["text"].strip()

	# set last DTMF time
	call.last_dtmf_time = time.time()

	# menu access level
	if call.access_level == "usermenu":
		menu(call, text)
		return

	# check if we can try to authenticate user
	if (not call.authenticated) and (call.password != "") and \
	    (text in "#*"):
		# start the DTMF timeout thing over again
		call.last_dtmf_time = 0
				
		# try password authentication
		# (+ get user balance from RADIUS)
		auth_user(call, "password")
		return

	if text.isalnum():
		call.password += text

def auth_user(call, auth_type):
	"""Authenticate caller with RADIUS server.

	call -- structure that contains all data about this call
	auth_type -- 'number', 'password' or 'default'

	auth_type = 'number' means we set User-Name attribute to the calling
			number and omit the User-Password attribute.

	auth_type = 'password' means we set User-Name and User-Password to the
			thepassword that user has input (collected from
			DTMFs), accessible here as call.password

	auth_type = 'default' means we will take User-Name and User-Password
			from existing call data. This method should only be used for 
			request of special operation, like balance etc where caller is already
			authenticated and their data is stored in call object

	Returns user balance.
	"""
	# get caller number and IP address from call.route message
	caller = call.route_msg.params.get("caller", "")
	caller_address = call.route_msg.params.get("address", "")

	# remove port number from address
	caller_address = caller_address.split(':')[0]

	# authenticate caller (and get her/his balance)
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"Framed-IP-Address": caller_address,
		"Calling-Station-Id": caller
	}

	# do we need to register this ANI?
	register_ani = conf.get_bool("IVR", "register_ani", False)
	if register_ani != False and caller.isdigit():
		rad_params['Cisco-AVPair'] = "h323-ivr-out=DTL-Radius-Command:Register-CB-ANI"

	# add digest stuff, if available for this user
	user_digest = routes.digest_data.get(caller)
	if user_digest != None:
		rad_params['Digest-Attributes'] = user_digest['digest_attrs']
		rad_params['Digest-Response'] = user_digest['digest_response']
		# refresh timeout timestamp
		user_digest['exp_timestamp'] = time.time() + user_digest['exp_timeout']

	if auth_type == "number":
		rad_params["User-Name"] = caller
		call.auth_type = "number"
	elif auth_type == "password":
		rad_params["User-Name"] = call.password
		rad_params["User-Password"] = [call.password]
		call.auth_type = "password"
	elif auth_type == "default":
		if call.auth_type == "password":
			rad_params["User-Name"] = call.password
		else:
			rad_params["User-Name"] = caller
		rad_params["User-Password"] = [call.password]
	else:
		raise Exception("invalid authorization type")

	# send auth packet
	calls.lock(call)
	radius.queue_auth(rad_params, authentication_cb, call)

def authentication_cb(rad_result, call):
	"""Authentication callback (RADIUS thread calls this)."""
	
	try:
		proc_authentication(rad_result, call)
	except:
		slog.err(traceback.format_exc())

	calls.unlock(call)

def proc_authentication(rad_result, call):
	"""Process authentication response."""
	
	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		slog.info("RADIUS auth failed (cannot get account balance).")
		auth_failure(call)
		return

	#
	# extract balance
	#
	
	balance_attr = rad_result.get("h323-credit-amount", [None])[0]
	if balance_attr == None:
		slog.err("Could not extract h323-credit-amount from "
		    "auth response")
		auth_success(call, None, None)
		return
	
	balance_split = balance_attr.split('=')
	if len(balance_split) != 2:
		slog.err("Invalid balance string:", balance_attr)
		auth_success(call, None, None)
		return
	
	balance = balance_split[1]
	slog.info("Balance:", balance)

	#
	# extract currency
	#
	
	currency_attr = rad_result.get("h323-currency", [None])[0]
	if currency_attr == None:
		slog.warn("Could not extract h323-currency from auth "
		     "response.")
		auth_success(call, balance, None)
		return
		
	currency_split = currency_attr.split('=')
	if len(currency_split) != 2:
		slog.warn("Invalid currency string", currency_attr)
		auth_success(call, balance, None)
		return

	currency = currency_split[1].upper()
	slog.info("Currency:", currency)
	auth_success(call, balance, currency)

def auth_success(call, balance, currency):
	# we now know who's calling
	call.authenticated = True
	
	# play account balance if it is available
	if balance != None:
		callctl.play_map(call, call.dumb_chan, "welcome", call.lang)
		# callctl.play(call, call.dumb_chan,
		#    ["your", "account-balance-is"])
		callctl.play_map(call, call.dumb_chan, "card-balance-is", call.lang)
		balance_sounds = sounds.money(balance, currency, call.lang)
		callctl.play_map(call, call.dumb_chan, balance_sounds, call.lang)
		call.dropme = True
		# go on to the main menu
#		call.access_level = "usermenu"
#		call.status == "menu"
#		menu(call, False)

	call.num_pw_attempts = 1

def auth_failure(call):
	call.num_pw_attempts += 1
	call.password = ""
	slog.info("Attempt ",call.num_pw_attempts)
	if call.num_pw_attempts == 1:
		callctl.play_map(call, call.dumb_chan, "enter-account", call.lang)
		return
	
	if call.num_pw_attempts < call.max_pw_attempts + 1:
		callctl.play_map(call, call.dumb_chan, "invalid-account", call.lang)
		callctl.play_map(call, call.dumb_chan, "enter-account", call.lang)

	else:
		call.dropme = True
		callctl.play_map(call, call.dumb_chan, "login-fail", call.lang)

def menu(call, command=False):
	""" Main menu """
	if call.status == "menu" or  call.status == None:
		if command == False:
			call.authenticated = True
			callctl.play_map(call, call.dumb_chan, 
				["available-options", "press-1", "press-2"])
			call.status = "menu"
			return
	# inside the menu
	# top level
	if call.status == "menu":
		if command == "1":
			callctl.play_map(call, call.dumb_chan,
				["please-enter-the", "number-to-add"])
			call.status = "menu_1"
			call.last_dtmf_time = 0
			return
		if command == "2":
			callctl.play_map(call, call.dumb_chan,
				["please-enter-the", "number-to-recharge"])
			call.status = "menu_2"
			call.last_dtmf_time = 0
			return
		if command == "*":
			callctl.play_map(call, call.dumb_chan, "goodbye")
			call.last_dtmf_time = 0
			call.dtmf_cmd = ""
			call.access_level = None
			call.status = "exit"
			auth_user(call, "number")

	
	