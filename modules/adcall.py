# 
# this is IVR module compatible with YATE1.3 series. Please use this module
# with YATE versions prior to 2.0.0. This is due to YATE ignoring timeout 
# in chan.masquerade message which causes calls with IVR never to time out
#
# This is now official IVR module and should be used in any cases

import time, traceback, re
from wobble import slog, conf
from aswitch import yate, calls, callctl, sounds, radius, routes, return_codes
import bsdradius.pyrad.packet as packet

callto_breakup = re.compile("([0-9]+)@([^\s]+)")

CALLCMD_NONE = 0
CALLCMD_BALPLAY = 1
CALLCMD_REQDROP = 2

acct_update_enable = conf.get_bool("RADIUS", "acct_update_enable", False)
acct_update_interval = conf.get_int("RADIUS", "acct_update_interval", 60)

sound_dir = conf.get_str("PATHS", "sound_dir")
extension = conf.get_str("PATHS", "extension")

beep_offset = 10 

#
# Message handlers
#

def engine_timer(call, msg):
	# check if DTMF timeout has been reached
	time_diff = time.time() - call.last_dtmf_time
	# check if call drop time is reached (incl safety margin)
	if hasattr(call,"timeout") and hasattr(call,"answer_time"):
	
		# play beep before disconnect message
		if call.answer_time -2 + call.timeout/1000 - beep_offset < time.time():
			if call.dropmsg_beep_done == False:
			
				play_message(call.in_chan, "beep", notify=False)
				play_message(call.out_chan, "beep", notify=False)
				call.dropmsg_beep_done = True
					
		# play disconnect message 
		if call.answer_time -2 + call.timeout/1000  < time.time():
			if call.dropmsg_done == False:
				end_msg = call.dropmsg_id
				
				slog.info("playing end message and disconnecting the call")

				play_message(call.in_chan, "adcalldefmedia"+str(end_msg), notify=True)
				play_message(call.out_chan, "adcalldefmedia"+str(end_msg), notify=True)
				call.dropmsg_done = True
				call.dropme = True

		# play beep before end warning message 
		if call.answer_time - 60 + call.timeout/1000 - beep_offset < time.time():
			if call.endwarnmsg_beep_done == False:
				play_message(call.in_chan, "beep", notify=False)
				play_message(call.out_chan, "beep", notify=False)
				call.endwarnmsg_beep_done = True

		# play end warning message 60 sec before drop time
		if call.answer_time - 60 + call.timeout/1000  < time.time():
			if call.endwarnmsg_done == False:
				warn_msg = call.endwarnmsg_id
				#	play message into in chan

				play_message(call.in_chan, "adcalldefmedia"+str(warn_msg), notify=False)
				play_message(call.out_chan, "adcalldefmedia"+str(warn_msg), notify=False)
				call.endwarnmsg_done = True
				return 

	# check for media play events 
	if hasattr(call,"answer_time"):

		dur = int(time.time() - call.answer_time)
		if len(call.media_ids) > 0 and len(call.media_times) > 0:
		
			# check if need to beep
			if len(call.media_beeptimes) > 0:
				if dur >= int(call.media_beeptimes[0]) - beep_offset:
					play_message(call.in_chan, "beep", notify=False)
					play_message(call.out_chan, "beep", notify=False)
					call.media_beeptimes.remove(call.media_beeptimes[0])
		
			# take first interval and compare if time reached
			if dur >= int(call.media_times[0]):
				media_id = call.media_ids[0]
			
				#	play message into in and out chan
				play_message(call.in_chan, "adcallmedia"+str(media_id), notify=False)
				play_message(call.out_chan, "adcallmedia"+str(media_id), notify=False)
				
				call.media_ids.remove(call.media_ids[0])
				call.media_times.remove(call.media_times[0])
				# append to list of played media for acct purposes
				call.media_acct.append(str(media_id))

	# DTMF timeout events
	if (call.last_dtmf_time != 0) and (time_diff > call.dtmf_timeout) and \
		(call.type == calls.CALLTYPE_IVR):
		handle_dtmf_timeout(call)

	if (call.last_dtmf_time != 0) and (time_diff > call.dtmf_timeout) and \
		(call.type == calls.CALLTYPE_STD):
		handle_dtmf_timeout_during_call(call)

	# check for accounting update timers 
	if acct_update_enable and hasattr(call,"answer_time"):
		call.update_timer = call.update_timer + 1
		if 	call.update_timer >= acct_update_interval:
			acct_update(call)
			call.update_timer = 0


def call_route(call, route_msg):
	# route the call to a 'dumb' channel
	route_msg.retvalue = "dumb/"
	route_msg.send()

	call.type = calls.CALLTYPE_IVR	# set call type
	call.billable = False		# the IVR part is not billable
	call.route_msg = route_msg	# save routing message

	# extract incoming channel & set outgoing to empty string
	call.in_chan = route_msg.params["id"]
	call.out_chan = ""
	call.target_chan = ""   # target chan is chan which is expected to connect 
	call.tried_routes = [] # pairs of (number, address)

	call.dropme = False		# whether to drop the call
	call.callout = False		# whether to place an outgoing call
	call.authenticated = False	# whether we have authenticated user
	if "formats" in route_msg.params:
		call.formats = route_msg.params["formats"]	# media formats (codecs)
	else:
		call.formats = ""
		slog.warn("call.route message without formats.")

	# set outgoing number and initial password (which may not be required)
	call.outgoing_number = ""
	call.password = ""

	# set DTMF command string
	call.dtmf_cmd = ""
	call.command = CALLCMD_NONE		# special call handling commands

	# set DNIS number - IVR access number
	call.dnis = ""

	# list of Media IDs which were played. Used in accounting at the end of call
	call.media_acct = []
	call.media_beeptimes = []
	
	# flags to check if particular msg was played or no
	call.endwarnmsg_done = False
	call.dropmsg_done = False
	call.endwarnmsg_beep_done = False
	call.dropmsg_beep_done = False
	
	# get call-through (DID) number list
	call.call_through_numbers = conf.get_list("CALLS",
	    "call_through_numbers")
	
	# don't wait for messages forever
	call.timeout_tick = conf.get_int("CALLS", "timeout_tick")
	call.dtmf_timeout = conf.get_int("CALLS", "dtmf_timeout")
	call.last_dtmf_time = 0	# when was the last DTMF received
	call.update_timer = 0 # accounting update timer

	# maximum number of digits in outgoing number
	call.dest_number_max_digits = conf.get_int("CALLS",
	    "dest_number_max_digits")

	# maximum retries (password and destination number)
	call.max_pw_attempts = conf.get_int("CALLS", "max_attempts")
	call.max_dn_attempts = conf.get_int("CALLS", "max_attempts")
	call.num_pw_attempts = 0
	call.num_dn_attempts = 0
			
def call_execute(call, exec_msg):
	# save outgoing channel ID
	chan = exec_msg.params.get("id")
	target_chan = exec_msg.params.get("targetid", "")
	if target_chan == "":
		slog.warn("call.execute message without target channel ID.")
		return

	# each leg can have its own confid, so we generate it here
	h323_conf_id = radius.gen_h323_conf_id()
	call.confid = h323_conf_id
	
	call.target_chan = target_chan
	# gather tried routes if we're in callout mode
	if call.type == calls.CALLTYPE_STD:
		callto = exec_msg.params.get("callto")
		if callto == None:
			return

		if (chan == call.in_chan) and target_chan.startswith("fork/"):
			# not interested in incoming_chan->fork_chan
			return

#		# this could be return after outgoing leg disconnect
#		if (chan == call.in_chan) and target_chan.startswith("dumb/"):
#			call.dumb_chan = target_chan

#			# pick up the phone
#			callctl.answer(call)
#			# prompt for destination number
#			if call.command == CALLCMD_REQDROP:
#				# we dropped it, prompt for destination
#				callctl.play_map(call, call.dumb_chan, "enter-destination")
#			else:
#				# it probably gone
#				callctl.play_map(call, call.dumb_chan, ["destin-busy", "enter-destination"])
#			# reset call
#			call.outgoing_number = ""
#			call.dtmf_cmd = ""
#			call.last_dtmf_time = 0
#			call.callout = False
#			call.dropme = False
#			call.tried_routes = []
#			call.type = calls.CALLTYPE_IVR
#			return

		matches = callto_breakup.findall(callto)
		if len(matches) != 1:
			slog.warn("Unrecognized 'callto' parameter in "
			    "call.execute.")
			return

		call.tried_routes.append(matches[0])
		return

	call.dumb_chan = target_chan

	# pick up the phone
	callctl.answer(call)
	callctl.play(call, call.dumb_chan, "silence/1")

	# save access number
	call.access_number =  exec_msg.params.get("called","")

	# try initial authentication
	auth_user(call, "number")

def call_answered(call, answer_msg):
	chan = answer_msg.params.get("id")
	target_chan = answer_msg.params.get("targetid")

	# set outgoing channel when it answers
	if (target_chan == call.in_chan) and (chan != call.dumb_chan):
		call.out_chan = chan
		slog.info("Setting outgoing channel to:", chan)
		call.answer_time = time.time()
		slog.info("Setting answer time to", time.time())

def call_cdr(call, cdr_msg):
	chan = cdr_msg.params.get("chan", "")
	op = cdr_msg.params.get("operation", "")
	status = cdr_msg.params.get("status", "")
	direction = cdr_msg.params.get("direction", "")

	# the first leg which got active or disconnected will use confid stored in call structure
	# that is to match conf ids in auth and acct messages
	if ((op == "finalize") and (direction == "outgoing")) or \
	    ((op == "update") and (chan == call.out_chan) and \
	    (status == "answered")):
		if hasattr(call,"confid"):
			confid = call.confid
			calls.conf_ids[chan] = confid
			del call.confid
			slog.info("replacing confid for chan",chan,"with",confid)
		else:
			slog.info("call has no confid attribute, using confid from chan",chan)
			confid = calls.conf_ids[chan]

	slog.info ("call_cdr message for direction ",direction)
	# remove outgoing leg, keep the call on
	if (op == "finalize") and (direction == "outgoing"):
		slog.info("remove outgoing leg")
		calls.rm_channel(call, chan, True)

	# remove incoming leg
	if (op == "finalize") and (direction == "incoming"):
		slog.info("remove incoming leg")
		calls.rm_channel(call, chan, True)	

	# accounting stops for all routes
	if (op == "finalize") and (direction == "outgoing") and \
	    (not chan.startswith("dumb/")):
		route_msg = call.route_msg
		
		cdr_data = {}
		cdr_data["duration"] = float(cdr_msg.params["duration"])
		cdr_data["billtime"] = float(cdr_msg.params["billtime"])
		cdr_data["time"] = float(cdr_msg.params["time"])
		cdr_data["billid"] = cdr_msg.params["billid"]
		cdr_data["caller"] = route_msg.params["caller"]
		cdr_data["called"] = call.outgoing_number
		cdr_data["calledfull"] = call.outgoing_number
		cdr_data["caller_addr"] = route_msg.params["address"]
		cdr_data["called_addr"] = cdr_msg.params["address"]
		if call.password != "":
			cdr_data["username"] = call.password
		else:
			cdr_data["username"] = route_msg.params["caller"]
		if cdr_data["billtime"] != 0.0:
			cdr_data["dcause"] = 200
		else:
			reason = cdr_msg.params.get("reason", "Unspecified error")
			cdr_data["dcause"] = return_codes.sipResponseToSipCode.get(reason, 0)
		# set DNIS number
		cdr_data["dnis_access"] = call.access_number
		cdr_data["confid"] = confid
		# append list of played media Ids
		cdr_data["options"] = ';'.join(call.media_acct)
		


		callctl.accounting_stop(call, cdr_data, route_msg)

	# accounting alive
	if (op == "update") and (chan == call.out_chan) and \
	    (status == "answered"):
		route_msg = call.route_msg

		cdr_data = {}
		cdr_data["time"] = float(cdr_msg.params["time"])
		cdr_data["billid"] = cdr_msg.params["billid"]
		cdr_data["caller"] = route_msg.params["caller"]
		cdr_data["called"] = call.outgoing_number
		cdr_data["caller_addr"] = route_msg.params["address"]
		cdr_data["called_addr"] = cdr_msg.params["address"]
		if call.password != "":
			cdr_data["username"] = call.password
		else:
			cdr_data["username"] = route_msg.params["caller"]
		cdr_data["confid"] = confid
		# save cdr_data for use in updates
		call.cdr_msg = cdr_msg
		callctl.accounting_alive(call, cdr_data, route_msg)


def chan_dtmf(call, dtmf_msg):
	# extract the DTMF char
	text = dtmf_msg.params["text"].strip()

	# set last DTMF time
	call.last_dtmf_time = time.time()

	if call.type != calls.CALLTYPE_IVR:
		slog.info("Received DTMF during call")
		# collect ** or ##
		if (text in "#*5"):
			call.dtmf_cmd += text
		if (call.dtmf_cmd == "##") or (call.dtmf_cmd == "**") or (call.dtmf_cmd == "*5"):
			slog.info("Recieved disconnect command during call ",call.dtmf_cmd)
			call.command = CALLCMD_REQDROP
			# disconnect outgoing call leg
			#calls.rm_channel(call, call.out_chan)
		#	callctl.dropchan(call.out_chan)	
			callctl.dropchan(call.target_chan)	
		return


	# stop playing any sounds since user pressed a button
	if call.playing:
		call.sound_queue = []

	if call.dropme:
		return

	if call.locked:
		# don't do anything while RADIUS client is doing its job
		return

	# check if we can try to authenticate user
	if (not call.authenticated) and (call.password != "") and \
	    (text in "#*"):
		# start the DTMF timeout thing over again
		call.last_dtmf_time = 0
				
		# try password authentication
		# (+ get user balance from RADIUS)
		auth_user(call, "password")
		return

	# check if we can try to place the outgoing call (received #/*
	# character or maximum number of digits dialled)
	if (call.authenticated) and (call.outgoing_number != "") and \
	    (call.callout == False) and \
	    ((text in "#") or (len(call.outgoing_number) >= call.dest_number_max_digits)):
		# start the DTMF timeout thing over again
		call.last_dtmf_time = 0

		# try to authorize the call
		auth_call(call, call.route_msg, call.outgoing_number)
		return

	# try destination based max digits
	if (call.authenticated) and (call.outgoing_number != "") and \
		(call.callout == False):
		dest_max_digits = conf.get_list("IVR", "dest_max_digits", "," ,["0=20"])
		slog.info("dest_max_digits",dest_max_digits)
		for dmaxdigits in dest_max_digits:
			slog.info("dmaxdigits",dmaxdigits)
			(mprefix, mdigits) = dmaxdigits.split("=")
			if call.outgoing_number.startswith(mprefix) and \
				len(call.outgoing_number) >= mdigits:
				# start the DTMF timeout thing over again
				call.last_dtmf_time = 0

				# try to authorize the call
				auth_call(call, call.route_msg, call.outgoing_number)
				return

	if (call.authenticated) and (call.callout == False):
		# append digits/letters to outgoing number
		if text.isalnum() or text == "*":
			call.outgoing_number += text

		if call.outgoing_number.endswith("**") or call.outgoing_number.endswith("##"):
			slog.info("resetting destination number")
			call.outgoing_number = ""
			callctl.play_map(call, call.dumb_chan, "enter-destination")
			return

		# see if any special command was received
		if call.outgoing_number == conf.get_str("IVR", "balance_dtmf", "*5"):
			slog.info("User requested balance check")
			call.outgoing_number = call.dnis # reset the called number back to DNIS
			call.command = CALLCMD_BALPLAY
			call.callout = False
			call.type = calls.CALLTYPE_IVR
			call.num_dn_attempts = 0
			call.last_dtmf_time = 0
			auth_user(call, "default")

	elif (call.callout == False):
		# append digits/letters to user password
		if text.isalnum():
			call.password += text


def chan_disconnected(call, disconnect_msg):
	chan = disconnect_msg.params.get("chan", "")
	id = disconnect_msg.params.get("id", "")
	op = disconnect_msg.params.get("operation", "")
	status = disconnect_msg.params.get("status", "")
	direction = disconnect_msg.params.get("direction", "")
	slog.verb("disconnect_msg call call.in_chan ",call.in_chan)

	# we should reply to disconnect message
	disconnect_msg.reverse("true")
	disconnect_msg.send()
	return

#	if id == call.in_chan:
#		# inbound channel disconected, route it to dumb chan
#		slog.info ("disconnect_msg message for inbound chan")
#		#	route to dumb channel to play IVR again
#		exec_msg = yate.Message("chan.masquerade")
#		exec_msg.params["message"] = "call.execute"
#		exec_msg.params["id"] = call.in_chan
#		exec_msg.params["callto"] = "dumb/"
#		exec_msg.send()



def chan_notify(call, note_msg):
	"""Notify messages are received whenever a sound has finished playing.

	Since all IVR operations have some sort of informational text played
	before them, this is the logical place to do the most important stuff
	-- like place an outgoing call (the whole point of this module).
	"""
	call.playing = False

	if len(call.sound_queue) == 0:
		if call.dropme:
			callctl.dropchan(call.in_chan)
					
		if call.callout:
			# make the call
			exec_msg = yate.Message("chan.masquerade")
			exec_msg.params["message"] = "call.execute"
			exec_msg.params["id"] = call.in_chan
			exec_msg.params["caller"] = call.route_msg.params["caller"]
			exec_msg.params["called"] = call.outgoing_number
			exec_msg.params["callto"] = call.callout_route
			exec_msg.params["formats"] = call.formats
#			exec_msg.params["timeout"] = call.timeout
			exec_msg.send()
			
			# change call type and set the flag which indicates
			# that we may charge for this call
			call.type = calls.CALLTYPE_STD
			call.billable = True

			return

		if call.last_dtmf_time == 0:
			# start the DTMF timeout thing
			call.last_dtmf_time = time.time()

	# play queued sounds
	callctl.proc_sound_queue(call)

#
# Helper routines
#

def handle_dtmf_timeout(call):
	slog.info("DTMF timeout has occured")
	if call.locked:
		# don't do anything while RADIUS client is doing its job
		return
	
	# start the DTMF timeout thing over again
	call.last_dtmf_time = 0
			
	# check if we can try to authenticate user
	if (not call.authenticated) and (call.password == ""):
		auth_failure(call)
		return

	if (not call.authenticated):
		auth_user(call, "password")		
		return

	#
	# we are authenticated if we get past this point
	#

	# check if we can try to place the outgoing call
	if call.outgoing_number == "":
		slog.info("Destination number not collected")
		callout_failure(call, "timeout")
		return
	
	# try to authorize the call
	auth_call(call, call.route_msg, call.outgoing_number)

def handle_dtmf_timeout_during_call(call):
	if call.locked:
		# don't do anything while RADIUS client is doing its job
		return

	# start the DTMF timeout thing over again
	call.last_dtmf_time = 0
	# reset DTMF command
	call.dtmf_cmd = ""


def call_through(call):
	"""Check if called number is in the DID number list and if so play the
	instructions to enter the desired destination number
	"""
	if call.outgoing_number in call.call_through_numbers:
		# set DNIS access number 
		call.dnis = call.outgoing_number
		# no need for the original called number anymore
		call.outgoing_number = ""
				
		# instructions
		callctl.play_map(call, call.dumb_chan, "enter-destination")
		
		return True

	return False

def auth_call(call, route_msg, outgoing_number):
	"""Authorize call.

	Returns (route, remaining time in seconds)
		or None if something went wrong.
	"""
	# get call attributes
	caller = route_msg.params.get("caller", "")
	called = outgoing_number
	route_msg.params["called"] = called
	caller_address = route_msg.params.get("address", "")
	billid = route_msg.params.get("billid", "")

	h323_conf_id = radius.gen_h323_conf_id()
	# save h323confid for use in first leg accounting request
	slog.info("Saving call.confid value",h323_conf_id)

	# remove port number from address
	caller_address = caller_address.split(':')[0]

	# some RADIUS configuration
	radius_enabled = conf.get_bool("RADIUS", "enable")
	routing_enabled = conf.get_bool("RADIUS", "routing")

	slog.info("Access-Request for call to", called)

	# prepare RADIUS auth request
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"User-Name": caller,
		"Framed-IP-Address": caller_address,
		"Calling-Station-Id": caller,
		"Called-Station-Id": called,
		"h323-conf-id": h323_conf_id,
		"Acct-Session-Id": billid
	}


	# set DNIS number
	rad_params['Cisco-AVPair'] = "h323-ivr-out=ACCESSCODE:" + call.access_number

	# send auth packet
	calls.lock(call)
	args = (call, route_msg)
	radius.queue_auth(rad_params, authorization_cb, args)

def authorization_cb(rad_result, call, route_msg):
	"""Authorization callback (RADIUS thread calls this)."""
	
	try:
		proc_authorization(rad_result, call, route_msg)
	except:
		slog.err(traceback.format_exc())

	calls.unlock(call)

def proc_authorization(rad_result, call, route_msg):
	"""Process authorization response."""
	
	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		slog.err("RADIUS auth for outgoing call failed, result code: ", rad_result.code)
		failure_cause = "noroutes"
		if rad_result != None:
			# extract failure reason
			h323_return_code = rad_result["h323-return-code"][0]
			h323_return_code = int(h323_return_code.split('=')[1])
			slog.info("RADIUS return code ",h323_return_code)
			if h323_return_code == 12:
				failure_cause = "nocredit"
			if h323_return_code == 3:
				failure_cause = "inuse"

		callout_failure(call, failure_cause)
		return

	# get called number and local user
	called = route_msg.params.get("called", "")
	called_user = routes.getuser(called)

	# get remaining seconds
	rem_sec = rad_result["h323-credit-time"][0]
	rem_sec = int(rem_sec.split('=')[1])
	slog.info("Remaining seconds:", rem_sec)
	# ivr reported remaining seconds
	ivr_rem_sec = rem_sec

	# get call rate in cents (or any other currency 1/100 units per minute)
	slog.info("Cisco-AVPair", rad_result["Cisco-AVPair"])
	call_rate = None
	for avpairs in rad_result["Cisco-AVPair"]:
		parts = avpairs.split("=")
		r1 = re.compile("^media-ids:(.+)")
		r2 = re.compile("^media-times:(.+)")
		r3 = re.compile("^called-number:(.+)")
		r4 = re.compile("^endwarn-media-id:(.+)")
		r5 = re.compile("^drop-media-id:(.+)")
		r1t = r1.match(parts[1])
		r2t = r2.match(parts[1])
		r3t = r3.match(parts[1])
		r4t = r4.match(parts[1])
		r5t = r5.match(parts[1])
		if r1t:
			slog.info("Media ID list: ", r1t.group(1))
			media_ids = r1t.group(1).split(";")
			slog.info("Media IDs", media_ids)
			call.media_ids = media_ids
		if r2t:
			slog.info("Media Time list: ", r2t.group(1))
			media_times = r2t.group(1).split(";")
			media_beeptimes = r2t.group(1).split(";")
			slog.info("Media Times", media_times)
			call.media_times = media_times
			call.media_beeptimes = media_beeptimes
		if r3t:
			slog.info("Override Called number: ", r3t.group(1))
			call.outgoing_number = r3t.group(1)
		if r4t:
			slog.info("End warning media ID: ", r4t.group(1))
			call.endwarnmsg_id = r4t.group(1)
		if r5t:
			slog.info("Call drop media ID: ", r5t.group(1))
			call.dropmsg_id = r5t.group(1)

	# add session timeout in millisecs
	route_msg.params["timeout"] = str(rem_sec * 1000)
	slog.info("Attaching remaining time to route message, ms ",route_msg.params["timeout"])

	# send route to local user if possible
	if called_user:
		slog.info("Routing call to local user:", called_user.location)
		callout_success(call, called_user.location, rem_sec, ivr_rem_sec, call_rate)
		return

	# check if we should use RADIUS based routing
	routing_enabled = conf.get_bool("RADIUS", "routing")

	# static routing
	if not routing_enabled:
		static_route = conf.get_str("RADIUS", "static_route")
		if static_route:
			p = re.compile('\{called\}')
			static_route = p.sub(called, static_route)
			slog.info("Static route", static_route)
			callout_success(call, static_route, rem_sec, ivr_rem_sec, call_rate)
			return
		else:
			slog.err("Static route is invalid.")
			callout_failure(call, "noroutes")
			return

	# RADIUS based routing
	if routing_enabled and not rad_result.has_key("Cisco-Command-Code"):
		slog.err("No route from RADIUS.")
		callout_failure(call, "noroutes")
		return

	yate_routes = []
	routing_attrs = rad_result["Cisco-Command-Code"]
	for cmd_code in routing_attrs:
		tokens = cmd_code.split("/")
		if len(tokens) < 7:
			slog.warn("Invalid Cisco-Command-Code attribute"
			    " format")
			continue
	
		protocol = tokens[0].strip().lower()
		if protocol == "h.323":
			protocol = "h323"

		if protocol not in ("sip", "h323"):
			slog.warn("Invalid protocol:", protocol)
			continue

		dest_number = tokens[3]
		dest_ip = tokens[6]

		# add "sip:" prefix to called number for SIP
		if protocol == "sip":
			dest_number = "sip:" + dest_number

		# create a yate route -- module/number@ip
		yroute = protocol + '/' +  dest_number + '@' +  dest_ip
		yate_routes.append(yroute)

	slog.info("yate routes:", yate_routes)

	if len(yate_routes) == 0:
		slog.err("No valid routes received from RADIUS server.")
		callout_failure(call, "noroutes")
		return

	if len(yate_routes) == 1:
		# single route
		callout_success(call, yate_routes[0], rem_sec, ivr_rem_sec, call_rate)
		return

	# for description of yate's callfork module see
	# http://yate.null.ro/pmwiki/index.php?n=Main.Callfork
	forked_route = "fork " + " | ".join(yate_routes)
	slog.info("Forked route:", forked_route)

	callout_success(call, forked_route, rem_sec, ivr_rem_sec, call_rate)

def callout_success(call, yate_route, rem_sec, ivr_rem_sec, call_rate):
	# play remaining time for this call
	call.callout = True
	call.callout_route = yate_route
	call.timeout = rem_sec * 1000

	# IVR play mode 
	
	ivr_set_mode = conf.get_str("IVR", "ivr_settings_mode")
	play_time_conf = conf.get_bool("IVR", "time_announce")
	play_rate_conf = conf.get_bool("IVR", "rate_announce")

	play_time = False
	if (play_time_conf == 1) and (ivr_set_mode == "conf"):
		play_time = True

	play_rate = False
	if (play_rate_conf == 1) and (ivr_set_mode == "conf"):
		play_rate = True


	if play_time:
		callctl.play_map(call, call.dumb_chan, "you-have")
		callctl.play_map(call, call.dumb_chan, sounds.time(ivr_rem_sec))

	if play_rate and call_rate:
		callctl.play(call, call.dumb_chan, "this-call-will-cost")
		call_rate_sounds = sounds.integer(call_rate)
		callctl.play(call, call.dumb_chan, call_rate_sounds)
		callctl.play(call, call.dumb_chan, "cents-per-minute")

	if (not play_time) and (not play_rate):
		# just dumb operation
		callctl.play(call, call.dumb_chan, "silence/1")


def callout_failure(call, reason):
	noroutes_sounds = [
		"invalid-destin",
		"enter-destination",
	]
	noroutes_sounds_drop = ["no-route-exists-to-dest", "goodbye"]
	timeout_sounds = "still-waiting"
	timeout_sounds_drop = "goodbye"
	nocredit_sounds = [
		"not-enough-credit",
		"check-number-dial-again",
	]
	nocredit_sounds_drop = ["not-enough-credit", "goodbye"]
	inuse_sounds = ["in-use","goodbye"]
	if reason == "noroutes":
		sounds = noroutes_sounds
		sounds_drop = noroutes_sounds_drop
	elif reason == "nocredit":
		sounds = nocredit_sounds
		sounds_drop = nocredit_sounds_drop
	elif reason == "timeout":
		sounds = call.dest_prompt_media_id
		sounds_drop = timeout_sounds_drop    #???
	elif reason == "inuse":
		sounds_drop = inuse_sounds
		call.dropme = True
	else:
		slog.err("Unknown reason passed to callout_failure()")
	
	if call.num_dn_attempts < call.max_dn_attempts and call.dropme == False:
		call.outgoing_number = ""
		call.num_dn_attempts += 1
		callctl.play_raw(call, call.dumb_chan, "adcalldefmedia"+str(sounds))
	else:
		call.dropme = True
		callctl.play_raw(call, call.dumb_chan, "adcalldefmedia"+str(sounds_drop))

def auth_user(call, auth_type):
	"""Authenticate caller with RADIUS server.

	call -- structure that contains all data about this call
	auth_type -- 'number', 'password' or 'default'

	auth_type = 'number' means we set User-Name attribute to the calling
			number and omit the User-Password attribute.

	auth_type = 'password' means we set User-Name and User-Password to the
			thepassword that user has input (collected from
			DTMFs), accessible here as call.password

	auth_type = 'default' means we will take User-Name and User-Password
			from existing call data. This method should only be used for 
			request of special operation, like balance etc where caller is already
			authenticated and their data is stored in call object

	Returns user balance.
	"""
	# get caller number and IP address from call.route message
	caller = call.route_msg.params.get("caller", "")
	caller_address = call.route_msg.params.get("address", "")

	# remove port number from address
	caller_address = caller_address.split(':')[0]

	# authenticate caller (and get her balance)
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"Framed-IP-Address": caller_address,
		"Calling-Station-Id": caller,
		"Cisco-AVPair": "h323-ivr-out=DTL-Module:adcall"
	}

	# add digest stuff, if available for this user
#	user_digest = routes.digest_data.get(caller)
#	if user_digest != None:
#		rad_params['Digest-Attributes'] = user_digest['digest_attrs']
#		rad_params['Digest-Response'] = user_digest['digest_response']
#		# refresh timeout timestamp
#		user_digest['exp_timestamp'] = time.time() + user_digest['exp_timeout']

	if auth_type == "number":
		rad_params["User-Name"] = caller
		rad_params['Cisco-AVPair'] = "h323-ivr-out=ACCESSCODE:" + call.access_number
	elif auth_type == "password":
		rad_params["User-Name"] = call.password
		rad_params["User-Password"] = [call.password]
	elif auth_type == "default":
		rad_params["User-Name"] = caller
		rad_params["User-Password"] = [call.password]
	else:
		raise Exception("invalid authorization type")

	# send auth packet
	calls.lock(call)
	radius.queue_auth(rad_params, authentication_cb, call)

def authentication_cb(rad_result, call):
	"""Authentication callback (RADIUS thread calls this)."""
	
	try:
		proc_authentication(rad_result, call)
	except:
		slog.err(traceback.format_exc())

	calls.unlock(call)

def proc_authentication(rad_result, call):
	"""Process authentication response."""
	
	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		slog.info("RADIUS auth failed (user is not registered).")
		auth_failure(rad_result, call)
		return

	#
	# extract media ID to play
	#
	prompt_media_id = 0
	if "Cisco-AVPair" in rad_result:
		for avpairs in rad_result["Cisco-AVPair"]:
			parts = avpairs.split("=")
			m1 = re.compile("^media-id:(.+)")
			m2 = re.compile("^prompt-media-id:(.+)")

			m1t = m1.match(parts[1])
			m2t = m2.match(parts[1])

			if m1t:
				slog.info("Media ID: ", m1t.group(1))
				media_id = int(m1t.group(1))
			if m2t:
				slog.info("Prompt Media ID: ", m2t.group(1))
				prompt_media_id = int(m2t.group(1))
				call.dest_prompt_media_id = prompt_media_id

	auth_success(call, media_id, prompt_media_id)

def auth_success(call, media_id, prompt_media_id):
	# we now know who's calling
	call.authenticated = True
	# play initial welcome message
	callctl.play_raw(call, call.dumb_chan, "adcallmedia"+str(media_id))
	call.media_acct.append(str(media_id))
	# play destination prompt
	callctl.play_raw(call, call.dumb_chan, "adcalldefmedia"+str(prompt_media_id))

def auth_failure(rad_result, call):
	# the user is not registered 
	# we will play info message and quit
	
	if "Cisco-AVPair" in rad_result:
		for avpairs in rad_result["Cisco-AVPair"]:
			parts = avpairs.split("=")
			m1 = re.compile("^media-id:(.+)")
			m2 = re.compile("^language-menu:(.+)")

			m1t = m1.match(parts[1])
			m2t = m2.match(parts[1])

			if m1t:
				slog.info("Media ID: ", m1t.group(1))
				media_id = int(m1t.group(1))
			if m2t:
				slog.info("Language Menu play: ", m2t.group(1))
				menu_play = bool(m2t.group(1))

	# drop call after media play - user not allowed
	call.dropme = True
	callctl.play_raw(call, call.dumb_chan, "adcalldefmedia"+str(media_id))

def acct_update(call):
	route_msg = call.route_msg
	cdr_msg = call.cdr_msg
	chan = call.out_chan

	cdr_data = {}
	cdr_data["time"] = float(cdr_msg.params["time"])
	cdr_data["billid"] = cdr_msg.params["billid"]
	cdr_data["caller"] = route_msg.params["caller"]
	cdr_data["called"] = call.outgoing_number
	cdr_data["caller_addr"] = route_msg.params["address"]
	cdr_data["called_addr"] = cdr_msg.params["address"]
	if call.password != "":
		cdr_data["username"] = call.password
	else:
		cdr_data["username"] = route_msg.params["caller"]
		
	cdr_data["confid"] = calls.conf_ids[chan]
	callctl.accounting_alive(call, cdr_data, route_msg, True)
	
def play_message(chan, sound, notify=False):
	""" Local sound play function. It is used to play vaious sounds within
		call. We cannot use the one provided by callctl module as it will
		always put sound files in queue instead of playing them in all
		channels simultaneously
	"""

	slog.info("Playing message",sound,"into channel",chan)

	msg = yate.Message("chan.masquerade")
	msg.params["message"] = "chan.attach"
	msg.params["id"] = chan
	msg.params["override"] = "wave/play/%s/%s.%s" % (sound_dir, sound, extension)
	if notify != False:
		msg.params["notify"] = chan
	msg.send()
