"""
ANI Callback trigger module.
"""

import time, telnetlib, Queue, traceback, re
from wobble import slog, conf
from aswitch import yate, calls, callctl, routes, radius, sounds, return_codes
from threading import Thread
import bsdradius.pyrad.packet as packet

acct_update_enable = conf.get_bool("RADIUS", "acct_update_enable", False)
acct_update_interval = conf.get_int("RADIUS", "acct_update_interval", 60)
allow_aleg_authentication = conf.get_bool("MOD_ANI_CALLBACK", "allow_aleg_authentication", False)
aleg_pin_auth = conf.get_bool("MOD_ANI_CALLBACK", "aleg_pin_authentication", False)

CALLCMD_NONE = 0
CALLCMD_BALPLAY = 1
CALLCMD_REQDROP = 2

#
# Message handlers
#

def engine_timer(call, msg):
	# check if DTMF timeout has been reached
	time_diff = time.time() - call.last_dtmf_time
	# check if call drop time is reached (incl safety margin)
	if hasattr(call,"timeout") and hasattr(call,"answer_time"): 
		if call.answer_time -2 + call.timeout/1000  < time.time():
			if call.leg1_chan in call.channels:
				slog.info("disconnecting the call")
				callctl.dropchan(call.leg1_chan)
				calls.rm_channel(call, call.leg1_chan)

	# check if the trigger leg has ringed enough
	if hasattr(call,"trigger_chan_ringtime"):
		trigger_ringtime = conf.get_int("MOD_ANI_CALLBACK", "trigger_ringtime", 5)
		if ((call.trigger_chan_ringtime + trigger_ringtime < time.time()) and call.trigger_dropped == False):
			slog.info("Trigger ringed",trigger_ringtime,"seconds, disconnecting")
			callctl.dropchan(call.trigger_chan_id)
			call.trigger_dropped = True

	if (call.last_dtmf_time != 0) and (time_diff > call.dtmf_timeout) and \
		(call.type == calls.CALLTYPE_IVR):
		handle_dtmf_timeout(call)

	if (call.last_dtmf_time != 0) and (time_diff > call.dtmf_timeout) and \
		(call.type == calls.CALLTYPE_STD):
		handle_dtmf_timeout_during_call(call)
	# check for accounting update timers (out leg)
	if acct_update_enable and hasattr(call,"answer_time") and hasattr(call,"leg2_chan"):
		call.update_timer = call.update_timer + 1
		if 	call.update_timer >= acct_update_interval:
			acct_update(call, call.leg2_chan, 2)
			call.update_timer = 0
	# check for accounting update timers (in leg)
	if acct_update_enable and hasattr(call,"leg1_answered") and hasattr(call,"leg1_chan"):
		if call.leg1_answered == True:
			call.leg1_update_timer = call.leg1_update_timer + 1
			if 	call.leg1_update_timer >= acct_update_interval:
				acct_update(call, call.leg1_chan, 1)
				call.leg1_update_timer = 0




def call_route(call, route_msg):

	callername = route_msg.params.get("callername", "")
	call.last_dtmf_time = 0	# when was the last DTMF received
	call.outgoing_number = ""	# we dont have outgoing number yet
	call.update_timer = 0 # accounting update timer
	call.leg1_update_timer = 0 # accounting update timer

	# catch any lateroute messages here - use special called: lateroute_XXX param
	called = route_msg.params.get("called", "")
	if called.startswith("lateroute"):
		callstr = called.split(";")[0]
		lateroute_id = int(callstr.split("_")[1])
		slog.info("lateroute ID", lateroute_id)
		# replace attributes from call object
		if hasattr(call,"yate_routes"):
			rstr = call.yate_routes[lateroute_id-1].rsplit(';')
			slog.info("routestring",rstr)
			for rdata in rstr:
				(key,val) = rdata.rsplit('=')
				route_msg.params[key] = val
				if key == "callto":
					route_msg.retvalue = val
			route_msg.reverse("true")
			route_msg.send()
			return
		else:
			# sometimes we get here- not sure why
			route_msg.reverse("true")
			route_msg.send()
			return

	if not callername.startswith("ani_callback_"):
		slog.info("ANI callback inbound trigger request")
		call.type = calls.CALLTYPE_NONE
		chan = route_msg.params.get("id", "")
		call.trigger_chan = True
		call.trigger_chan_id = chan
		call.trigger_chan_ringtime = time.time()
		call.trigger_dropped = False

		call.leg1_chan = None
		call.leg2_chan = None

		call.leg1_dumb_chan = None
		call.leg2_dumb_chan = None

		call.authenticated = False

		# ANI which needs to be called back
		# check if requested to manipulate prefix
		caller = route_msg.params.get("caller", "")

		prefix_rule = conf.get_str("MOD_ANI_CALLBACK", "inbound_prefix_rule", "")
		rules = prefix_rule.split(",")
		for rule in rules:
			try:
				(pattern, rewrite) = rule.split("=")
				slog.info("prefix rules: pattern:",pattern," rewrite:",rewrite,"subject:",caller)
				modcaller = re.sub(pattern, rewrite, caller)
				caller = modcaller
				slog.info("rewritten to ",caller)
			except:
				slog.warn("ani_callback: error in inbound_prefix_rule")

		call.cb_ani = caller
		# route call for 1st leg
		slog.info("Trying to authorize ANI ", caller)
		auth(route_msg, call)

		# we will redirect call to dumb chan in order to get ring tone
		route_msg.retvalue = "dumb/"
		route_msg.send()

		return

	if hasattr(call, "leg1_route_msg") == False:
		slog.info("ANI callback leg1 route request")
		# call.route for 1st leg

		call.id = route_msg.params["callername"]
		call.type = calls.CALLTYPE_IVR
		
		call.leg1_dumb_chan = route_msg.params["id"]
		call.leg2_dumb_chan = None
		call.trigger_chan = None
		
		call.leg1_answered = False
		call.leg2_answered = False
		call.leg1_start_sent = False

		call.leg1_chan = None
		call.leg2_chan = None

		call.leg1_route_msg = route_msg
		call.leg2_route_msg = None

		# set DTMF command string
		call.dtmf_cmd = ""
		call.command = CALLCMD_NONE		# special call handling commands

		# don't wait for messages forever
		call.timeout_tick = conf.get_int("CALLS", "timeout_tick")
		call.dtmf_timeout = conf.get_int("CALLS", "dtmf_timeout")
	
		call.dropme = False		# whether to drop the call
		call.callout = False		# whether to place an outgoing call
		
		if aleg_pin_auth == True:
			call.dtmfnostop = True
		else:
			call.dtmfnostop = False	# whether to not stop digit collection on # or *

		# maximum number of digits in outgoing number
		call.dest_number_max_digits = conf.get_int("CALLS",
		    "dest_number_max_digits")

		call.route_msg = route_msg	# save routing message
		call.leg1_called = route_msg.params.get("called", "") # save leg 1 called number

		# set DNIS number - IVR access number
		call.dnis = ""
		call.password = ""

		# maximum retries (password and destination number)
		call.max_pw_attempts = conf.get_int("CALLS", "max_attempts")
		call.max_dn_attempts = conf.get_int("CALLS", "max_attempts")
		call.num_pw_attempts = 0
		call.num_dn_attempts = 0

		# formats, i.e. codecs should match
		if "formats" in route_msg.params:
			call.formats = route_msg.params["formats"]	# media formats (codecs)
		else:
			call.formats = "" # dont leave this blank, as it may cause rejects from far end!!!
			slog.warn("call.route message without formats.")

		if allow_aleg_authentication != False:
			call.aleg_route = True

		# route call for 1st leg
		routes.authorize_call(call, route_msg)
		return


	# catch any lateroute messages here - use special called: lateroute_XXX param
	called = route_msg.params.get("called", "")
	if called.startswith("lateroute"):
		callstr = called.split(";")[0]
		lateroute_id = int(callstr.split("_")[1])
		slog.info("lateroute ID", lateroute_id)
		# replace attributes from call object
		if hasattr(call,"yate_routes"):
			rstr = call.yate_routes[lateroute_id-1].rsplit(';')
			slog.info("routestring",rstr)
			for rdata in rstr:
				(key,val) = rdata.rsplit('=')
				route_msg.params[key] = val
				if key == "callto":
					route_msg.retvalue = val
			route_msg.reverse("true")
			route_msg.send()
			return
		else:
			# sometimes we get here- not sure why
			route_msg.reverse("true")
			route_msg.send()
			return

	if call.leg2_route_msg != None:
		slog.err("Extra call.route message for callback module.")
		return
	
	call.leg2_dumb_chan = route_msg.params["id"]
	call.leg2_route_msg = route_msg

	calls.add_channel(call, call.leg2_dumb_chan)

	# check if 1st leg has gone away (before we perform routing for 2nd
	# leg)
	if call.leg1_chan == None:
		slog.warn("1st leg has gone away, not doing routing for 2nd.")
		callctl.reject(route_msg, "First leg gone")
		return

# this function authorizes callback trigger leg
def auth(msg, call):
	# remove port number from IP address
	address = msg.params.get("address", "").split(':')[0]

	# get caller parameter
	caller = call.cb_ani
	if caller == "":
		slog.info("ANI not present in user.auth message,"
		    " access denied.")
		msg.reverse("false")
		msg.send()
		return

	# prepare RADIUS auth request
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"Framed-IP-Address": address,
		"Calling-Station-Id": caller,
		"Cisco-AVPair": "h323-ivr-out=DTL-Radius-Command:Request-ANI-CB-Aleg"
	}
	# put plain text user password if it exists
	if "password" in msg.params:
		rad_params["User-Password"] = [msg.params["password"]]

	# put digest attibutes in request
	digestAttrs = None
	digestResponse = None
	args = (msg, digestAttrs, digestResponse, call)
	radius.queue_auth(rad_params, radius_auth_cb, args)

# we provide our own radius callback function which will always reject the
# call regardless of the result

def radius_auth_cb(rad_result, msg, digestAttrs, digestResponse, call):
	"""ANI Callback Authorization callback."""
	
	if allow_aleg_authentication != False:
		slog.info("Call back and authenticate on A-leg")
		return
	
	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		slog.warn("RADIUS authentication unsuccessful.")
		call.trigger_chan = None
		return

	# see what we got
	slog.info("RADIUS authentication successful, but we reject it anyway.")
	slog.verb("Answer from RADIUS server:", str(rad_result))


def chan_disconnected(call, disconnect_msg):
	chan = disconnect_msg.params.get("chan", "")
	id = disconnect_msg.params.get("id", "")
	
	if call.trigger_chan == True:
		caller = call.cb_ani
		slog.info("Initiating callback trigger to number ", caller)

		request = rconnect(caller)
		request.start()	
		# we should reply to disconnect message
		disconnect_msg.reverse("true")
		disconnect_msg.send()
		return

	if id == call.leg1_chan:
		# inbound channel disconected, route it to dumb chan
		slog.info ("disconnect_msg message for inbound chan")
		#	route to dumb channel to play IVR again
		exec_msg = yate.Message("chan.masquerade")
		exec_msg.params["message"] = "call.execute"
		exec_msg.params["id"] = call.leg1_chan
		exec_msg.params["callto"] = "dumb/"
		exec_msg.send()
		return

	# we should reply to disconnect message
	disconnect_msg.reverse("true")
	disconnect_msg.send()
	return

class rconnect(Thread):
	"""Rmanager telnet connection class in separate thread which connects to rmanager 
	and sends callback request
	"""
	def __init__(self, caller):
		Thread.__init__(self)
		self.caller = caller
	def run(self):
		ip_address = conf.get_str("SERVER", "ip_address")
		trigger_pause = conf.get_int("MOD_ANI_CALLBACK", "trigger_pause", 1)
		slog.info("waiting",trigger_pause,"seconds before calling back")
		time.sleep(trigger_pause)
		port = 5038
		
		line = "call ani_callback %s:%s:%s\r\n" % (self.caller, self.caller, "AAA")

		tn = telnetlib.Telnet(ip_address, port)
		slog.info("Waiting for engine to be ready")
		tn.read_until("ready")
		slog.info("Engine is ready to accept telnet command")
		tn.write(line + "\n")
		slog.info("telnet wrote", line)
		tn.read_until("\n")
		slog.info("telnet will quit")
		tn.write("quit\n")
		tn.read_until("\n")
		tn.close()


def call_execute(call, exec_msg):
#	if call.type == calls.CALLTYPE_STD:
#		# no need for tone generating channel anymore
#		slog.info("dropping tone generating channel",call.leg1_dumb_chan)
#		callctl.dropchan(call.leg1_dumb_chan)
	
	if "cb_numbers" in exec_msg.params:
		# store all callback numbers
		call.cb_numbers = exec_msg.params["cb_numbers"]

	chan = exec_msg.params.get("id")
	target_chan = exec_msg.params.get("targetid")

	# check if call.execute concerns 2nd leg, and if 1st leg has gone away,
	# drop the 2nd leg
#	if (chan == call.leg2_dumb_chan) and (call.leg1_chan == None):
#		slog.warn("1st leg has gone away, dropping 2nd.")
#		callctl.smartdrop(call, call.leg2_dumb_chan)
#		call.leg2_dumb_chan = None

	if call.type == calls.CALLTYPE_STD:
		# this could be return after outgoing leg disconnect
		if (chan == call.leg1_chan) and target_chan.startswith("dumb/"):
			call.leg1_dumb_chan = target_chan
			call.dumb_chan = target_chan

			# pick up the phone
#			callctl.answer(call)
			# prompt for destination number
			if call.command == CALLCMD_REQDROP:
				# we dropped it, prompt for destination
				callctl.play_map(call, call.leg1_dumb_chan, "enter-destination")
			else:
				# it probably gone
				callctl.play_map(call, call.leg1_dumb_chan, ["destin-busy", "enter-destination"])
			# reset call
			call.outgoing_number = ""
			call.dtmf_cmd = ""
			call.last_dtmf_time = 0
			call.callout = False
			call.dropme = False
			call.tried_routes = []
			call.type = calls.CALLTYPE_IVR
			return	

def call_answered(call, answer_msg):
	chan = answer_msg.params.get("id")
	slog.info("call answered by ANI callback")
	if call.leg1_answered == False:
		call.leg1_chan = chan
		call.leg1_answered = True
		
		if call.authenticated == False:
			slog.info("first leg answered, prompt for account number")
			callctl.play_map(call, call.leg1_dumb_chan, "enter-account")
			return
		
		slog.info("first leg answered, prompt for destination number")

		# determine if need to play balance
		playbalance = False
		ivr_set_mode = conf.get_str("MOD_ANI_CALLBACK", "ivr_settings_mode", "rad_dtl")
		balance_announce = conf.get_bool("MOD_ANI_CALLBACK", "balance_announce", False)

		# IVR options could be set by radius
		if (hasattr(call,"ivr_options") and (ivr_set_mode == "rad_dtl")):
			if "balance" in call.ivr_options:
				playbalance = True

		# or by config file
		if ivr_set_mode == "conf":
			if balance_announce:
				playbalance = True

		if playbalance:
			callctl.play_map(call, call.leg1_dumb_chan, "card-balance-is", call.ivr_lang)
			balance_sounds = sounds.money(call.balance, call.currency)
			callctl.play_map(call, call.leg1_dumb_chan, balance_sounds, call.ivr_lang)

		# reply to the answer message
		#answer_msg.send()

		# prompt for destination number
		callctl.play_map(call, call.leg1_dumb_chan, "enter-destination")
		
#		callctl.play_tone(call, call.leg1_dumb_chan, "ring")

#		conf_msg = yate.Message("chan.masquerade")
#		conf_msg.params["message"] = "call.conference"
#		conf_msg.params["id"] = call.leg1_chan
#		conf_msg.send()
		return

	if call.leg2_answered == False:
		call.leg2_chan = chan
		call.leg2_answered = True
		target_chan = answer_msg.params.get("targetid")
	
		# set outgoing channel when it answers
		if (target_chan == call.leg1_chan) and (chan != call.leg1_dumb_chan):
			slog.info("Setting outgoing channel to:", chan)
			call.answer_time = time.time()
			slog.info("Setting answer time to", time.time())

			# connect 2nd leg to conference
#			exec_msg = yate.Message("chan.masquerade")
#			exec_msg.params["message"] = "call.execute"
#			exec_msg.params["id"] = call.leg2_chan
#			exec_msg.params["callto"] = call.conf_room
#			exec_msg.send()
#			answer_msg.send()

			call.type = calls.CALLTYPE_STD
			return

	slog.err("Too many answers for callback call.")


def call_conference(call, conf_msg):
	slog.info("call.leg1_chan", call.leg1_chan)
	slog.info("call.leg2_chan", call.leg2_chan)
	slog.info("call.leg1_answered", call.leg1_answered)
	slog.info("call.leg2_answered", call.leg2_answered)
	slog.info("call.type", call.type)
	call.conf_room = conf_msg.params["room"]
	call.conf_chan = conf_msg.params["peerid"]
	if call.leg2_answered != False:
		call.type = calls.CALLTYPE_CONF
		connect_leg2(call)


def call_cdr(call, cdr_msg):

	chan = cdr_msg.params.get("chan", "")
	op = cdr_msg.params.get("operation", "")
	if call.trigger_chan == True and op == "finalize":
		# not interested in trigger channel
		calls.rm_channel(call, chan)
		return

	op = cdr_msg.params.get("operation", "")
	status = cdr_msg.params.get("status", "")
	direction = cdr_msg.params.get("direction", "")
	caller_ip = conf.get_str("RADIUS", "nas_ip_address") # NAS is caller for both legs
	called = None
	
	# the first leg which got active or disconnected will use confid stored in call structure
	# that is to match conf ids in auth and acct messages
	if not chan.startswith("dumb/"):
		if hasattr(call,"confid"):
			confid = call.confid
			calls.conf_ids[chan] = confid
			del call.confid
			slog.info("replacing confid for chan",chan,"with",confid)
		else:
			slog.info("call has no confid attribute, using confid from chan",chan)
			confid = calls.conf_ids[chan]


	if op == "finalize":
		calls.rm_channel(call, chan)

	# check if 1st leg hung up (stop the call if so)
	if (op == "finalize") and (chan == call.leg1_chan):
		slog.info("1st leg hung up.")
		call.leg1_chan = None
		callctl.smartdrop(call, call.leg2_dumb_chan)
		called = call.leg1_called

	# check if routing for 2nd call leg failed or it just hung up
	# (stop the call if so)
	if (op == "finalize") and (chan == call.leg2_dumb_chan) and \
	    (not call.leg2_answered):
		slog.warn("2nd call leg failed to route or hung up.")
		callctl.dropchan(call.leg1_chan)
		called = call.route_msg.params["called"]

	# accounting stops for all routes
	if (op == "finalize") and (direction == "outgoing") and \
	    (not chan.startswith("dumb/")):
		
		if called == None:
			called = call.route_msg.params["called"]
		route_msg = call.route_msg
		
		cdr_data = {}
		cdr_data["duration"] = float(cdr_msg.params["duration"])
		cdr_data["billtime"] = float(cdr_msg.params["billtime"])
		cdr_data["time"] = float(cdr_msg.params["time"])
		# XXX this is real hack (but sometimes there is no billid for failed calls)
		billid = cdr_msg.params.get("billid", "")
		if billid != "":
			cdr_data["billid"] = cdr_msg.params["billid"]
		else:
			cdr_data["billid"] = cdr_msg.params["runid"]
		cdr_data["caller"] = route_msg.params["caller"]
		cdr_data["caller_addr"] = caller_ip
		cdr_data["called_addr"] = cdr_msg.params["address"]
		if call.password != "":
			cdr_data["username"] = call.password
		else:
			cdr_data["username"] = route_msg.params["caller"]
		if cdr_data["billtime"] != 0.0:
			cdr_data["dcause"] = 200
		else:
			reason = cdr_msg.params.get("reason", "Unspecified error")
			cdr_data["dcause"] = return_codes.sipResponseToSipCode.get(reason, 0)
		# set DNIS number
		cdr_data["dnis_access"] = call.dnis
		# calledfull and called numbers
		calledfull = cdr_msg.params.get("calledfull", "")
		if calledfull != "":
			cdr_data["calledfull"] = calledfull
		else:
			slog.warn("Using called as calledfull", called)
			cdr_data["calledfull"] = called
		call.dialed_number = called
		cdr_data["called"] = called
		cdr_data["confid"] = confid

		callctl.accounting_stop(call, cdr_data, route_msg)

	# for some reason there is no billid in outgoing leg1_chan call.cdr message
	# therefore we take leg1_dumb_chan and store its billid
	if (op == "update") and (chan == call.leg1_dumb_chan) and (status == "incoming"):
		call.leg1_dumb_chan_billid = cdr_msg.params["billid"]

	# accounting start 1st leg
	if (op == "update") and (chan == call.leg1_chan) and \
	    (status == "answered"):
		slog.info("accounting start 1st leg")
		if call.leg1_start_sent == True:
			slog.warn("leg1 start was already sent, skipping")
			return

		route_msg = call.route_msg

		cdr_data = {}
		cdr_data["time"] = float(cdr_msg.params["time"])
		billid = cdr_msg.params.get("billid","")
		if billid != "":
			cdr_data["billid"] = cdr_msg.params["billid"]
		else:
			cdr_data["billid"] = call.leg1_dumb_chan_billid

		cdr_data["caller"] = route_msg.params["caller"]
		cdr_data["called"] = cdr_msg.params["called"]
		if "address" in route_msg.params:
			cdr_data["caller_addr"] = route_msg.params["address"]
		else:
			cdr_data["caller_addr"] = caller_ip
		cdr_data["called_addr"] = cdr_msg.params["address"]
		if call.password != "":
			cdr_data["username"] = call.password
		else:
			cdr_data["username"] = route_msg.params["caller"]
		cdr_data["confid"] = calls.conf_ids[call.leg1_chan]
		
		callctl.accounting_alive(call, cdr_data, route_msg)
		# save cdr_data for use in updates
		call.cdr_msg = cdr_msg
		call.leg1_cdr_msg = cdr_msg
		call.leg1_start_sent = True

	# accounting start 2nd leg
	if (op == "update") and (chan == call.leg2_chan) and \
	    (status == "answered"):
		slog.info("accounting start 2nd leg")
		route_msg = call.route_msg

		cdr_data = {}
		cdr_data["time"] = float(cdr_msg.params["time"])
		billid = cdr_msg.params.get("billid","")
		if billid != "":
			cdr_data["billid"] = cdr_msg.params["billid"]
		else:
			cdr_data["billid"] = call.leg1_dumb_chan_billid
		cdr_data["caller"] = route_msg.params["caller"]
		cdr_data["called"] = call.outgoing_number
		if "address" in route_msg.params:
			cdr_data["caller_addr"] = route_msg.params["address"]
		else:
			cdr_data["caller_addr"] = ""
		cdr_data["called_addr"] = cdr_msg.params["address"]
		if call.password != "":
			cdr_data["username"] = call.password
		else:
			cdr_data["username"] = route_msg.params["caller"]
		cdr_data["confid"] = calls.conf_ids[call.leg2_chan]
		call.leg2_cdr_msg = cdr_msg

		callctl.accounting_alive(call, cdr_data, route_msg)

def chan_dtmf(call, dtmf_msg):
	# extract the DTMF char
	text = dtmf_msg.params["text"].strip()

	# set last DTMF time
	call.last_dtmf_time = time.time()
	slog.info("call.type",call.type,"call.dtmf_cmd",call.dtmf_cmd, "call.authenticated", call.authenticated)
	slog.info("call.callout",call.callout,"call.outgoing_number",call.outgoing_number)
	slog.info("call.dropme",call.dropme,"call.locked",call.locked)
	if call.type != calls.CALLTYPE_IVR:
		slog.info("Received DTMF during call")
		# collect ** or ##
		if (text in "#*5"):
			call.dtmf_cmd += text
		if (call.dtmf_cmd == "##") or (call.dtmf_cmd == "**") or (call.dtmf_cmd == "*5"):
			slog.info("Recieved disconnect command during call ",call.dtmf_cmd)
			call.command = CALLCMD_REQDROP
			# disconnect outgoing call leg
			#calls.rm_channel(call, call.out_chan)
		#	callctl.dropchan(call.out_chan)	
			callctl.dropchan(call.leg2_chan)
			#	route to dumb channel to play IVR again
			call.outgoing_number = ""
			exec_msg = yate.Message("chan.masquerade")
			exec_msg.params["message"] = "call.execute"
			exec_msg.params["id"] = call.leg1_chan
			exec_msg.params["callto"] = "dumb/"
			exec_msg.send()
		return


	# stop playing any sounds since user pressed a button
	if call.playing:
		call.sound_queue = []

	if call.dropme:
		return

	if call.locked:
		# don't do anything while RADIUS client is doing its job
		return

	# check if we can try to authenticate user
	if (not call.authenticated) and (call.password != "") and \
	    (text in "#*"):
		slog.info("authenticate user")
		# start the DTMF timeout thing over again
		call.last_dtmf_time = 0
				
		# try password authentication
		# (+ get user balance from RADIUS)
		auth_user(call, "password")
		return

	# check if we can try to place the outgoing call (received #/*
	# character or maximum number of digits dialled)
	if (call.authenticated) and (call.outgoing_number != "") and \
	    (call.callout == False) and \
	    (call.dtmfnostop == False) and \
	    ((text in "#*") or (len(call.outgoing_number) >= call.dest_number_max_digits)):
		# start the DTMF timeout thing over again
		
		call.last_dtmf_time = 0
		slog.info("authenticate call")
		# try to authorize the call
		auth_call(call, call.route_msg, call.outgoing_number)
		return

	# mark first * or # in aleg_pin_auth mode
	# so on 2nd pass it will allow auth
	if (aleg_pin_auth == True) and (call.dtmfnostop == True) and \
	    (text in "#*"):
		call.dtmfnostop = False

	if (call.authenticated) and (call.callout == False):
		# append digits/letters to outgoing number
		if text.isalnum() or text == "*":
			call.outgoing_number += text
		# append PIN/destnumber delimiter
		if (aleg_pin_auth == True) and (text in "#*"):
			call.outgoing_number += "#"

		if call.outgoing_number.endswith("**") or call.outgoing_number.endswith("##"):
			slog.info("resetting destination number")
			call.outgoing_number = ""
			callctl.play_map(call, call.leg1_dumb_chan, "enter-destination")
			return

		# see if any special command was received
		if call.outgoing_number == conf.get_str("IVR", "balance_dtmf", "*"):
			slog.info("User requested balance check")
			call.outgoing_number = call.dnis # reset the called number back to DNIS
			call.command = CALLCMD_BALPLAY
			call.callout = False
			call.type = calls.CALLTYPE_IVR
			call.num_dn_attempts = 0
			call.last_dtmf_time = 0
			auth_user(call, "default")
			
	elif (call.callout == False):
		# append digits/letters to user password
		slog.info("append digits")
		if text.isalnum():
			call.password += text

def chan_notify(call, note_msg):
	"""Notify messages are received whenever a sound has finished playing.

	Since all IVR operations have some sort of informational text played
	before them, this is the logical place to do the most important stuff
	-- like place an outgoing call (the whole point of this module).
	"""
	call.playing = False

	if len(call.sound_queue) == 0:
		if call.dropme:
			callctl.dropchan(call.leg1_chan)
					
		if call.callout:
			# make the call
			
			exec_msg = yate.Message("chan.masquerade")
			exec_msg.params["message"] = "call.execute"
			exec_msg.params["id"] = call.leg1_chan
			exec_msg.params["caller"] = call.route_msg.params["caller"]
			exec_msg.params["called"] = call.outgoing_number
			exec_msg.params["callto"] = call.callout_route
			exec_msg.params["formats"] = call.formats  ## XXX - here we need to send whatever codec terminating gw supports
#			exec_msg.params["timeout"] = call.timeout
			exec_msg.send()
			
			# change call type and set the flag which indicates
			# that we may charge for this call
			call.type = calls.CALLTYPE_STD
			call.billable = True

			return

		if call.last_dtmf_time == 0:
			# start the DTMF timeout thing
			call.last_dtmf_time = time.time()

	# play queued sounds
	callctl.proc_sound_queue(call)


#
# Helper routines
#

def handle_dtmf_timeout(call):
	if call.locked:
		# don't do anything while RADIUS client is doing its job
		return
	
	# start the DTMF timeout thing over again
	call.last_dtmf_time = 0
			
	# check if we can try to authenticate user
	if (not call.authenticated) and (call.password == ""):
		auth_failure(call)
		return

	if (not call.authenticated):
		auth_user(call, "password")		
		return

	#
	# we are authenticated if we get past this point
	#

	# check if we can try to place the outgoing call
	if call.outgoing_number == "":
		callout_failure(call, "timeout")
		return
	
	# try to authorize the call
	auth_call(call, call.route_msg, call.outgoing_number)

def handle_dtmf_timeout_during_call(call):
	if call.locked:
		# don't do anything while RADIUS client is doing its job
		return

	# start the DTMF timeout thing over again
	call.last_dtmf_time = 0
	# reset DTMF command
	call.dtmf_cmd = ""


def connect_leg2(call):	
	cb_numbers = call.cb_numbers
	caller_number, cb_number, called_number = cb_numbers.split(':')

	exec_msg = yate.Message("call.execute")
	exec_msg.params["target"] = called_number
	exec_msg.params["callto"] = "dumb/"
	exec_msg.params["caller"] = caller_number
	exec_msg.params["callername"] = call.id
	exec_msg.send()

def auth_call(call, route_msg, outgoing_number):
	"""Authorize call.

	Returns (route, remaining time in seconds)
		or None if something went wrong.
	"""
	# special case: aleg_pin_auth
	# set that first
	if aleg_pin_auth:
		# split outgoing number by #
		parts = outgoing_number.split("#")
		if len(parts) > 1:
			if parts[1] == "":
				# missing 2nd part migh mean format: dialed#
				outgoing_number = parts[0]
			else:
				# format PIN#dialed#
				call.password = parts[0]
				outgoing_number = parts[1]
		else:
			# fallback to normal ANI mode
			outgoing_number = parts[0]
		call.outgoing_number = outgoing_number
	
	# get call attributes
	caller = route_msg.params.get("caller", "")
	called = outgoing_number
	route_msg.params["called"] = called
	caller_address = route_msg.params.get("address", "")

	chan = route_msg.params.get("id","")	
	h323_conf_id = radius.gen_h323_conf_id()
	# save h323confid for use in first leg accounting request
	slog.info("Saving call.confid value",h323_conf_id)

	# remove port number from address
	caller_address = caller_address.split(':')[0]

	# some RADIUS configuration
	radius_enabled = conf.get_bool("RADIUS", "enable")
	routing_enabled = conf.get_bool("RADIUS", "routing")

	slog.info("Access-Request for call to", called)

	# prepare RADIUS auth request
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"User-Name": caller,
		"Framed-IP-Address": caller_address,
		"Calling-Station-Id": caller,
		"Called-Station-Id": called,
		"h323-conf-id": h323_conf_id
	}
	## enable below line to allow passwordless auth
	# rad_params['Cisco-AVPair'] = "h323-ivr-out=DTL-Ignore-Password:YES"
	# alter call attributes if user entered her password
	if call.password:
		rad_params["User-Name"] = call.password
		rad_params["User-Password"] = [call.password]

	# add digest attributes if available
	user_digest = routes.digest_data.get(caller)
	if user_digest != None:
		rad_params['Digest-Attributes'] = user_digest['digest_attrs']
		rad_params['Digest-Response'] = user_digest['digest_response']
		# refresh timeout timestamp
		user_digest['exp_timestamp'] = time.time() + user_digest['exp_timeout']

	# set DNIS number
	rad_params['Cisco-AVPair'] = "h323-ivr-out=ACCESSCODE:" + call.dnis

	# send auth packet
	calls.lock(call)
	args = (call, route_msg)
	radius.queue_auth(rad_params, authorization_cb, args)

def authorization_cb(rad_result, call, route_msg):
	"""Authorization callback (RADIUS thread calls this)."""
	
	try:
		proc_authorization(rad_result, call, route_msg)
	except:
		slog.err(traceback.format_exc())

	calls.unlock(call)

def proc_authorization(rad_result, call, route_msg):
	"""Process authorization response."""
	
	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		slog.err("RADIUS auth for outgoing call failed, result code: ", rad_result.code)
		failure_cause = "noroutes"
		if rad_result != None:
			# extract failure reason
			h323_return_code = rad_result["h323-return-code"][0]
			h323_return_code = int(h323_return_code.split('=')[1])
			slog.info("RADIUS return code ",h323_return_code)
			if h323_return_code == 12:
				failure_cause = "nocredit"

		callout_failure(call, failure_cause)
		return

	# get called number and local user
	called = route_msg.params.get("called", "")
	called_user = routes.getuser(called)

	# get remaining seconds
	rem_sec = rad_result["h323-credit-time"][0]
	rem_sec = int(rem_sec.split('=')[1])
	slog.info("Remaining seconds:", rem_sec)
	# ivr reported remaining seconds
	ivr_rem_sec = rem_sec

	# get call rate in cents (or any other currency 1/100 units per minute)
	slog.info("Cisco-AVPair", rad_result["Cisco-AVPair"])
	call_rate = None
	for avpairs in rad_result["Cisco-AVPair"]:
		parts = avpairs.split("=")
		rt = re.compile("^CALLRATE:(.+)")
		du = re.compile("^DURATION:(.+)")
		cn = re.compile("^called-number:(.+)")
		mrt = rt.match(parts[1])
		mdu = du.match(parts[1])
		mcn = cn.match(parts[1])
		if mrt:
			slog.info("Callrate is: ", mrt.group(1))
			call_rate = int(float(mrt.group(1)) * 100)
		if mdu:
			slog.info("Override remaining seconds: ", mdu.group(1))
			rem_sec = int(float(mdu.group(1)))
		if mcn:
			slog.info("Override Called number: ", mcn.group(1))
			call.outgoing_number = mcn.group(1)

	# add session timeout in millisecs
	route_msg.params["timeout"] = str(rem_sec * 1000)
	slog.info("Attaching remaining time to route message, ms ",route_msg.params["timeout"])

	# send route to local user if possible
	if called_user:
		slog.info("Routing call to local user:", called_user.location)
		callout_success(call, called_user.location, rem_sec, ivr_rem_sec, call_rate)
		return

	# check if we should use RADIUS based routing
	routing_enabled = conf.get_bool("RADIUS", "routing")

	# static routing
	if not routing_enabled:
		static_route = conf.get_str("RADIUS", "static_route")
		if static_route:
			p = re.compile('\{called\}')
			static_route = p.sub(called, static_route)
			slog.info("Static route", static_route)
			callout_success(call, static_route, rem_sec, ivr_rem_sec, call_rate)
			return
		else:
			slog.err("Static route is invalid.")
			callout_failure(call, "noroutes")
			return

	# RADIUS based routing
	if routing_enabled and not rad_result.has_key("Cisco-Command-Code"):
		slog.err("No route from RADIUS.")
		callout_failure(call, "noroutes")
		return

	yate_routes = []
	yate_routes_attrs = []
	routing_attrs = rad_result["Cisco-Command-Code"]
	lateroute_id = 1
	for cmd_code in routing_attrs:
		tokens = cmd_code.split("/")
		if len(tokens) < 7:
			slog.warn("Invalid Cisco-Command-Code attribute"
			    " format")
			continue

		gateway = tokens[0]
		subtokens = gateway.split(":")
		protocol = subtokens[0].strip().lower()

		if protocol == "h.323":
			protocol = "h323"

		if protocol not in ("sip", "h323"):
			slog.warn("Invalid protocol:", protocol)
			continue

		dest_number = tokens[3]
		dest_ip = tokens[6]

		# add "sip:" prefix to called number for SIP
		if protocol == "sip":
			dest_number = "sip:" + dest_number

		# create a yate route -- module/number@ip
		yroute = protocol + '/' +  dest_number + '@' +  dest_ip

		# build full routestring
		yroute_attrs = 'callto=' + yroute + ';caller=' + tokens[2] + ';called=' + dest_number
		# append line (account) information
		if len(subtokens) > 1:
			account = subtokens[1].strip()
			yroute_attrs = yroute_attrs + ';line=' + account + '@' + dest_ip
		if len(tokens) > 7 and len(tokens[7]) > 0:
			yroute_attrs = yroute_attrs + ';formats=' + tokens[7]

		yate_routes.append('lateroute/lateroute_' + str(lateroute_id))
		lateroute_id = lateroute_id + 1
		yate_routes_attrs.append(yroute_attrs)

	slog.info("yate routes:", yate_routes)

	if len(yate_routes) == 0:
		slog.err("No valid routes received from RADIUS server.")
		callout_failure(call, "noroutes")
		return

	call.authenticated = True
	call.yate_routes = yate_routes_attrs

	# one-choice routes do not need any special handling here anymore

	# for description of yate's callfork module see
	# http://yate.null.ro/pmwiki/index.php?n=Main.Callfork
	forked_route = "fork " + " | ".join(yate_routes) + ";stoperror=busy"
	slog.info("Forked route:", forked_route)
	call.route_msg.retvalue = forked_route

	callout_success(call, forked_route, rem_sec, ivr_rem_sec, call_rate)

def callout_success(call, yate_route, rem_sec, ivr_rem_sec, call_rate):
	# play remaining time for this call
	call.callout = True
	call.callout_route = yate_route
	call.timeout = rem_sec * 1000

	# IVR play mode 
	playrate = False
	playtime = False
	ivr_set_mode = conf.get_str("MOD_ANI_CALLBACK", "ivr_settings_mode", "rad_dtl")
	time_announce = conf.get_bool("MOD_ANI_CALLBACK", "time_announce", False)
	# play rate only by configfile 
	play_rate = conf.get_bool("MOD_ANI_CALLBACK", "rate_announce", False)

	# IVR options could be set by radius
	if (hasattr(call,"ivr_options") and (ivr_set_mode == "rad_dtl")):
		if "time" in call.ivr_options:
			playtime = True
	
	# or by config file
	if ivr_set_mode == "conf":
		if time_announce:
			playtime = True

	if playtime and hasattr(call,"ivr_time"):
		callctl.play_map(call, call.leg1_dumb_chan, "you-have")
		callctl.play_map(call, call.leg1_dumb_chan, sounds.time(ivr_rem_sec))

	if play_rate and call_rate:
		callctl.play(call, call.leg1_dumb_chan, "this-call-will-cost")
		call_rate_sounds = sounds.integer(call_rate)
		callctl.play(call, call.leg1_dumb_chan, call_rate_sounds)
		callctl.play(call, call.leg1_dumb_chan, "cents-per-minute")

	if (not playtime) and (not play_rate):
		# just dumb operation
		callctl.play(call, call.leg1_dumb_chan, "silence/1")


def callout_failure(call, reason):
	noroutes_sounds = [
		"invalid-destin",
		"enter-destination",
	]
	noroutes_sounds_drop = ["no-route-exists-to-dest", "goodbye"]
	timeout_sounds = "still-waiting"
	timeout_sounds_drop = "goodbye"
	nocredit_sounds = [
		"not-enough-credit",
		"check-number-dial-again",
	]
	nocredit_sounds_drop = ["not-enough-credit", "goodbye"]
	if reason == "noroutes":
		sounds = noroutes_sounds
		sounds_drop = noroutes_sounds_drop
	elif reason == "nocredit":
		sounds = nocredit_sounds
		sounds_drop = nocredit_sounds_drop
	elif reason == "timeout":
		sounds = timeout_sounds
		sounds_drop = timeout_sounds_drop
	else:
		slog.err("Unknown reason passed to callout_failure()")
	
	if call.num_dn_attempts < call.max_dn_attempts:
		call.outgoing_number = ""
		call.num_dn_attempts += 1
		callctl.play_map(call, call.leg1_dumb_chan, sounds)
	else:
		call.dropme = True
		callctl.play_map(call, call.leg1_dumb_chan, sounds_drop)

def acct_update(call, chan, leg):
	route_msg = call.route_msg
	if leg == 2:
		if hasattr(call,"leg2_cdr_msg"):
			cdr_msg = call.leg2_cdr_msg
		else:
			slog.warn("Unable to send acct_update for leg 2, call.leg2_cdr_msg missing")
			return
	else:
		cdr_msg = call.leg1_cdr_msg
	slog.info("Accounting update leg",leg)
	cdr_data = {}
	if not chan in calls.conf_ids:
		slog.warn("cannot find chan",chan,"call leg possibly gone")
		return

	cdr_data["confid"] = calls.conf_ids[chan]
	cdr_data["time"] = float(cdr_msg.params["time"])
	cdr_data["billid"] = cdr_msg.params.get("billid",call.leg1_dumb_chan_billid)
	cdr_data["caller"] = route_msg.params["caller"]
	cdr_data["called"] = cdr_msg.params["called"]
	if "address" in route_msg.params:
		cdr_data["caller_addr"] = route_msg.params["address"]
	else:
		cdr_data["caller_addr"] = "";
	if "address" in cdr_msg.params:
		cdr_data["called_addr"] = cdr_msg.params["address"]
	else:
		cdr_data["called_addr"] = ""
	if call.password != "":
		cdr_data["username"] = call.password
	else:
		cdr_data["username"] = route_msg.params["caller"]
	cdr_data["confid"] = calls.conf_ids[chan]
	
	callctl.accounting_alive(call, cdr_data, route_msg, True)
	
### below functions user for balance inquiry and for A-leg based authentication	
def auth_user(call, auth_type):
	"""Authenticate caller with RADIUS server.

	call -- structure that contains all data about this call
	auth_type -- 'number', 'password' or 'default'

	auth_type = 'number' means we set User-Name attribute to the calling
			number and omit the User-Password attribute.

	auth_type = 'password' means we set User-Name and User-Password to the
			thepassword that user has input (collected from
			DTMFs), accessible here as call.password

	auth_type = 'default' means we will take User-Name and User-Password
			from existing call data. This method should only be used for 
			request of special operation, like balance etc where caller is already
			authenticated and their data is stored in call object

	Returns user balance.
	"""
	# get caller number and IP address from call.route message
	caller = call.route_msg.params.get("caller", "")
	caller_address = call.route_msg.params.get("address", "")

	# remove port number from address
	caller_address = caller_address.split(':')[0]

	# authenticate caller (and get her balance)
	rad_params = {
		"NAS-IP-Address": conf.get_str("RADIUS", "nas_ip_address"),
		"Framed-IP-Address": caller_address,
		"Calling-Station-Id": caller
	}

	# do we need to register this ANI?
	register_ani = conf.get_bool("IVR", "register_ani", False)
	if register_ani != False and caller.isdigit():
		rad_params['Cisco-AVPair'] = "h323-ivr-out=DTL-Radius-Command:Register-CB-ANI"

	# add digest stuff, if available for this user
	user_digest = routes.digest_data.get(caller)
	if user_digest != None:
		rad_params['Digest-Attributes'] = user_digest['digest_attrs']
		rad_params['Digest-Response'] = user_digest['digest_response']
		# refresh timeout timestamp
		user_digest['exp_timestamp'] = time.time() + user_digest['exp_timeout']

	if auth_type == "number":
		rad_params["User-Name"] = caller
	elif auth_type == "password":
		rad_params["User-Name"] = call.password
		rad_params["User-Password"] = [call.password]
	elif auth_type == "default":
		rad_params["User-Name"] = caller
		rad_params["User-Password"] = [call.password]
	else:
		raise Exception("invalid authorization type")

	# send auth packet
	calls.lock(call)
	radius.queue_auth(rad_params, authentication_cb, call)

def authentication_cb(rad_result, call):
	"""Authentication callback (RADIUS thread calls this)."""
	
	try:
		proc_authentication(rad_result, call)
	except:
		slog.err(traceback.format_exc())

	calls.unlock(call)

def proc_authentication(rad_result, call):
	"""Process authentication response."""
	
	if (rad_result == None) or (rad_result.code != packet.AccessAccept):
		slog.info("RADIUS auth failed (cannot get account balance).")
		auth_failure(call)
		return

	# check if any special command was received
	if "Cisco-AVPair" in rad_result:
		for avpairs in rad_result["Cisco-AVPair"]:
			parts = avpairs.split("=")
			iv = re.compile("^ivr-options:(.+)")
			miv = iv.match(parts[1])
			if miv:
				slog.info("IVR options: ", miv.group(1))
				call.ivr_options = miv.group(1)

	#
	# extract balance
	#
	
	balance_attr = rad_result.get("h323-credit-amount", [None])[0]
	if balance_attr == None:
		slog.err("Could not extract h323-credit-amount from "
		    "auth response")
		auth_success(call, None, None)
		return
	
	balance_split = balance_attr.split('=')
	if len(balance_split) != 2:
		slog.err("Invalid balance string:", balance_attr)
		auth_success(call, None, None)
		return
	
	balance = balance_split[1]
	slog.info("Balance:", balance)

	#
	# extract currency
	#
	
	currency_attr = rad_result.get("h323-currency", [None])[0]
	if currency_attr == None:
		slog.warn("Could not extract h323-currency from auth "
		     "response.")
		auth_success(call, balance, None)
		return
		
	currency_split = currency_attr.split('=')
	if len(currency_split) != 2:
		slog.warn("Invalid currency string", currency_attr)
		auth_success(call, balance, None)
		return

	currency = currency_split[1].upper()
	slog.info("Currency:", currency)
	auth_success(call, balance, currency)

def auth_success(call, balance, currency):
	# we now know who's calling
	call.authenticated = True
	
	# determine if need to play balance
	playbalance = False
	ivr_set_mode = conf.get_str("MOD_ANI_CALLBACK", "ivr_settings_mode", "rad_dtl")
	balance_announce = conf.get_bool("MOD_ANI_CALLBACK", "balance_announce", False)

	# IVR options could be set by radius
	if (hasattr(call,"ivr_options") and (ivr_set_mode == "rad_dtl")):
		if "balance" in call.ivr_options:
			playbalance = True

	# or by config file
	if ivr_set_mode == "conf":
		if balance_announce:
			playbalance = True
	
	# or by CALLCMD_BALPLAY command:
	if call.command == CALLCMD_BALPLAY:
		playbalance = True
	
	slog.info("Play balance is ",playbalance)
	# play account balance if it is available
	if balance != None and playbalance:
		callctl.play_map(call, call.leg1_dumb_chan, "welcome")
		callctl.play_map(call, call.leg1_dumb_chan, "card-balance-is")
		balance_sounds = sounds.money(balance, currency)
		callctl.play_map(call, call.leg1_dumb_chan, balance_sounds)

	# play the destination prompt again
	callctl.play_map(call, call.leg1_dumb_chan, "enter-destination")


def auth_failure(call):
	call.num_pw_attempts += 1
	call.password = ""
	slog.info("Attempt ",call.num_pw_attempts)
	if call.num_pw_attempts == 1:
		callctl.play_map(call, call.leg1_dumb_chan, "enter-account")
		return
	
	if call.num_pw_attempts < call.max_pw_attempts + 1:
		callctl.play_map(call, call.leg1_dumb_chan, "invalid-account")
		callctl.play_map(call, call.leg1_dumb_chan, "enter-account")

	else:
		call.dropme = True
		callctl.play_map(call, call.leg1_dumb_chan, "login-fail")