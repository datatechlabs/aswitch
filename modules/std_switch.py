"""
Standard call (without IVR)
"""

import time, re
from wobble import slog, conf
from aswitch import calls, callctl, routes, return_codes, utils

callto_breakup = re.compile("([0-9]+)@([^\s]+)")

acct_update_enable = conf.get_bool("RADIUS", "acct_update_enable", False)
acct_update_interval = conf.get_int("RADIUS", "acct_update_interval", 60)
sip_update_interval = conf.get_int("CALLS", "sip_update_interval", 0)

#
# Message handlers
#

def engine_timer(call, msg):
	# check if call drop time is reached (incl safety margin)
	if hasattr(call,"timeout") and hasattr(call,"answer_time"): 
		if call.answer_time -2 + call.timeout/1000  < time.time():
			if call.in_chan in call.channels:
				slog.info("disconnecting the call")
				callctl.dropchan(call.in_chan)
				calls.rm_channel(call, call.in_chan)

	# check for accounting update timers 
	if acct_update_enable and hasattr(call,"answer_time"):
		call.update_timer = call.update_timer + 1
		if 	call.update_timer >= acct_update_interval:
			acct_update(call)
			call.update_timer = 0

	# check for SIP UPDATE timer
	if sip_update_interval > 0 and hasattr(call, "answer_time"):
		call.sip_update_timer = call.sip_update_timer + 1
		if call.sip_update_timer >= sip_update_interval:
			routes.send_sip_update(call, msg)
			call.sip_update_timer = 0
			
def call_route(call, route_msg):

	# catch any lateroute messages here - use special called: lateroute_XXX param
	called = route_msg.params.get("called", "")

	chan = route_msg.params["id"]
	callto = route_msg.params.get("callto", "")
	if chan.startswith("fork/") and callto != "":
		route_msg.send()
		return

	# extract incoming channel & set outgoing to empty string
	call.in_chan = chan
	call.out_chan = ""
	call.tried_routes = [] # pairs of (number, address)
	call.leginfo = [] # info on each forked leg
	call.cdr_msg = False

	call.route_msg = route_msg
	call.yate_routes = False
	call.type = calls.CALLTYPE_STD
	call.dialed_number = route_msg.params["called"]
	# only initialize once
	if not hasattr(call,"outgoing_number"):
		call.outgoing_number = route_msg.params.get("called", "")

	call.update_timer = 0 # accounting update timer
	call.sip_update_timer = 0 # SIP UPDATE timer

	# force yate to not proxy voice
	rtp_proxy = conf.get_bool("CALLS", "rtp_proxy")
	if (not rtp_proxy):
		route_msg.params["rtp_forward"] = "yes"
	else:
		route_msg.params["rtp_forward"] = "no"
	
	routes.authorize_call(call, route_msg)

def call_execute(call, exec_msg):
	"""We use call.execute messages to gather tried routes."""

	chan = exec_msg.params.get("id", "")
	target_chan = exec_msg.params.get("targetid", "")

	callto = exec_msg.params.get("callto")
	if callto == None:
		exec_msg.reverse()
		exec_msg.send()
		return

	# since call.execute may arrive before call.cdr, use targetid to set out_chan
	# do not allow to modify it by subsequent fork channels
	if call.out_chan == "":
		slog.info("setting call.out_chan to ",target_chan)
		call.out_chan = target_chan
	else:
		slog.info("call.out_chan is already set to ",call.out_chan)

	if hasattr(call, "in_chan"):
		if (chan == call.in_chan) and target_chan.startswith("fork/"):
			# not interested in incoming_chan->fork_chan
			exec_msg.reverse()
			exec_msg.send()
			return

	matches = callto_breakup.findall(callto)
	if len(matches) != 1:
		slog.warn("Unrecognized 'callto' parameter in call.execute.")
		exec_msg.reverse()
		exec_msg.send()
		return

	call.tried_routes.append(matches[0])

	leg_info = 'chan=' + target_chan + ';domain=' + exec_msg.params.get("line", "") +\
	  ';out-gw=' + exec_msg.params.get("domain", "")
	call.leginfo.append(leg_info)
	
	# override params from call.yate_routes
	try:
		for yroute in call.yate_routes:
			if callto == yroute:
				slog.info("Overriding params from call.yate_routes for leg ", yroute)
				exec_msg.params["formats"] = call.yate_routes[yroute]["formats"]
				exec_msg.params["domain"] = call.yate_routes[yroute]["domain"]
				exec_msg.params["caller"] = call.yate_routes[yroute]["caller"]
				exec_msg.params["called"] = call.yate_routes[yroute]["called"]
				exec_msg.params["callername"] = call.yate_routes[yroute]["callername"]
				if call.yate_routes[yroute]["line"]:
					exec_msg.params["line"] = call.yate_routes[yroute]["line"]
	except:
		pass
	exec_msg.reverse()
	exec_msg.send()
	
	
def call_answered(call, answer_msg):
	# the one channel that answers is the outgoing leg
	chan = answer_msg.params.get("id")
	call.out_chan = chan
	call.answer_time = time.time()
		
def call_cdr(call, cdr_msg):
	chan = cdr_msg.params.get("chan", "")
	op = cdr_msg.params.get("operation", "")
	status = cdr_msg.params.get("status", "")
	direction = cdr_msg.params.get("direction", "")
	slog.info("op",op,"status",status,"chan",chan,"direction",direction)
	server_addr = conf.get_str("SERVER", "ip_address", "localhost")
	
	# remove channel from call object
	if op == "finalize":
		calls.rm_channel(call, chan)

	if op == "combined":
		calls.rm_channel(call, chan)

	# we only use outgoing legs
	if direction != "outgoing":
		return

	confid = cdr_msg.params["billid"] + "-" + cdr_msg.params.get("cdrid", "")
	if op == "initialize":
		# accounting start
		route_msg = call.route_msg

		cdr_data = {}
		cdr_data["time"] = float(cdr_msg.params["time"])
		cdr_data["billid"] = cdr_msg.params["billid"]
		cdr_data["caller"] = route_msg.params["caller"]
		if hasattr(call, "dialed_number"):
			cdr_data["called"] = call.dialed_number
		else:
			cdr_data["called"] = cdr_msg.params.get("calledfull")
		cdr_data["caller_addr"] = route_msg.params.get("address", server_addr)
		cdr_data["called_addr"] = cdr_msg.params["address"]
		cdr_data["username"] = call.auth_user
		cdr_data["confid"] = confid
		cdr_data["status"] = status

		# save cdr_data for use in updates
		call.outgoing_number = cdr_data["called"]
		call.cdr_msg = cdr_msg

		callctl.accounting_alive(call, cdr_data, route_msg)

	elif op == "update":
		# accounting update
		route_msg = call.route_msg

		cdr_data = {}
		cdr_data["time"] = float(cdr_msg.params["time"])
		cdr_data["billid"] = cdr_msg.params["billid"]
		cdr_data["caller"] = route_msg.params["caller"]
		if hasattr(call, "dialed_number"):
			cdr_data["called"] = call.dialed_number
		else:
			cdr_data["called"] = cdr_msg.params.get("calledfull")
		cdr_data["caller_addr"] = route_msg.params.get("address", server_addr)
		cdr_data["called_addr"] = cdr_msg.params["address"]
		cdr_data["username"] = call.auth_user
		cdr_data["confid"] = confid
		cdr_data["status"] = status

		# save cdr_data for use in updates
		call.outgoing_number = cdr_data["called"]
		call.cdr_msg = cdr_msg

		callctl.accounting_alive(call, cdr_data, route_msg, True)
	
	elif op == "finalize":
		# Accounting stop
		route_msg = call.route_msg
		
		cdr_data = {}
		cdr_data["duration"] = float(cdr_msg.params["duration"])
		cdr_data["billtime"] = float(cdr_msg.params["billtime"])
		cdr_data["time"] = float(cdr_msg.params["time"])
		cdr_data["billid"] = cdr_msg.params["billid"]
		cdr_data["caller"] = route_msg.params["caller"]
		cdr_data["called"] = route_msg.params["called"]
		cdr_data["calledfull"] = cdr_msg.params.get("calledfull")
		cdr_data["caller_addr"] = route_msg.params.get("address", server_addr)
		cdr_data["called_addr"] = cdr_msg.params["address"]
		cdr_data["chan"] = cdr_msg.params["chan"]
		cdr_data["cdr_called"] = cdr_msg.params["called"]
		if "external" in cdr_msg.params:
			cdr_data["external"] = cdr_msg.params["external"]

		cdr_data["username"] = call.auth_user

		if cdr_data["billtime"] != 0.0:
			cdr_data["dcause"] = 200
		else:
			reason = cdr_msg.params.get("reason", "Unspecified error")
			cdr_data["dcause"] = return_codes.sipResponseToSipCode.get(reason.lower(), 0)
		cdr_data["confid"] = confid
		cdr_data["status"] = status

		callctl.accounting_stop(call, cdr_data, route_msg)
	elif op == "combined":
		slog.info("Ignoring combined cdr messages")
	else:
		slog.warn("Invalid call.cdr message operation found: %s" % op)
		slow.info("Dumping message %s" % call_cdr)
	

def acct_update(call):
	# we will not emit our own update messages anymore
	return

	route_msg = call.route_msg
	cdr_msg = call.cdr_msg
	if cdr_msg == False:
		return
	chan = call.out_chan
	server_addr = conf.get_str("SERVER", "ip_address", "localhost")

	cdr_data = {}
	if cdr_msg != False:
		cdr_data["time"] = float(cdr_msg.params["time"])
		cdr_data["billid"] = cdr_msg.params["billid"]
		
	cdr_data["caller"] = route_msg.params["caller"]
	cdr_data["called"] = call.dialed_number
	cdr_data["caller_addr"] = route_msg.params.get("address", server_addr)
	cdr_data["called_addr"] = cdr_msg.params["address"]
	cdr_data["username"] = call.auth_user
	cdr_data["confid"] = calls.conf_ids[chan]

	callctl.accounting_alive(call, cdr_data, route_msg, True)
