#! /bin/sh
#
#               Written by Miquel van Smoorenburg <miquels@cistron.nl>.
#               Modified for Debian by Ian Murdock <imurdock@gnu.ai.mit.edu>.
#
# run "update-rc.d aswitch defaults" to install this script


### BEGIN INIT INFO
# Provides:          aswitch
# Required-Start:    $all
# Required-Stop:     $all
# Should-Start:      $local_fs
# Should-Stop:       $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: ArrowSwitch VoIP server
# Description:       Start ArrowSwitch in daemon mode
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/local/bin/aswitch
DAEMON_OPTS=-c /usr/local/etc/aswitch.conf
NAME=aswitch
DESC=aswitch
# PIDFILE=/var/run/$NAME.pid
test -x $DAEMON || exit 0

# Include defaults if available
if [ -f /etc/default/aswitch ] ; then
        . /etc/default/aswitch
fi

if [ "$NO_START" = "1" ];then
        echo Aswitch Daemon not setup for automagic start. Edit /etc/default/aswitch to change this.
        exit 0
fi
set -e

case "$1" in
  start)
        echo -n "Starting $DESC: "

        start-stop-daemon --start --quiet \
                --exec $DAEMON $DAEMON_OPTS
        echo "$NAME."
        ;;
  stop)
        echo -n "Stopping $DESC: "
        start-stop-daemon --stop --quiet --oknodo --name $NAME
        echo "$NAME."
        ;;
  restart|force-reload)
        echo -n "Restarting $DESC: "
        start-stop-daemon --stop --quiet --name $NAME
        sleep 1

        start-stop-daemon --start --quiet  \
                --exec $DAEMON $DAEMON_OPTS
        echo "$NAME."
        ;;
  *)
        N=/etc/init.d/$NAME
        echo "Usage: $N {start|stop|restart|force-reload}" >&2
        exit 1
        ;;
esac

exit 0
